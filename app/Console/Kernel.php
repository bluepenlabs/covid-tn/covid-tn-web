<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use App\Modules\Notification\Controllers\ApiController as ApiNotificationController;
use App\Modules\General\Controllers\WebController as WebGeneralController;

use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
       $schedule->call(function () { ApiNotificationController::scheduleMedicalAgentReminder(); })->twiceDaily(9,15);
       $schedule->call(function () { WebGeneralController::scheduleSyncStats(); })->twiceDaily(17,21);

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
