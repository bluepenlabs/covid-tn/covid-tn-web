<?php

Route::group(['module' => 'User', 'middleware' => ['api'], 'namespace' => 'App\Modules\User\Controllers'], function() {

    Route::post('/api/user/login/native', 'ApiController@handleUserNativeLogin');
    Route::post('/api/user/update', 'ApiController@handleUserUpdateProfile');
    Route::post('/api/user/login/social', 'ApiController@handleUserSocialLogin');
    Route::post('/api/user/device/add', 'ApiController@handleCheckUserDevice');
    Route::post('/api/user/device/update', 'ApiController@handleUpdateDeviceToken');

});
