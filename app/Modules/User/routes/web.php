<?php

Route::group(['module' => 'User', 'middleware' => ['web'], 'namespace' => 'App\Modules\User\Controllers'], function() {

    Route::post('/membership/register', 'WebController@handleUserRegister')->name('handleUserRegister');
    Route::post('/membership/login', 'WebController@handleUserLogin')->name('handleUserLogin');
    Route::get('/membership/profile', 'WebController@showUserProfile')->name('showUserProfile');
    Route::post('/membership/profile/update', 'WebController@handleUpdateUserProfile')->name('handleUpdateUserProfile');
    Route::get('/membership/logout', 'WebController@handleLogout')->name('handleLogout');

    Route::get('/membership/social/{provider}', 'WebController@handleSocialRedirect')->name('handleSocialRedirect');
    Route::get('/membership/social/{provider}/callback', 'WebController@handleSocialCallback')->name('handleSocialCallback');

    Route::get('/manager/users', 'WebController@showManagerUsersList')->name('showManagerUsersList')->middleware('checkManagerAccess');
    Route::post('/manager/users/add', 'WebController@handleManagerAddUser')->name('handleManagerAddUser')->middleware('checkManagerAccess');
    Route::get('/manager/users/ban/{id}', 'WebController@handleManagerBanUser')->name('handleManagerBanUser')->middleware('checkManagerAccess');
    Route::get('/manager/users/reactivate/{id}', 'WebController@handleManagerActivateUser')->name('handleManagerActivateUser')->middleware('checkManagerAccess');
    Route::post('/manager/users/update/{id}', 'WebController@handleManagerEditUser')->name('handleManagerEditUser')->middleware('checkManagerAccess');

});
