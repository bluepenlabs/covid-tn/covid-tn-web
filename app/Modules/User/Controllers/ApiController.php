<?php

namespace App\Modules\User\Controllers;

use App\Modules\User\Models\User;
use App\Modules\User\Models\UserDevice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    public function handleUserNativeLogin(Request $request)
    {

        $data = $request->json()->all();

        if (!isset($data['token']) or !checkApiToken($data['token'])) {
            return response()->json(['data' => [], 'status' => ['message' => 'Token mismatch', 'code' => 403]]);
        }

        if(!isset($data['email']) or !isset($data['password']))
        {
            return response()->json(['data' => [], 'status' => ['message' => 'Email or Password missing', 'code' => 404]]);
        }

        $credentials = [
            'email' => $data['email'],
            'password' => $data['password'],
        ];

        if (Auth::validate($credentials)) {
            $user = User::where('email', $data['email'])->with('services')->first();

            if (count($user->services) == 0){
                return response()->json(['data' => $user, 'error' => ['message' => 'User not attached to hospital services', 'code' => 509]]);
            }

            if ($user->status === 3) {
                return response()->json(['data' => [], 'status' => ['message' => 'User banned', 'code' => 500]]);
            }

            return response()->json(['data' => $user, 'error' => ['message' => '', 'code' => 200]]);

        } else {
            return response()->json(['data' => [], 'status' => ['message' => 'Wrong Email or Password', 'code' => 401]]);
        }
    }

    public function handleUserUpdateProfile(Request $request)
    {
        $data = $request->json()->all();

        if (!isset($data['token']) or !checkApiToken($data['token'])) {
            return response()->json(['data' => [], 'status' => ['message' => 'Token mismatch', 'code' => 403]]);
        }

        if(!isset($data['id']))
        {
            return response()->json(['data' => [], 'status' => ['message' => 'Id missing', 'code' => 404]]);
        }

        $user = User::find($data['id']);

        if(isset($data['name'])){ $user->name = $data['name']; }
        if(isset($data['age'])){ $user->age = $data['age']; }
        if(isset($data['phone'])){ $user->phone = $data['phone']; }
        if(isset($data['description'])){ $user->description = $data['description']; }
        if(isset($data['password'])){ $user->password = bcrypt($data['password']); }

        $user->save();

        return response()->json(['data' => $user, 'error' => ['message' => 'User updated successfully', 'code' => 200]]);
    }

    public function handleUserSocialLogin(Request$request){

        $data = $request->json()->all();

        if (!isset($data['token']) or !checkApiToken($data['token'])) {
            return response()->json(['data' => [], 'status' => ['message' => 'Token mismatch', 'code' => 403]]);
        }


        $user = User::where('provider_id', '=',$request['providerId'])
            ->orWhere('email', '=',$request['email'])
            ->first();

        if (!$user) {
            if ($request['picture']) {
                $imagePath = 'storage/uploads/users/' . $request['providerId'] . '-' . time() . '.png';
                file_put_contents($imagePath, file_get_contents('https://graph.facebook.com/' . $request['providerId'] . '/picture?type=large'));
            } else {
                $imagePath = null;
            }

            $user = User::create([
                'provider_id' =>$request['providerId'],
                'provider' =>$request['provider'],
                'email' =>$request['email'],
                'first_name' =>$request['first_name'],
                'last_name' =>$request['last_name'],
                'picture' => $imagePath,
                'status' => 1,
            ]);

            return response()->json(['data' => $user, 'error' => ['message' => 'User not attached to hospital services', 'code' => 509]]);

        }else{

            if ($user->status == 2) {
                return response()->json(['data' => [], 'status' => ['message' => 'User banned', 'code' => 500]]);
            } else {
                if (count($user->services) == 0){
                    return response()->json(['data' => $user, 'error' => ['message' => 'User not attached to hospital services', 'code' => 509]]);
                }

                return response()->json(['data' => $user, 'error' => ['message' => '', 'code' => 200]]);
            }
        }
    }

    public function handleCheckUserDevice(Request $request){
        $data = $request->json()->all();

        if (!isset($data['token']) or !checkApiToken($data['token'])) {
            return response()->json(['data' => [], 'status' => ['message' => 'Token mismatch', 'code' => 403]]);
        }

        if(!isset($data['uuid']) or
            !isset($data['platform']) or
            !isset($data['os_version']) or
            !isset($data['model']) or
            !isset($data['id'])
        ){
            return response()->json(['data' => [], 'status' => ['message' => 'Data missing', 'code' => 404]]);
        }

        $device = UserDevice::where('uuid', '=', $data['uuid'])->first();

        if(!$device){
            UserDevice::create([
                'user_id' => $data['id'],
                'platform' => $data['platform'],
                'os_version' => $data['os_version'],
                'model' => $data['model'],
                'status' => 1,
                'token' => isset($data['device_token'])? $data['device_token'] : null,
                'uuid' => $data['uuid'],
            ]);
        } elseif($data['id'] !== $device->user_id) {
            $device->user_id = $data['id'];
            $device->token = isset($data['device_token'])? $data['device_token'] : $device->token;
            $device->save();
        }

        return response()->json(['data' => [], 'error' => ['message' => 'Update Successful', 'code' => 200]]);
    }

    public function handleUpdateDeviceToken(Request $request){

        $data = $request->json()->all();

        if (!isset($data['token']) or !checkApiToken($data['token'])) {
            return response()->json(['data' => [], 'status' => ['message' => 'Token mismatch', 'code' => 403]]);
        }

        if(!isset($data['uuid']) or
            !isset($data['device_token'])
        ){
            return response()->json(['data' => [], 'status' => ['message' => 'Data missing', 'code' => 404]]);
        }

        $device = UserDevice::where('uuid', '=', $data['uuid'])->first();

        if($device){
            $device->token = $data['device_token'];
            $device->save();
        } else {
            return response()->json(['data' => [], 'status' => ['message' => 'No device found', 'code' => 405]]);
        }

        return response()->json(['data' => [], 'error' => ['message' => 'Update Successful', 'code' => 200]]);
    }


}
