<?php

namespace App\Modules\User\Controllers;

use App\Modules\General\Models\General;
use App\Modules\Hospital\Models\Hospital;
use App\Modules\Hospital\Models\HospitalService;
use App\Modules\User\Models\Role;
use App\Modules\User\Models\User;
use App\Modules\Volunteer\Models\Volunteer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Kamaln7\Toastr\Facades\Toastr;
use Laravel\Socialite\Facades\Socialite;

class WebController extends Controller
{

    public function handleUserRegister(Request $request){
        $data = $request->all();

        $check = User::where('email', '=', $data['email'])->first();

        if($check){
            Toastr::error('L\'email entré est déjà utilisé !');
            return redirect(route('showHome'));
        }

        $user = User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password'])
        ]);

        Auth::login($user);
        User::find($user->id)->roles()->attach(2);

        Toastr::success('Bienvenue parmis nous !');
        return redirect(route('showHome'));
    }

    public function handleUserLogin(Request $request)
    {
        $data = $request->all();

        $credentials = [
            'email' => $data['email'],
            'password' => $data['password'],
        ];

        if(isset($data['remember'])){
            $remember = true;
        } else {
            $remember = false;
        }

        if (Auth::attempt($credentials, $remember)) {
            $user = Auth::user();
            if ($user->status === 3) {
                Auth::logout();
                Toastr::error('Votre compte est désactivé !');
            }
        } else {
            Toastr::error('Mot de Passe / Email erronés !');
        }

        return redirect(route('showHome'));
    }

    public function handleLogout()
    {
        Auth::logout();
        return redirect(route('showHome'));
    }

    public function showManagerUsersList()
    {
        return view('User::backOffice.list', [
            'users' => User::all(),
            'roles' => Role::all(),
            'announcement' => General::find(1)->announcement
        ]);
    }

    public function handleManagerBanUser($id)
    {
        $user = User::find($id);

        $user->status = 3;
        $user->save();

        Toastr::success('Utilisateur banni');
        return back();
    }

    public function handleManagerActivateUser($id)
    {
        $user = User::find($id);

        $user->status = 1;
        $user->save();

        Toastr::success('Utilisateur réactivé');
        return back();
    }

    public function handleManagerEditUser($id, Request $request)
    {
        $data = $request->all();

        $user = User::find($id);

        $user->phone = isset($data['phone']) ? $data['phone'] : $user->phone;
        $user->name = isset($data['name']) ? $data['name'] : $user->name;
        $user->password = isset($data['password']) ? bcrypt($data['password']) : $user->password;

        if (isset($data['photo'])) {
            if ($user->photo) {
                unlink($user->photo);
            }

            $photo = time() . '-' . str_random(5) . '.' . $data['photo']->getClientOriginalExtension();
            $fullImagePath = public_path('storage/uploads/users/' . $photo);
            Image::make($data['photo']->getRealPath())->save($fullImagePath);

            $user->photo = 'storage/uploads/users/' . $photo;
        }

        $user->roles()->detach();

        foreach($data['roles'] as $role){
            $user->roles()->attach($role);
        }

        $user->save();

        Toastr::success('Utilisateur modifié');
        return back();
    }

    public function handleSocialRedirect($provider)
    {
        switch ($provider) {
            case 'facebook':
                return Socialite::driver($provider)->redirect();
            default :
                return '';
        }
    }

    public function handleSocialCallback($provider, Request $request)
    {
        if (!$request->has('code') || $request->has('denied')) {
            Toastr::error('Nous n\'avons pas reçu les informations nécessaires depuis ' . $provider);
            return redirect(route('showHome'));
        }

        $providerData = Socialite::driver($provider)->stateless()->user();

        $user = User::where('provider_id', '=', $providerData->id)
            ->orWhere('email', '=', $providerData->email)
            ->first();

        if (!$user) {

            if ($providerData->name == null) {
                if ($providerData->email != null) {
                    $name = strtok($providerData->email, '@');
                } else {
                    $name = "Utilisateur Inconnu";
                }

            } else {
                $name = $providerData->name;
            }

            if ($providerData->email == null) {
                $email = $providerData->id . '@' . $provider . '.com';
            } else {
                $email = $providerData->email;
            }

            if ($providerData->avatar != null) {
                $imagePath = 'storage/uploads/users/' . $providerData->id . '-' . time() . '.png';
                file_put_contents($imagePath, file_get_contents('https://graph.facebook.com/'.$providerData->id.'/picture?type=large'));
            }

            $user = User::create([
                'provider_id' => $providerData->id,
                'provider' => $provider,
                'email' => $email,
                'name' => $name,
                'photo' => isset($imagePath) ? $imagePath : null,
                'status' => 1,
            ]);

            User::find($user->id)->roles()->attach(2);

            Auth::login($user);
            Toastr::success('Bienvenue parmis nous !');
            return redirect(route('showHome'));
        }

        if ($user->status === 3) {
            Auth::logout();
            Toastr::error('Votre compte est désactivé !');
            return back();
        } else {
            Auth::login($user);
            return redirect(route('showHome'));

        }
    }

    public function handleManagerAddUser(Request $request)
    {
        $data = $request->all();

        if(!isset($data['roles'])){
            Toastr::error('Vous n\'avez séléctionné aucun rôle !');
            return back();
        }

        if (isset($data['photo'])) {
            $photo = time() . '-' . str_random(5) . '.' . $data['photo']->getClientOriginalExtension();
            $fullImagePath = public_path('storage/uploads/users/' . $photo);
            Image::make($data['photo']->getRealPath())->save($fullImagePath);

            $photo = 'storage/uploads/users/' . $photo;
        }

        $user = User::create([
            'name' => $data['name'],
            'description' => $data['description'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => bcrypt($data['password']),
            'photo' => isset($photo) ? $photo : null,
        ]);

        foreach($data['roles'] as $role){
            $user->roles()->attach($role);
        }

        Toastr::success('Utilisateur créé avec succès !');
        return back();
    }

    public function showUserProfile(){
        if(!Auth::user()){
            Toastr::error('Vous devez vous connecter !');
            return redirect(route('showHome'));
        }

        return view('User::frontOffice.private.profile', [
            'services' => HospitalService::count(),
            'beds' => HospitalService::sum('beds'),
            'volunteers' => Volunteer::count(),
            'hospitals' => Hospital::count(),
        ]);
    }

    public function handleUpdateUserProfile(Request $request){
        if(!Auth::user()){
            Toastr::error('Vous devez vous connecter !');
            return redirect(route('showHome'));
        }

        $data = $request->all();

        $user = User::find(Auth::user()->id);

        if(isset($data['password']) and $data['password'] != $data['password_confirm']){
            Toastr::error('Les mots de passes ne sont pas identiques !');
            return back();
        } else {
            $user->password = bcrypt($data['password']);
        }

        $user->description = isset($data['description']) ? $data['description'] : $user->description;
        $user->phone = isset($data['phone']) ? $data['phone'] : $user->phone;
        $user->name = isset($data['name']) ? $data['name'] : $user->name;


        if (isset($data['photo'])) {
            // todo add photo remove. File::delete and unlink didn't work

            $photo = time() . '-' . str_random(5) . '.' . $data['photo']->getClientOriginalExtension();
            $fullImagePath = public_path('storage/uploads/users/' . $photo);
            Image::make($data['photo']->getRealPath())->save($fullImagePath);

            $user->photo = 'storage/uploads/users/' . $photo;
        }

        $user->save();

        Toastr::success('Votre profil a été modifié !');
        return back();
    }
}
