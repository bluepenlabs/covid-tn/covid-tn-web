@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - Mon Profil',
    'description' => 'Mon Profil- ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header',
        [
            'actual' => '',
            'cat' => ''
        ])
@endsection

@section('content')
    <link href="{{ asset('css/frontOffice/dashboard-style.css') }}" rel="stylesheet">

    <div class="content">
        <!--  section  -->
        <section class="parallax-section dashboard-header-sec gradient-bg" data-scrollax-parent="true">
            <div class="container">
                <div class="dashboard-breadcrumbs breadcrumbs"><a href="{{ route('showHome') }}">Accueil</a><span>Mon Profil</span></div>
                <!--Tariff Plan menu-->
                <div class="tfp-btn"><a href="{{ route('showContact') }}"> <strong>Besoin d'Aide ?</strong></a></div>
                <!--Tariff Plan menu end-->
                <div class="dashboard-header_conatiner fl-wrap dashboard-header_title">
                    <h1>Salut <span>{{ Auth::user()->name }}</span></h1>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="dashboard-header fl-wrap">
                <div class="container">
                    <div class="dashboard-header_conatiner fl-wrap">
                        <div class="dashboard-header-avatar">
                            <img @if(Auth::user()->photo) src="{{ asset(Auth::user()->photo) }}" @else src="{{ asset('img/unknown.png') }}" @endif alt="">
                        </div>
                        <div class="dashboard-header-stats-wrap" style="width:100%">
                            <div class="dashboard-header-stats">
                                <div class="" style="display: flex;">
                                    <div class="swiper-slide swiper-slide-active" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-hand-peace"></i>
                                            Volontaires
                                            <span>{{ $volunteers }}</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-active" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-hospital"></i>
                                            Hôpitaux
                                            <span>{{ $hospitals }}</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-active" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-hospital-symbol"></i>
                                            Services
                                            <span>{{ $services }}</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-active" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-bed"></i>
                                            Lits Disponibles
                                            <span>{{ $beds }}</span>
                                        </div>
                                    </div>
                                </div>
                                <!--  dashboard-header-stats  end -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
                <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
                <div class="circle-wrap" style="left: 120px; bottom: 120px; transform: translateZ(0px) translateY(35.0877px);" data-scrollax="properties: { translateY: '-200px' }">
                    <div class="circle_bg-bal circle_bg-bal_small"></div>
                </div>
                <div class="circle-wrap" style="right: 420px; bottom: -70px; transform: translateZ(0px) translateY(-26.3158px);" data-scrollax="properties: { translateY: '150px' }">
                    <div class="circle_bg-bal circle_bg-bal_big"></div>
                </div>
                <div class="circle-wrap" style="left: 420px; top: -70px; transform: translateZ(0px) translateY(-17.5439px);" data-scrollax="properties: { translateY: '100px' }">
                    <div class="circle_bg-bal circle_bg-bal_big"></div>
                </div>
                <div class="circle-wrap" style="left:40%;bottom:-70px;">
                    <div class="circle_bg-bal circle_bg-bal_middle"></div>
                </div>
                <div class="circle-wrap" style="right:40%;top:-10px;">
                    <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }" style="transform: translateZ(0px) translateY(61.4035px);"></div>
                </div>
                <div class="circle-wrap" style="right:55%;top:90px;">
                    <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }" style="transform: translateZ(0px) translateY(61.4035px);"></div>
                </div>
            </div>
        </section>
        <!--  section  end-->
        <!--  section  -->
        <section class="gray-bg main-dashboard-sec" id="sec1">
            <div class="container">
                <!--  dashboard-menu-->
                <div class="col-md-3">
                    <div class="mob-nav-content-btn color2-bg init-dsmen fl-wrap"><i class="fal fa-bars"></i> Menu</div>
                    <div class="clearfix"></div>
                    <div class="fixed-bar fl-wrap" id="dash_menu">
                        <div class="user-profile-menu-wrap fl-wrap block_box">
                            <!-- user-profile-menu-->
                            <div class="user-profile-menu">
                                <h3>Mon Menu</h3>
                                <ul class="no-list-style">
                                    <li><a class="user-profile-act" href="{{ route('showUserProfile') }}"><i class="fal fa-user"></i>Mon Profil</a></li>
                                    <li><a href="{{ route('showVolunteerUpdateProfile') }}"><i class="fal fa-user-nurse"></i>Profil Bénévole</a></li>
                                </ul>
                            </div>
                            <a href="{{ route('handleLogout') }}" class="logout_btn color2-bg">Déconnexion <i class="fas fa-sign-out"></i></a>
                        </div>
                    </div>
                    <a class="back-tofilters color2-bg custom-scroll-link fl-wrap" href="#dash_menu" style="z-index: 12;">Retour au Menu<i class="fas fa-caret-up"></i></a><div></div>
                    <div class="clearfix"></div>
                </div>
                <!-- dashboard-menu  end-->
                <!-- dashboard content-->
                <div class="col-md-9">
                    <form method="post" action="{{ route('handleUpdateUserProfile') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="dashboard-title   fl-wrap">
                            <h3>Informations Générales</h3>
                        </div>
                        <!-- profile-edit-container-->
                        <div class="profile-edit-container fl-wrap block_box">
                            <div class="custom-form">
                                <label>Nom <i class="fal fa-briefcase"></i></label>
                                <input type="text" name="name" value="{{ Auth::user()->name}}">
                                <label>Email <i class="fal fa-mailbox"></i></label>
                                <input type="email" name="email" value="{{ Auth::user()->email }}">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Description <i class="fal fa-text"></i></label>
                                        <input type="text" name="description" value="{{ Auth::user()->description }}">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Téléphone <i class="fal fa-phone"></i></label>
                                        <input name="phone" type="number" value="{{ Auth::user()->phone }}">
                                    </div>
                                </div>
                                <label>Photo <i class="fal fa-photo-video"></i></label>
                                <input type="file" name="photo">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Nouveau Mot de Passe <i class="fal fa-user-secret"></i></label>
                                        <input type="password" name="password">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Confirmer Mot de Passe <i class="fal fa-user-secret"></i></label>
                                        <input name="password_confirm" type="password">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- profile-edit-container end-->
                        <button type="submit" class="btn color2-bg" style="float: right;border-color: transparent;">Enregistrer<i class="fal fa-save"></i></button>
                    </form>
                </div>
                <!-- dashboard content end-->
            </div>
        </section>
        <!--  section  end-->
        <div class="limit-box fl-wrap"></div>
    </div>

@endsection

