@extends('backOffice.layout')

@section('head')
    @include('backOffice.inc.head',
    ['title' => config('app.name') . ' - Gestion des Utilisateurs',
    'description' => 'Gestion des Utilisateurs - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('backOffice.inc.header')
@endsection

@section('sidebar')
    @include('backOffice.inc.sidebar', [
        'current' => 'users'
    ])
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable.js') }}"></script>

    <script type="text/javascript">

        /* Datatables responsive */

        $(document).ready(function () {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "Rechercher...");
        });

    </script>

    <div class="breadcrumb">
        <h1>Utilisateurs</h1>
        <ul>
            <li><a href="{{ route('showManagerHome') }}">Tableau de Bord</a></li>
            <li>Synthèse</li>
        </ul>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row mb-4">

        <div class="col-md-8 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <div class="card-title mb-3">Synthèse</div>

                    <table id="datatable-responsive"
                           class="display table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Téléphone</th>
                            <th>Rôle</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Téléphone</th>
                            <th>Rôles</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>@foreach($user->roles as $role) {{ $role->title }} @endforeach</td>
                                <td style="text-align: center">
                                    <span class="badge badge-pill  badge-outline-success p-2 m-1" title="Modifier" data-toggle="modal"
                                          data-target="#edit-{{ $user->id }}"><i class="nav-icon i-Edit"></i></span>
                                    @if($user->status == 1)
                                        <a href="{{ route('handleManagerBanUser', ['id' => $user->id]) }}"><span
                                                    title="Bannir" class="badge badge-pill  badge-outline-warning p-2 m-1"><i class="nav-icon i-Communication-Tower-2"></i></span></a>
                                    @else
                                        <a href="{{ route('handleManagerActivateUser', ['id' => $user->id]) }}"><span
                                                    title="Activer" class="badge badge-pill  badge-outline-info p-2 m-1"><i
                                                        class="nav-icon i-Communication-Tower"></i></span></a>
                                    @endif
                                </td>
                            </tr>
                            <div class="modal fade" id="edit-{{ $user->id }}" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form method="post" enctype="multipart/form-data"
                                              action="{{ route('handleManagerEditUser', ['id' => $user->id]) }}">
                                            {{ csrf_field() }}
                                            <div class="modal-header">
                                                <h5 class="modal-title">Modifier {{ $user->name }}</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">×
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="control-label">Nom</label>
                                                        <input name="name" required class="form-control"
                                                               value="{{ $user->name }}">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Email</label>
                                                        <input name="email" required class="form-control"
                                                               value="{{ $user->email }}">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Nouveau Mot de Passe</label>
                                                        <input name="password" class="form-control" type="password">
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label class="control-label">Description</label>
                                                        <input name="description" class="form-control"
                                                               value="{{ $user->description }}">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Téléphone</label>
                                                        <input name="phone" class="form-control" placeholder=""
                                                               type="number" value="{{ $user->phone }}">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Photo @if($user->photo) <sup><a
                                                                        target="_blank"
                                                                        href="{{ asset($user->photo) }}">Voir</a>
                                                            </sup>@endif</label>
                                                        <input name="photo" class="form-control" type="file">
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label class="control-label">Rôles</label><br>
                                                        @foreach($roles as $role)
                                                            <input type="checkbox" name="roles[]" autocomplete="off"
                                                                   @if(checkUnspecificRole($user, $role->id)) checked="checked"
                                                                   @endif value="{{ $role->id }}"> {{ $role->title }}
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Annuler
                                                </button>
                                                <button type="submit" class="btn btn-primary">Envoyer</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-md-4 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <div class="card-title mb-3">Nouvel Utilisateur</div>
                    <form enctype="multipart/form-data" role="form" action="{{ route('handleManagerAddUser') }}"
                          method="post">
                        {{ csrf_field() }}
                        <div class="row">

                            <div class="col-md-12 form-group mb-3">
                                <label for="firstName1">Nom</label>
                                <input class="form-control" required value="" name="name" type="text">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="firstName1">Mot de Passe</label>
                                <input class="form-control" required value="" name="password" type="password">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="firstName1">Email</label>
                                <input class="form-control" required value="" name="email" type="email">
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="firstName1">Téléphone</label>
                                <input class="form-control" value="" name="phone" type="number">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="firstName1">Description</label>
                                <input class="form-control" value="" name="description" type="text">
                            </div>

                            <div class="col-md-12 form-group mb-3">
                                <label for="firstName1">Photo</label>
                                <input class="form-control" value="" name="photo" type="file">
                            </div>

                            <div class="col-md-12 form-group mb-3">
                                <label for="firstName1">Rôles</label><br>
                                @foreach($roles as $role)
                                    <input type="checkbox" name="roles[]" autocomplete="off"
                                           @if(checkUnspecificRole($user, $role->id)) checked="checked"
                                           @endif value="{{ $role->id }}"> {{ $role->title }}
                                @endforeach
                            </div>

                            <div class="col-md-12 pull-right">
                                <button type="reset" class="btn btn-warning">Réinitialiser</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

