<?php

namespace App\Modules\General\Models;

use Illuminate\Database\Eloquent\Model;

class StatisticsGender extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'statistics_gender';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'deaths',
        'recovered',
        'cases',
        'gender',
        'created_at',
    ];

}
