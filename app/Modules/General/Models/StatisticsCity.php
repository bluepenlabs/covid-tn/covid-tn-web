<?php

namespace App\Modules\General\Models;

use Illuminate\Database\Eloquent\Model;

class StatisticsCity extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'statistics_city';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'deaths',
        'cases',
        'city_id',
        'created_at',
    ];

    public function city()
    {
        return $this->hasOne('App\Modules\General\Models\City', 'id', 'city_id');
    }
}
