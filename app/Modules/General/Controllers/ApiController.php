<?php

namespace App\Modules\General\Controllers;

use App\Modules\General\Models\City;
use App\Modules\General\Models\StatisticsCity;
use App\Modules\General\Models\StatisticsGlobal;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;

class ApiController extends Controller
{

    public function getGeneralStatisticsByDate($date){

        $date = Carbon::createFromFormat('Y-m-d', $date)->format('Y-m-d');

        $statistics = StatisticsGlobal::select('deaths', 'recovered', 'cases', 'tests')->whereDate('created_at', $date)->first();

        if(!$statistics or count($statistics) == 0){
            return response()->json(['data' => [], 'status' => ['message' => 'No data found', 'code' => 404]]);
        }

        return response()->json(['data' => $statistics, 'error' => ['message' => '', 'code' => 200]]);
    }

    public function getStatisticsCities($limit = 10, $offset = 0){
        if($limit > 100){ $limit = 100; }

        $cities = City::select('id', 'title')
            ->limit($limit)
            ->skip($offset * $limit)->get();

        foreach($cities as $city){
            $city->id = $city->id * 1021;
        }

        if(!$cities or count($cities) == 0){
            return response()->json(['data' => [], 'status' => ['message' => 'No data found', 'code' => 404]]);
        }

        return response()->json(['data' => $cities, 'error' => ['message' => '', 'code' => 200]]);
    }

    public function getCityStatisticsByDate($id, $date){

        $decryptedId = $id / 1021;

        $date = Carbon::createFromFormat('Y-m-d', $date)->format('Y-m-d');

        $statistics = StatisticsCity::whereDate('created_at', $date)->where('city_id', $decryptedId)->select('deaths', 'cases')->get();

        if(!$statistics or count($statistics) == 0){
            return response()->json(['data' => [], 'status' => ['message' => 'No data found', 'code' => 404]]);
        }

        return response()->json(['data' => $statistics, 'error' => ['message' => '', 'code' => 200]]);
    }

}
