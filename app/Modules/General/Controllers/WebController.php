<?php

namespace App\Modules\General\Controllers;

use App\Modules\General\Models\City;
use App\Modules\General\Models\General;
use App\Modules\General\Models\StatisticsCity;
use App\Modules\General\Models\StatisticsGender;
use App\Modules\General\Models\StatisticsGlobal;
use App\Modules\Hospital\Models\Hospital;
use App\Modules\Hospital\Models\HospitalService;
use App\Modules\Hospital\Models\Product;
use App\Modules\Volunteer\Models\Category;
use App\Modules\Volunteer\Models\Organisation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Kamaln7\Toastr\Facades\Toastr;
use PulkitJalan\Google\Facades\Google;

class WebController extends Controller
{

    public static function scheduleSyncStats(){
        $sheets = Google::make('sheets');
        $spreadsheet_id = "18aDPBcPWQ2BuqbKpAoAQKlub07revrbSKnaexB6gb4o";

        $daysNumber = now()->diffInDays(Carbon::createFromDate("2020", "03", "02")) + 2 ;

        $dates = $sheets->spreadsheets_values->get($spreadsheet_id,"A3:A".$daysNumber)->getValues();
        $cases = $sheets->spreadsheets_values->get($spreadsheet_id,"D3:D".$daysNumber)->getValues();
        $deaths = $sheets->spreadsheets_values->get($spreadsheet_id,"G3:G".$daysNumber)->getValues();
        $tests = $sheets->spreadsheets_values->get($spreadsheet_id,"F3:F".$daysNumber)->getValues();
        $recovered = $sheets->spreadsheets_values->get($spreadsheet_id,"H3:H".$daysNumber)->getValues();

        $tunisCases = $sheets->spreadsheets_values->get($spreadsheet_id,"J3:J".$daysNumber)->getValues();
        $arianaCases = $sheets->spreadsheets_values->get($spreadsheet_id,"K3:K".$daysNumber)->getValues();
        $bejaCases = $sheets->spreadsheets_values->get($spreadsheet_id,"L3:L".$daysNumber)->getValues();
        $benArousCases = $sheets->spreadsheets_values->get($spreadsheet_id,"M3:M".$daysNumber)->getValues();
        $bizerteCases = $sheets->spreadsheets_values->get($spreadsheet_id,"N3:N".$daysNumber)->getValues();
        $gabesCases = $sheets->spreadsheets_values->get($spreadsheet_id,"G3:G".$daysNumber)->getValues();
        $gafsaCases = $sheets->spreadsheets_values->get($spreadsheet_id,"P3:P".$daysNumber)->getValues();
        $jendoubaCases = $sheets->spreadsheets_values->get($spreadsheet_id,"Q3:Q".$daysNumber)->getValues();
        $kairouanCases = $sheets->spreadsheets_values->get($spreadsheet_id,"R3:R".$daysNumber)->getValues();
        $kasserineCases = $sheets->spreadsheets_values->get($spreadsheet_id,"S3:S".$daysNumber)->getValues();
        $kebiliCases = $sheets->spreadsheets_values->get($spreadsheet_id,"T3:T".$daysNumber)->getValues();
        $kefCases = $sheets->spreadsheets_values->get($spreadsheet_id,"U3:U".$daysNumber)->getValues();
        $mahdiaCases = $sheets->spreadsheets_values->get($spreadsheet_id,"V3:V".$daysNumber)->getValues();
        $manoubaCases = $sheets->spreadsheets_values->get($spreadsheet_id,"W3:W".$daysNumber)->getValues();
        $medenineCases = $sheets->spreadsheets_values->get($spreadsheet_id,"X3:X".$daysNumber)->getValues();
        $monastirCases = $sheets->spreadsheets_values->get($spreadsheet_id,"Y3:Y".$daysNumber)->getValues();
        $nabeulCases = $sheets->spreadsheets_values->get($spreadsheet_id,"Z3:Z".$daysNumber)->getValues();
        $sfaxCases = $sheets->spreadsheets_values->get($spreadsheet_id,"AA3:AA".$daysNumber)->getValues();
        $bouzidCases = $sheets->spreadsheets_values->get($spreadsheet_id,"AB3:AB".$daysNumber)->getValues();
        $silianaCases = $sheets->spreadsheets_values->get($spreadsheet_id,"AC3:AC".$daysNumber)->getValues();
        $sousseCases = $sheets->spreadsheets_values->get($spreadsheet_id,"AD3:AD".$daysNumber)->getValues();
        $tataouineCases = $sheets->spreadsheets_values->get($spreadsheet_id,"AE3:AE".$daysNumber)->getValues();
        $tozeurCases = $sheets->spreadsheets_values->get($spreadsheet_id,"AF3:AF".$daysNumber)->getValues();
        $zaghouanCases = $sheets->spreadsheets_values->get($spreadsheet_id,"AG3:AG".$daysNumber)->getValues();

        $tunisDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AH3:AH".$daysNumber)->getValues();
        $arianaDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AI3:AI".$daysNumber)->getValues();
        $bejaDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AJ3:AJ".$daysNumber)->getValues();
        $benArousDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AK3:AK".$daysNumber)->getValues();
        $bizerteDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AL3:AL".$daysNumber)->getValues();
        $gabesDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AM3:AM".$daysNumber)->getValues();
        $gafsaDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AN3:AN".$daysNumber)->getValues();
        $jendoubaDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AO3:AO".$daysNumber)->getValues();
        $kairouanDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AP3:AP".$daysNumber)->getValues();
        $kasserineDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AQ3:AQ".$daysNumber)->getValues();
        $kebiliDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AR3:AR".$daysNumber)->getValues();
        $kefDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AS3:AS".$daysNumber)->getValues();
        $mahdiaDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AT3:AT".$daysNumber)->getValues();
        $manoubaDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AU3:AU".$daysNumber)->getValues();
        $medenineDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AV3:AV".$daysNumber)->getValues();
        $monastirDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AW3:AW".$daysNumber)->getValues();
        $nabeulDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AX3:AX".$daysNumber)->getValues();
        $sfaxDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AY3:AY".$daysNumber)->getValues();
        $bouzidDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"AZ3:AZ".$daysNumber)->getValues();
        $silianaDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"BA3:BA".$daysNumber)->getValues();
        $sousseDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"BB3:BB".$daysNumber)->getValues();
        $tataouineDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"BC3:BC".$daysNumber)->getValues();
        $tozeurDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"BD3:BD".$daysNumber)->getValues();
        $zaghouanDeaths = $sheets->spreadsheets_values->get($spreadsheet_id,"BE3:BE".$daysNumber)->getValues();

        for($i=0; $i < count($dates); $i++) {

            if(isset($recovered[$i][0])){ $dayRecovered = $recovered[$i][0]; } elseif(isset($recovered[$i - 1][0])){ $dayRecovered = $recovered[$i - 1][0]; } else { $dayRecovered = 0; }
            if(isset($deaths[$i][0])){ $dayDeaths = $deaths[$i][0]; } elseif(isset($deaths[$i - 1][0])){ $dayDeaths = $deaths[$i - 1][0]; } else { $dayDeaths = 0; }
            if(isset($cases[$i][0])){ $dayCases = $cases[$i][0]; } elseif(isset($cases[$i - 1][0])){ $dayCases = $cases[$i - 1][0]; } else { $dayCases = 0; }
            if(isset($tests[$i][0])){ $dayTests = $tests[$i][0]; } elseif(isset($tests[$i - 1][0])){ $dayTests = $tests[$i - 1][0]; } else { $dayTests = 0; }

            if(isset($tunisCases[$i][0])){ $dayCasesTunis = $tunisCases[$i][0]; } elseif(isset($tunisCases[$i - 1][0])){ $dayCasesTunis = $tunisCases[$i - 1][0]; } else { $dayCasesTunis = 0; }
            if(isset($arianaCases[$i][0])){ $dayCasesAriana = $arianaCases[$i][0]; } elseif(isset($arianaCases[$i - 1][0])){ $dayCasesAriana = $arianaCases[$i - 1][0]; } else { $dayCasesAriana = 0; }
            if(isset($bejaCases[$i][0])){ $dayCasesBeja = $bejaCases[$i][0]; } elseif(isset($bejaCases[$i - 1][0])){ $dayCasesBeja = $bejaCases[$i - 1][0]; } else { $dayCasesBeja = 0; }
            if(isset($benArousCases[$i][0])){ $dayCasesBenArous = $benArousCases[$i][0]; } elseif(isset($benArousCases[$i - 1][0])){ $dayCasesBenArous = $benArousCases[$i - 1][0]; } else { $dayCasesBenArous = 0; }
            if(isset($bizerteCases[$i][0])){ $dayCasesBizerte = $bizerteCases[$i][0]; } elseif(isset($bizerteCases[$i - 1][0])){ $dayCasesBizerte = $bizerteCases[$i - 1][0]; } else { $dayCasesBizerte = 0; }
            if(isset($gabesCases[$i][0])){ $dayCasesGabes = $gabesCases[$i][0]; } elseif(isset($gabesCases[$i - 1][0])){ $dayCasesGabes = $gabesCases[$i - 1][0]; } else { $dayCasesGabes = 0; }
            if(isset($gafsaCases[$i][0])){ $dayCasesGafsa = $gafsaCases[$i][0]; } elseif(isset($gafsaCases[$i - 1][0])){ $dayCasesGafsa = $gafsaCases[$i - 1][0]; } else { $dayCasesGafsa = 0; }
            if(isset($jendoubaCases[$i][0])){ $dayCasesJendouba = $jendoubaCases[$i][0]; } elseif(isset($jendoubaCases[$i - 1][0])){ $dayCasesJendouba = $jendoubaCases[$i - 1][0]; } else { $dayCasesJendouba = 0; }
            if(isset($kairouanCases[$i][0])){ $dayCasesKairouan = $kairouanCases[$i][0]; } elseif(isset($kairouanCases[$i - 1][0])){ $dayCasesKairouan = $kairouanCases[$i - 1][0]; } else { $dayCasesKairouan = 0; }
            if(isset($kasserineCases[$i][0])){ $dayCasesKasserine = $kasserineCases[$i][0]; } elseif(isset($kasserineCases[$i - 1][0])){ $dayCasesKasserine = $kasserineCases[$i - 1][0]; } else { $dayCasesKasserine = 0; }
            if(isset($kebiliCases[$i][0])){ $dayCasesKebili = $kebiliCases[$i][0]; } elseif(isset($kebiliCases[$i - 1][0])){ $dayCasesKebili = $kebiliCases[$i - 1][0]; } else { $dayCasesKebili = 0; }
            if(isset($kefCases[$i][0])){ $dayCasesKef = $kefCases[$i][0]; } elseif(isset($kefCases[$i - 1][0])){ $dayCasesKef = $kefCases[$i - 1][0]; } else { $dayCasesKef = 0; }
            if(isset($mahdiaCases[$i][0])){ $dayCasesMahdia = $mahdiaCases[$i][0]; } elseif(isset($mahdiaCases[$i - 1][0])){ $dayCasesMahdia = $mahdiaCases[$i - 1][0]; } else { $dayCasesMahdia = 0; }
            if(isset($manoubaCases[$i][0])){ $dayCasesManouba = $manoubaCases[$i][0]; } elseif(isset($manoubaCases[$i - 1][0])){ $dayCasesManouba = $manoubaCases[$i - 1][0]; } else { $dayCasesManouba = 0; }
            if(isset($medenineCases[$i][0])){ $dayCasesMedenine = $medenineCases[$i][0]; } elseif(isset($medenineCases[$i - 1][0])){ $dayCasesMedenine = $medenineCases[$i - 1][0]; } else { $dayCasesMedenine = 0; }
            if(isset($monastirCases[$i][0])){ $dayCasesMonastir = $monastirCases[$i][0]; } elseif(isset($monastirCases[$i - 1][0])){ $dayCasesMonastir = $monastirCases[$i - 1][0]; } else { $dayCasesMonastir = 0; }
            if(isset($nabeulCases[$i][0])){ $dayCasesNabeul = $nabeulCases[$i][0]; } elseif(isset($nabeulCases[$i - 1][0])){ $dayCasesNabeul = $nabeulCases[$i - 1][0]; } else { $dayCasesNabeul = 0; }
            if(isset($sfaxCases[$i][0])){ $dayCasesSfax = $sfaxCases[$i][0]; } elseif(isset($sfaxCases[$i - 1][0])){ $dayCasesSfax = $sfaxCases[$i - 1][0]; } else { $dayCasesSfax = 0; }
            if(isset($bouzidCases[$i][0])){ $dayCasesBouzid = $bouzidCases[$i][0]; } elseif(isset($bouzidCases[$i - 1][0])){ $dayCasesBouzid = $bouzidCases[$i - 1][0]; } else { $dayCasesBouzid = 0; }
            if(isset($silianaCases[$i][0])){ $dayCasesSiliana = $silianaCases[$i][0]; } elseif(isset($silianaCases[$i - 1][0])){ $dayCasesSiliana = $silianaCases[$i - 1][0]; } else { $dayCasesSiliana = 0; }
            if(isset($sousseCases[$i][0])){ $dayCasesSousse = $sousseCases[$i][0]; } elseif(isset($sousseCases[$i - 1][0])){ $dayCasesSousse = $sousseCases[$i - 1][0]; } else { $dayCasesSousse = 0; }
            if(isset($tataouineCases[$i][0])){ $dayCasesTataouine = $tataouineCases[$i][0]; } elseif(isset($tataouineCases[$i - 1][0])){ $dayCasesTataouine = $tataouineCases[$i - 1][0]; } else { $dayCasesTataouine = 0; }
            if(isset($tozeurCases[$i][0])){ $dayCasesTouzeur = $tozeurCases[$i][0]; } elseif(isset($tozeurCases[$i - 1][0])){ $dayCasesTouzeur = $tozeurCases[$i - 1][0]; } else { $dayCasesTouzeur = 0; }
            if(isset($zaghouanCases[$i][0])){ $dayCasesZaghouan = $zaghouanCases[$i][0]; } elseif(isset($zaghouanCases[$i - 1][0])){ $dayCasesZaghouan = $zaghouanCases[$i - 1][0]; } else { $dayCasesZaghouan = 0; }

            if(isset($tunisDeaths[$i][0])){ $dayDeathsTunis = $tunisDeaths[$i][0]; } elseif(isset($tunisDeaths[$i - 1][0])){ $dayDeathsTunis = $tunisDeaths[$i - 1][0]; } else { $dayDeathsTunis = 0; }
            if(isset($arianaDeaths[$i][0])){ $dayDeathsAriana = $arianaDeaths[$i][0]; } elseif(isset($arianaDeaths[$i - 1][0])){ $dayDeathsAriana = $arianaDeaths[$i - 1][0]; } else { $dayDeathsAriana = 0; }
            if(isset($bejaDeaths[$i][0])){ $dayDeathsBeja = $bejaDeaths[$i][0]; } elseif(isset($bejaDeaths[$i - 1][0])){ $dayDeathsBeja = $bejaDeaths[$i - 1][0]; } else { $dayDeathsBeja = 0; }
            if(isset($benArousDeaths[$i][0])){ $dayDeathsBenArous = $benArousDeaths[$i][0]; } elseif(isset($benArousDeaths[$i - 1][0])){ $dayDeathsBenArous = $benArousDeaths[$i - 1][0]; } else { $dayDeathsBenArous = 0; }
            if(isset($bizerteDeaths[$i][0])){ $dayDeathsBizerte = $bizerteDeaths[$i][0]; } elseif(isset($bizerteDeaths[$i - 1][0])){ $dayDeathsBizerte = $bizerteDeaths[$i - 1][0]; } else { $dayDeathsBizerte = 0; }
            if(isset($gabesDeaths[$i][0])){ $dayDeathsGabes = $gabesDeaths[$i][0]; } elseif(isset($gabesDeaths[$i - 1][0])){ $dayDeathsGabes = $gabesDeaths[$i - 1][0]; } else { $dayDeathsGabes = 0; }
            if(isset($gafsaDeaths[$i][0])){ $dayDeathsGafsa = $gafsaDeaths[$i][0]; } elseif(isset($gafsaDeaths[$i - 1][0])){ $dayDeathsGafsa = $gafsaDeaths[$i - 1][0]; } else { $dayDeathsGafsa = 0; }
            if(isset($jendoubaDeaths[$i][0])){ $dayDeathsJendouba = $jendoubaDeaths[$i][0]; } elseif(isset($jendoubaDeaths[$i - 1][0])){ $dayDeathsJendouba = $jendoubaDeaths[$i - 1][0]; } else { $dayDeathsJendouba = 0; }
            if(isset($kairouanDeaths[$i][0])){ $dayDeathsKairouan = $kairouanDeaths[$i][0]; } elseif(isset($kairouanDeaths[$i - 1][0])){ $dayDeathsKairouan = $kairouanDeaths[$i - 1][0]; } else { $dayDeathsKairouan = 0; }
            if(isset($kasserineDeaths[$i][0])){ $dayDeathsKasserine = $kasserineDeaths[$i][0]; } elseif(isset($kasserineDeaths[$i - 1][0])){ $dayDeathsKasserine = $kasserineDeaths[$i - 1][0]; } else { $dayDeathsKasserine = 0; }
            if(isset($kebiliDeaths[$i][0])){ $dayDeathsKebili = $kebiliDeaths[$i][0]; } elseif(isset($kebiliDeaths[$i - 1][0])){ $dayDeathsKebili = $kebiliDeaths[$i - 1][0]; } else { $dayDeathsKebili = 0; }
            if(isset($kefDeaths[$i][0])){ $dayDeathsKef = $kefDeaths[$i][0]; } elseif(isset($kefDeaths[$i - 1][0])){ $dayDeathsKef = $kefDeaths[$i - 1][0]; } else { $dayDeathsKef = 0; }
            if(isset($mahdiaDeaths[$i][0])){ $dayDeathsMahdia = $mahdiaDeaths[$i][0]; } elseif(isset($mahdiaDeaths[$i - 1][0])){ $dayDeathsMahdia = $mahdiaDeaths[$i - 1][0]; } else { $dayDeathsMahdia = 0; }
            if(isset($manoubaDeaths[$i][0])){ $dayDeathsManouba = $manoubaDeaths[$i][0]; } elseif(isset($manoubaDeaths[$i - 1][0])){ $dayDeathsManouba = $manoubaDeaths[$i - 1][0]; } else { $dayDeathsManouba = 0; }
            if(isset($medenineDeaths[$i][0])){ $dayDeathsMedenine = $medenineDeaths[$i][0]; } elseif(isset($medenineDeaths[$i - 1][0])){ $dayDeathsMedenine = $medenineDeaths[$i - 1][0]; } else { $dayDeathsMedenine = 0; }
            if(isset($monastirDeaths[$i][0])){ $dayDeathsMonastir = $monastirDeaths[$i][0]; } elseif(isset($monastirDeaths[$i - 1][0])){ $dayDeathsMonastir = $monastirDeaths[$i - 1][0]; } else { $dayDeathsMonastir = 0; }
            if(isset($nabeulDeaths[$i][0])){ $dayDeathsNabeul = $nabeulDeaths[$i][0]; } elseif(isset($nabeulDeaths[$i - 1][0])){ $dayDeathsNabeul = $nabeulDeaths[$i - 1][0]; } else { $dayDeathsNabeul = 0; }
            if(isset($sfaxDeaths[$i][0])){ $dayDeathsSfax = $sfaxDeaths[$i][0]; } elseif(isset($sfaxDeaths[$i - 1][0])){ $dayDeathsSfax = $sfaxDeaths[$i - 1][0]; } else { $dayDeathsSfax = 0; }
            if(isset($bouzidDeaths[$i][0])){ $dayDeathsBouzid = $bouzidDeaths[$i][0]; } elseif(isset($bouzidDeaths[$i - 1][0])){ $dayDeathsBouzid = $bouzidDeaths[$i - 1][0]; } else { $dayDeathsBouzid = 0; }
            if(isset($silianaDeaths[$i][0])){ $dayDeathsSiliana = $silianaDeaths[$i][0]; } elseif(isset($silianaDeaths[$i - 1][0])){ $dayDeathsSiliana = $silianaDeaths[$i - 1][0]; } else { $dayDeathsSiliana = 0; }
            if(isset($sousseDeaths[$i][0])){ $dayDeathsSousse = $sousseDeaths[$i][0]; } elseif(isset($sousseDeaths[$i - 1][0])){ $dayDeathsSousse = $sousseDeaths[$i - 1][0]; } else { $dayDeathsSousse = 0; }
            if(isset($tataouineDeaths[$i][0])){ $dayDeathsTataouine = $tataouineDeaths[$i][0]; } elseif(isset($tataouineDeaths[$i - 1][0])){ $dayDeathsTataouine = $tataouineDeaths[$i - 1][0]; } else { $dayDeathsTataouine = 0; }
            if(isset($tozeurDeaths[$i][0])){ $dayDeathsTouzeur = $tozeurDeaths[$i][0]; } elseif(isset($tozeurDeaths[$i - 1][0])){ $dayDeathsTouzeur = $tozeurDeaths[$i - 1][0]; } else { $dayDeathsTouzeur = 0; }
            if(isset($zaghouanDeaths[$i][0])){ $dayDeathsZaghouan = $zaghouanDeaths[$i][0]; } elseif(isset($zaghouanDeaths[$i - 1][0])){ $dayDeathsZaghouan = $zaghouanDeaths[$i - 1][0]; } else { $dayDeathsZaghouan = 0; }

            StatisticsGlobal::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d')
            ],[
                'deaths' => $dayDeaths,
                'recovered' => $dayRecovered,
                'cases' => $dayCases,
                'tests' => $dayTests,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 23
            ],[
                'deaths' => $dayDeathsTunis,
                'cases' => $dayCasesTunis,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 1
            ],[
                'deaths' => $dayDeathsAriana,
                'cases' => $dayCasesAriana,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 2
            ],[
                'deaths' => $dayDeathsBeja,
                'cases' => $dayCasesBeja,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 3
            ],[
                'deaths' => $dayDeathsBenArous,
                'cases' => $dayCasesBenArous,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 4
            ],[
                'deaths' => $dayDeathsBizerte,
                'cases' => $dayCasesBizerte,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 5
            ],[
                'deaths' => $dayDeathsGabes,
                'cases' => $dayCasesGabes,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 6
            ],[
                'deaths' => $dayDeathsGafsa,
                'cases' => $dayCasesGafsa,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 7
            ],[
                'deaths' => $dayDeathsJendouba,
                'cases' => $dayCasesJendouba,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 8
            ],[
                'deaths' => $dayDeathsKairouan,
                'cases' => $dayCasesKairouan,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 9
            ],[
                'deaths' => $dayDeathsKasserine,
                'cases' => $dayCasesKasserine,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 10
            ],[
                'deaths' => $dayDeathsKebili,
                'cases' => $dayCasesKebili,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 11
            ],[
                'deaths' => $dayDeathsKef,
                'cases' => $dayCasesKef,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 12
            ],[
                'deaths' => $dayDeathsMahdia,
                'cases' => $dayCasesMahdia,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 13
            ],[
                'deaths' => $dayDeathsManouba,
                'cases' => $dayCasesManouba,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 14
            ],[
                'deaths' => $dayDeathsMedenine,
                'cases' => $dayCasesMedenine,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 15
            ],[
                'deaths' => $dayDeathsMonastir,
                'cases' => $dayCasesMonastir,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 16
            ],[
                'deaths' => $dayDeathsNabeul,
                'cases' => $dayCasesNabeul,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 17
            ],[
                'deaths' => $dayDeathsSfax,
                'cases' => $dayCasesSfax,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 18
            ],[
                'deaths' => $dayDeathsBouzid,
                'cases' => $dayCasesBouzid,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 19
            ],[
                'deaths' => $dayDeathsSiliana,
                'cases' => $dayCasesSiliana,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 20
            ],[
                'deaths' => $dayDeathsSousse,
                'cases' => $dayCasesSousse,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 21
            ],[
                'deaths' => $dayDeathsTataouine,
                'cases' => $dayCasesTataouine,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 22
            ],[
                'deaths' => $dayDeathsTouzeur,
                'cases' => $dayCasesTouzeur,
            ]);

            StatisticsCity::updateOrCreate([
                'created_at' => Carbon::createFromFormat('Y-m-d', $dates[$i][0])->format('Y-m-d'),
                'city_id' => 24
            ],[
                'deaths' => $dayDeathsZaghouan,
                'cases' => $dayCasesZaghouan,
            ]);

        }
    }


    public function showManagerHome(){
        return view('General::backOffice.home', [

        ]);
    }

    public function showHome(){

        $hospitals = Hospital::all();

        foreach($hospitals as $hospital){
            $need = [];
            foreach($hospital->services as $service){
                foreach($service->products as $product){
                    $need[$product->product->id] = isset($need[$product->product->id]) ? $need[$product->product->id] : $product->product;
                    if($product->auto_renew == 1){
                        $stock = $product->dayStock(today())->first();
                        if($stock){
                            $need[$product->product->id]->stock = $need[$product->product->id]->stock + ($product->needed - $stock->quantity);
                        } else {
                            $need[$product->product->id]->stock = $need[$product->product->id]->stock + $product->needed;
                        }
                    } else {
                        $stock = $product->stocks->sum('quantity');
                        if($stock){
                            $need[$product->product->id]->stock = $need[$product->product->id]->stock + ($product->needed - $stock);
                        } else {
                            $need[$product->product->id]->stock = $need[$product->product->id]->stock + $product->needed;
                        }
                    }

                }
            }
            $hospital->needs = collect($need);
        }

        return view('General::frontOffice.public.home', [
            'hospitals' => $hospitals->take(10),
            'hospitalsOnMap' => $hospitals,
            'cities' => City::all(),
            'organisations' => Organisation::all(),
            'services' => HospitalService::count(),
            'products' => Product::all(),
            'announcement' => General::all()[0]->announcement,
            'volunteersCategories' => Category::all(),
            'offset' => 0,
        ]);
    }

    public function showStatistics(){
        $statistics = StatisticsGlobal::orderBy('created_at', 'asc')->get();
        $citiesStatistics = StatisticsCity::all();

        $separatedDeathsStats = $separatedTestsStats =$separatedCasesStats = $separatedRecoveredStats = [];

        $citiesFormattedStats =[];
        foreach($citiesStatistics as $statistic){
            $citiesFormattedStats[$statistic->city->title][$statistic->created_at->format('Y-m-d')] = $statistic;
        }

        for($i = 0; $i < count($statistics); $i++){
            $separatedCasesStats[$statistics[$i]->created_at->format('Y-m-d')] = $statistics[$i]->cases - (isset($statistics[$i - 1]) ? $statistics[$i - 1]->cases : 0);
            $separatedDeathsStats[$statistics[$i]->created_at->format('Y-m-d')] = $statistics[$i]->deaths - (isset($statistics[$i - 1]) ? $statistics[$i - 1]->deaths : 0);
            $separatedRecoveredStats[$statistics[$i]->created_at->format('Y-m-d')] = $statistics[$i]->recovered - (isset($statistics[$i - 1]) ? $statistics[$i - 1]->recovered : 0);
            $separatedTestsStats[$statistics[$i]->created_at->format('Y-m-d')] = $statistics[$i]->tests - (isset($statistics[$i - 1]) ? $statistics[$i - 1]->tests : 0);
        }

        return view('General::frontOffice.public.stats', [
            'statistics' => $statistics,
            'citiesFormattedStats' => $citiesFormattedStats,
            'separatedCasesStats' => $separatedCasesStats,
            'separatedDeathsStats' => $separatedDeathsStats,
            'separatedRecoveredStats' => $separatedRecoveredStats,
            'separatedTestsStats' => $separatedTestsStats,
        ]);
    }

    public function showManagerGeneralConfiguration(){
        return view('General::backOffice.list', [
            'hospitals' => Hospital::all(),
            'cities' => City::all(),
            'globalStatistics' => StatisticsGlobal::all(),
            'genderStatistics' => StatisticsGender::all(),
            'cityStatistics' => StatisticsCity::all(),
            'announcement' => General::all()[0]->announcement
        ]);
    }

    public function handleManagerEditGlobalStatistics($id, Request $request){
        $data = $request->all();

        $info = StatisticsGlobal::find($id);

        $info->cases = $data['cases'];
        $info->deaths = $data['deaths'];
        $info->recovered = $data['recovered'];
        $info->tests = $data['tests'];
        $info->created_at = $data['date'];

        $info->save();

        Toastr::success('Statistiques modifiés');
        return back();
    }

    public function handleManagerEditCityStatistics($id, Request $request){
        $data = $request->all();

        $info = StatisticsCity::find($id);

        $info->cases = $data['cases'];
        $info->deaths = $data['deaths'];
        $info->recovered = $data['recovered'];
        $info->city_id = $data['city'];
        $info->created_at = $data['date'];

        $info->save();

        Toastr::success('Statistiques modifiés');
        return back();
    }

    public function handleManagerEditGenderStatistics($id, Request $request){
        $data = $request->all();

        $info = StatisticsGender::find($id);

        $info->cases = $data['cases'];
        $info->deaths = $data['deaths'];
        $info->recovered = $data['recovered'];
        $info->gender = $data['gender'];
        $info->created_at = $data['date'];

        $info->save();

        Toastr::success('Statistiques modifiés');
        return back();
    }

    public function handleManagerDeleteGlobalStatistics($id){
        StatisticsGlobal::destroy($id);

        Toastr::warning('Statistiques supprimés');
        return back();
    }

    public function handleManagerAddGlobalStatistics(Request $request){
        $data = $request->all();

        StatisticsGlobal::create([
            'cases' => $data['cases'],
            'deaths' => $data['deaths'],
            'recovered' => $data['recovered'],
            'tests' => $data['tests'],
            'created_at' => $data['date']
        ]);

        Toastr::success('Statistiques à jour');
        return back();
    }

    public function handleManagerAddGenderStatistics(Request $request){
        $data = $request->all();

        StatisticsGender::create([
            'cases' => $data['cases'],
            'deaths' => $data['deaths'],
            'recovered' => $data['recovered'],
            'gender' => $data['gender'],
            'created_at' => $data['date']
        ]);

        Toastr::success('Statistiques à jour');
        return back();
    }

    public function handleManagerAddCityStatistics(Request $request){
        $data = $request->all();

        StatisticsCity::create([
            'cases' => $data['cases'],
            'deaths' => $data['deaths'],
            'recovered' => $data['recovered'],
            'city_id' => $data['city'],
            'created_at' => $data['date']
        ]);

        Toastr::success('Statistiques à jour');
        return back();
    }

    public function handleManagerEditAnnouncement(Request $request){
        $data = $request->all();

        $general = General::find(1);
        $general->announcement = $data['message'];
        $general->save();

        Toastr::success('Annonces à jour');
        return back();
    }

    public function showContact(){
        return view('General::frontOffice.public.contact', [
            'announcement' => General::all()[0]->announcement
        ]);
    }

    public function handleContact(Request $request){
        $data = $request->all();

        $content = [
            'name' => $data['name'],
            'email' =>  $data['email'],
            'content' => $data['message'],
            'subject' => $data['subject']
        ];

        Mail::send('General::mail.admin', $content, function ($message) use ($data) {
            $message->from($data['email']);
            $message->to('ceo@bluepenlabs.com');
            $message->subject('Nouveau Message : ' . $data['subject']);
        });

        if(count(Mail::failures()) > 0 ) {
            Toastr::warning('L\'envoi des mails a échoué, contactez le support');
        }
        Toastr::success('Message envoyé à l\'administration');
        return redirect(route('showHome'));
    }

    public function showAPI(){
        return view('General::frontOffice.public.api', [

        ]);
    }
}
