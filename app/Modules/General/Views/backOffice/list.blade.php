@extends('backOffice.layout')

@section('head')
    @include('backOffice.inc.head',
    ['title' => config('app.name') . ' - Administration',
    'description' => 'Espace Administratif - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('backOffice.inc.header')
@endsection

@section('sidebar')
    @include('backOffice.inc.sidebar', [
        'current' => 'general'
    ])
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable.js') }}"></script>

    <script type="text/javascript">

        /* Datatables responsive */

        $(document).ready(function () {
            $('.datatable').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "Rechercher...");
        });

    </script>

    <div class="breadcrumb">
        <h1>Général</h1>
        <ul>
            <li><a href="{{ route('showManagerHome') }}">Tableau de Bord</a></li>
            <li>Statistiques</li>
        </ul>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">Annonce</div>
                    <form class="form-horizontal" role="form" action="{{ route('handleManagerEditAnnouncement') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12 form-group mb-3">
                                <textarea class="form-control"  required rows="2" name="message"> {{ $announcement }}</textarea>
                            </div>

                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">Statistiques Globaux</div>
                    <form method="post" action="{{ route('handleManagerAddGlobalStatistics') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4 form-group mb-3">
                                <label for="firstName1">Cas</label>
                                <input class="form-control" required value="1" name="cases" type="number" min="0">
                            </div>

                            <div class="col-md-4 form-group mb-3">
                                <label for="firstName1">Rétablis</label>
                                <input class="form-control" required value="1" name="recovered" type="number" min="0">
                            </div>

                            <div class="col-md-4 form-group mb-3">
                                <label for="firstName1">Décès</label>
                                <input class="form-control" required value="1" name="deaths" type="number" min="0">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="firstName1">Tests</label>
                                <input class="form-control" required value="1" name="tests" type="number" min="0">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="firstName1">Date</label>
                                <input class="form-control" required  name="date" type="date">
                            </div>

                            <div class="col-md-12 pull-right">
                                <button type="submit" class="btn btn-primary">Soumettre</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">Statistiques par Sexe</div>
                    <form method="post" action="{{ route('handleManagerAddGenderStatistics') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4 form-group mb-3">
                                <label for="firstName1">Cas</label>
                                <input class="form-control" required value="1" name="cases" type="number" min="0">
                            </div>

                            <div class="col-md-4 form-group mb-3">
                                <label for="firstName1">Rétablis</label>
                                <input class="form-control" required value="1" name="recovered" type="number" min="0">
                            </div>

                            <div class="col-md-4 form-group mb-3">
                                <label for="firstName1">Décès</label>
                                <input class="form-control" required value="1" name="deaths" type="number" min="0">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="firstName1">Sexe</label>
                                <select class="form-control" name="gender">
                                    <option value="1">Homme</option>
                                    <option value="2">Femme</option>
                                </select>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="firstName1">Date</label>
                                <input class="form-control" required  name="date" type="date">
                            </div>

                            <div class="col-md-12 pull-right">
                                <button type="submit" class="btn btn-primary">Soumettre</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">Statistiques par Cité</div>
                    <form method="post" action="{{ route('handleManagerAddCityStatistics') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4 form-group mb-3">
                                <label for="firstName1">Cas</label>
                                <input class="form-control" required value="1" name="cases" type="number" min="0">
                            </div>

                            <div class="col-md-4 form-group mb-3">
                                <label for="firstName1">Rétablis</label>
                                <input class="form-control" required value="1" name="recovered" type="number" min="0">
                            </div>

                            <div class="col-md-4 form-group mb-3">
                                <label for="firstName1">Décès</label>
                                <input class="form-control" required value="1" name="deaths" type="number" min="0">
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="firstName1">Gouvernorat</label>
                                <select class="form-control" name="city">
                                    @foreach($cities as $city)
                                    <option value="{{ $city->id }}">{{ $city->title }}</option>
                                        @endforeach
                                </select>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="firstName1">Date</label>
                                <input class="form-control" required  name="date" type="date">
                            </div>

                            <div class="col-md-12 pull-right">
                                <button type="submit" class="btn btn-primary">Soumettre</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-4">

        <div class="col-md-12 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <div class="card-title mb-3">Synthèse Statistiques Globaux</div>

                    <table
                           class="display datatable table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Cas</th>
                            <th>Décés</th>
                            <th>Rétablis</th>
                            <th>Tests</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th>Cas</th>
                            <th>Décés</th>
                            <th>Rétablis</th>
                            <th>Tests</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach($globalStatistics as $info)
                            <tr>
                                <td>{{ $info->cases }}</td>
                                <td>{{ $info->deaths }}</td>
                                <td>{{ $info->recovered }}</td>
                                <td>{{ $info->tests }}</td>
                                <td>{{ $info->created_at->format('d-m-Y') }}</td>
                                <td style="text-align: center">
                                    <span class="badge badge-pill  badge-outline-success p-2 m-1" title="Modifier" data-toggle="modal" data-target="#edit-t-{{ $info->id }}"><i class="nav-icon i-Cloud-Settings"></i></span>
                                    <a href="{{ route('handleManagerDeleteGlobalStatistics', ['id' => $info->id]) }}"><span title="Supprimer" class="badge badge-pill badge-outline-warning p-2 m-1"><i class="nav-icon i-Recycling-2"></i></span></a>
                                </td>
                            </tr>
                            <div class="modal fade" id="edit-t-{{ $info->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form method="post" action="{{ route('handleManagerEditGlobalStatistics', ['id' => $info->id]) }}">
                                            {{ csrf_field() }}
                                            <div class="modal-header">
                                                <h5 class="modal-title">Modifier Stats Globaux pour le {{ $info->created_at->format('d-m-Y') }}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label class="control-label">Cas</label>
                                                        <input name="cases" required class="form-control" type="number" value="{{ $info->cases }}">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label class="control-label">Décés</label>
                                                        <input name="deaths" required class="form-control" type="number" value="{{ $info->deaths }}">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label class="control-label">Rétablis</label>
                                                        <input name="recovered" required class="form-control" type="number" value="{{ $info->recovered }}">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Tests</label>
                                                        <input name="tests" required class="form-control" type="number" value="{{ $info->tests }}">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Date</label>
                                                        <input name="date" required class="form-control" type="date" value="{{ $info->created_at->format('d/m/Y') }}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                                <button type="submit" class="btn btn-primary">Envoyer</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="col-md-12 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <div class="card-title mb-3">Synthèse Statistiques Par Sexe</div>

                    <table
                           class="display datatable table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Cas</th>
                            <th>Décés</th>
                            <th>Rétablis</th>
                            <th>Tests</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th>Cas</th>
                            <th>Décés</th>
                            <th>Rétablis</th>
                            <th>Sexe</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach($genderStatistics as $info)
                            <tr>
                                <td>{{ $info->cases }}</td>
                                <td>{{ $info->deaths }}</td>
                                <td>{{ $info->recovered }}</td>
                                <td>{{ $info->gender == 1 ? 'Homme' : 'Femme' }}</td>
                                <td>{{ $info->created_at->format('d-m-Y') }}</td>
                                <td style="text-align: center">
                                    <span class="badge badge-pill  badge-outline-success p-2 m-1" title="Modifier" data-toggle="modal" data-target="#edit-g-{{ $info->id }}"><i class="nav-icon i-Cloud-Settings"></i></span>
                                    <a href="{{ route('handleManagerDeleteGenderStatistics', ['id' => $info->id]) }}"><span title="Supprimer" class="badge badge-pill badge-outline-warning p-2 m-1"><i class="nav-icon i-Recycling-2"></i></span></a>
                                </td>
                            </tr>
                            <div class="modal fade" id="edit-g-{{ $info->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form method="post" action="{{ route('handleManagerEditGenderStatistics', ['id' => $info->id]) }}">
                                            {{ csrf_field() }}
                                            <div class="modal-header">
                                                <h5 class="modal-title">Modifier {{ $info->created_at->format('d-m-Y') }}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label class="control-label">Cas</label>
                                                        <input name="cases" required class="form-control" type="number" value="{{ $info->cases }}">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label class="control-label">Décés</label>
                                                        <input name="deaths" required class="form-control" type="number" value="{{ $info->deaths }}">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label class="control-label">Rétablis</label>
                                                        <input name="recovered" required class="form-control" type="number" value="{{ $info->recovered }}">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Sexe</label>
                                                        <select class="form-control" name="gender">
                                                            <option @if($info->gender == 1) selected @endif value="1">Homme</option>
                                                            <option @if($info->gender == 2) selected @endif value="2">Femme</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Date</label>
                                                        <input name="date" required class="form-control" type="date" value="{{ $info->created_at->format('d/m/Y') }}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                                <button type="submit" class="btn btn-primary">Envoyer</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="col-md-12 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <div class="card-title mb-3">Synthèse Statistiques par Cité</div>

                    <table
                           class="display datatable table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Cas</th>
                            <th>Décés</th>
                            <th>Rétablis</th>
                            <th>Cité</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th>Cas</th>
                            <th>Décés</th>
                            <th>Rétablis</th>
                            <th>Tests</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach($cityStatistics as $info)
                            <tr>
                                <td>{{ $info->cases }}</td>
                                <td>{{ $info->deaths }}</td>
                                <td>{{ $info->recovered }}</td>
                                <td>{{ $info->city->title }}</td>
                                <td>{{ $info->created_at->format('d-m-Y') }}</td>
                                <td style="text-align: center">
                                    <span class="badge badge-pill  badge-outline-success p-2 m-1" title="Modifier" data-toggle="modal" data-target="#edit-c-{{ $info->id }}"><i class="nav-icon i-Cloud-Settings"></i></span>
                                    <a href="{{ route('handleManagerDeleteCityStatistics', ['id' => $info->id]) }}"><span title="Supprimer" class="badge badge-pill badge-outline-warning p-2 m-1"><i class="nav-icon i-Recycling-2"></i></span></a>
                                </td>
                            </tr>
                            <div class="modal fade" id="edit-c-{{ $info->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form method="post" action="{{ route('handleManagerEditCityStatistics', ['id' => $info->id]) }}">
                                            {{ csrf_field() }}
                                            <div class="modal-header">
                                                <h5 class="modal-title">Modifier {{ $info->created_at->format('d-m-Y') }}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label class="control-label">Cas</label>
                                                        <input name="cases" required class="form-control" type="number" value="{{ $info->cases }}">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label class="control-label">Décés</label>
                                                        <input name="deaths" required class="form-control" type="number" value="{{ $info->deaths }}">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label class="control-label">Rétablis</label>
                                                        <input name="recovered" required class="form-control" type="number" value="{{ $info->recovered }}">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Sexe</label>
                                                        <select class="form-control" name="city">
                                                            @foreach($cities as $city)
                                                            <option @if($city->id == $info->city_id) selected @endif value="{{ $city->id }}">{{ $city->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Date</label>
                                                        <input name="date" required class="form-control" type="date" value="{{ $info->created_at->format('d/m/Y') }}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                                <button type="submit" class="btn btn-primary">Envoyer</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>



@endsection
