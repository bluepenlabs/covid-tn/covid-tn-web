@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - Statistiques COVID 19 en Tunisie',
    'description' => 'Statistiques COVID 19 en Tunisie - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header',
    [
        'actual' => 'stats',
        'cat' => ''
    ])
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/chart-js/chart-js.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/chart-js/chart-js.js') }}"></script>

    <div class="content">
        <!--  section  -->
        <section class="parallax-section single-par" data-scrollax-parent="true">
            <div class="bg par-elem " data-bg="{{ asset('img/stats.png') }}" data-scrollax="properties: { translateY: '30%' }" style="background-image: url('{{ asset('img/stats.png') }}'); transform: translateZ(0px) translateY(27.2239%);"></div>
            <div class="overlay op7"></div>
            <div class="container">
                <div class="section-title center-align big-title">
                    <h2><span>Statistiques</span></h2>
                    <span class="section-separator"></span>
                    <div class="breadcrumbs fl-wrap"><a href="{{ route('showHome') }}">Accueil</a><span>Statistiques</span></div>
                </div>
            </div>
            <div class="header-sec-link">
                <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a>
            </div>
        </section>
        <!--  section  end-->
        <section id="sec1" data-scrollax-parent="true">
            <div class="container">
                <div class="section-title">
                    <h2> Évolution de la Pandémie </h2>
                    <div class="section-subtitle">COVID 19</div>
                    <span class="section-separator"></span>
                    <p>De la COVID 19 (CoronaVirus Disease) en Tunisie.</p>
                </div>
                <!--about-wrap -->
                <div class="about-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <canvas id="globalChart"></canvas>
                        </div>
                        <div class="col-lg-12">
                            <canvas id="dailyChart"></canvas>
                        </div>
                    </div>
                </div>
                <!-- about-wrap end  -->
                <span class="fw-separator"></span>
                <div class=" single-facts bold-facts fl-wrap">
                    <!-- inline-facts -->
                    <div class="inline-facts-wrap">
                        <div class="inline-facts">
                            <div class="milestone-counter">
                                <div class="stats animaper">
                                    <div  style="color: #4DB7FE; font-size: 44px;font-weight: 600;font-family: 'Raleway', sans-serif;">{{ $statistics->last()->tests }}</div>
                                </div>
                            </div>
                            <h6>Nombre de Tests</h6>
                        </div>
                    </div>
                    <!-- inline-facts end -->
                    <!-- inline-facts  -->
                    <div class="inline-facts-wrap">
                        <div class="inline-facts">
                            <div class="milestone-counter">
                                <div class="stats animaper">
                                    <div  style="color: #4DB7FE; font-size: 44px;font-weight: 600;font-family: 'Raleway', sans-serif;">{{ $statistics->last()->cases - $statistics->last()->recovered - $statistics->last()->deaths }}</div>
                                </div>
                            </div>
                            <h6>Cas Actifs</h6>
                        </div>
                    </div>
                    <!-- inline-facts end -->
                    <!-- inline-facts  -->
                    <div class="inline-facts-wrap">
                        <div class="inline-facts">
                            <div class="milestone-counter">
                                <div class="stats animaper">
                                    <div  style="color: #4DB7FE; font-size: 44px;font-weight: 600;font-family: 'Raleway', sans-serif;">{{ round($statistics->last()->recovered * 100 / $statistics->last()->cases, 1)}} %</div>
                                </div>
                            </div>
                            <h6>% Rétablis</h6>
                        </div>
                    </div>
                    <!-- inline-facts end -->
                    <!-- inline-facts  -->
                    <div class="inline-facts-wrap">
                        <div class="inline-facts">
                            <div class="milestone-counter">
                                <div class="stats animaper">
                                    <div  style="color: #4DB7FE; font-size: 44px;font-weight: 600;font-family: 'Raleway', sans-serif;">{{ round($statistics->last()->deaths * 100 / $statistics->last()->cases, 1)}} %</div>
                                </div>
                            </div>
                            <h6>% Morts</h6>
                        </div>
                    </div>
                    <!-- inline-facts end -->
                </div>
                <span class="fw-separator"></span>

                <div class="section-title" style="margin-top: 25px;">
                    <h2> Évolution par Gouvernorat </h2>
                    <div class="section-subtitle">COVID 19</div>
                    <span class="section-separator"></span>
                    <p>De la COVID 19 (CoronaVirus Disease) en Tunisie.</p>
                </div>

                <div class="about-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <canvas id="citiesCasesChart"></canvas>
                        </div>
                        <div class="col-lg-12">
                            <canvas id="citiesDeathsChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <small style="font-size: 10px;color: gray;">(Les statistiques présentés sont une collection de recherches établis par plusieurs personnes volontaires, ainsi que nos algorithmes d'analyse créés pour ce besoin. La source principale de nos chiffres vient de ce <a href='https://docs.google.com/spreadsheets/d/18aDPBcPWQ2BuqbKpAoAQKlub07revrbSKnaexB6gb4o/' target="_blank">SpreadSheet</a>.) </small>

        </section>
    </div>

    <script>
        function hashCode() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        var dailyCanvas = document.getElementById("dailyChart");
        var dailyCtx = dailyCanvas.getContext('2d');

        Chart.defaults.global.defaultFontColor = 'black';
        Chart.defaults.global.defaultFontSize = 16;

        var dailyChartData = {
            labels: [
                @foreach($statistics as $number)
                    "{{ $number->created_at->format('d-M') }}",
                @endforeach
            ],
            datasets: [{
                label: "Cas",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "#FF6B6B",
                borderColor: "#FF6B6B", // The main line color
                borderCapStyle: 'square',
                borderDash: [], // try [5, 15] for instance
                borderDashOffset: 0.0,
                pointBorderWidth: 1,
                pointHoverRadius: 8,
                pointHoverBorderWidth: 2,
                pointRadius: 4,
                pointHitRadius: 10,
                // notice the gap in the data and the spanGaps: true
                data: [
                    @foreach($separatedCasesStats as $number)
                        "{{ $number }}",
                    @endforeach
                ],
                spanGaps: true,
            }, {
                label: "Rétablis",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "#65C3DF",
                borderColor: "#65C3DF",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                pointBorderWidth: 1,
                pointHoverRadius: 8,
                pointHoverBorderWidth: 2,
                pointRadius: 4,
                pointHitRadius: 10,
                // notice the gap in the data and the spanGaps: false
                data: [
                    @foreach($separatedRecoveredStats as $number)
                        "{{ $number }}",
                    @endforeach
                ],
                spanGaps: false,
            },
                {
                    label: "Morts",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "#000000",
                    borderColor: "#000000",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    pointBorderWidth: 1,
                    pointHoverRadius: 8,
                    pointHoverBorderWidth: 2,
                    pointRadius: 4,
                    pointHitRadius: 10,
                    // notice the gap in the data and the spanGaps: false
                    data: [
                        @foreach($separatedDeathsStats as $number)
                            "{{ $number }}",
                        @endforeach
                    ],
                    spanGaps: false,
                },
                {
                    label: "Tests",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "#168f48",
                    borderColor: "#168f48",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    pointBorderWidth: 1,
                    pointHoverRadius: 8,
                    pointHoverBorderWidth: 2,
                    pointRadius: 4,
                    pointHitRadius: 10,
                    // notice the gap in the data and the spanGaps: false
                    data: [
                        @foreach($separatedTestsStats as $number)
                            "{{ $number }}",
                        @endforeach
                    ],
                    spanGaps: false,
                }

            ]
        };

        var dailyChartOptions = {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Comparaison Quotidienne',
                        fontSize: 20
                    }
                }]
            }
        };

        // Chart declaration:
        var dailyChart = new Chart(dailyCtx, {
            type: 'line',
            data: dailyChartData,
            options: dailyChartOptions
        });

        var globalCanvas = document.getElementById("globalChart");
        var globalCtx = globalCanvas.getContext('2d');

        var globalData = {
            labels: [
                @foreach($statistics as $number)
                    "{{ $number->created_at->format('d-M') }}",
                @endforeach
            ],
            datasets: [{
                label: "Cas",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "#FF6B6B",
                borderColor: "#FF6B6B", // The main line color
                borderCapStyle: 'square',
                borderDash: [], // try [5, 15] for instance
                borderDashOffset: 0.0,
                pointBorderWidth: 1,
                pointHoverRadius: 8,
                pointHoverBorderWidth: 2,
                pointRadius: 4,
                pointHitRadius: 10,
                // notice the gap in the data and the spanGaps: true
                data: [
                    @foreach($statistics as $number)
                            "{{ $number->cases }}",
                            @endforeach
                    ],
                spanGaps: true,
            }, {
                label: "Rétablis",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "#65C3DF",
                borderColor: "#65C3DF",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                pointBorderWidth: 1,
                pointHoverRadius: 8,
                pointHoverBorderWidth: 2,
                pointRadius: 4,
                pointHitRadius: 10,
                // notice the gap in the data and the spanGaps: false
                data: [
                    @foreach($statistics as $number)
                        "{{ $number->recovered }}",
                    @endforeach
                ],
                spanGaps: false,
            },
                {
                    label: "Morts",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "#000000",
                    borderColor: "#000000",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    pointBorderWidth: 1,
                    pointHoverRadius: 8,
                    pointHoverBorderWidth: 2,
                    pointRadius: 4,
                    pointHitRadius: 10,
                    // notice the gap in the data and the spanGaps: false
                    data: [
                        @foreach($statistics as $number)
                            "{{ $number->deaths }}",
                        @endforeach
                    ],
                    spanGaps: false,
                }
,
                {
                    label: "Tests",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "#168f48",
                    borderColor: "#168f48",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    pointBorderWidth: 1,
                    pointHoverRadius: 8,
                    pointHoverBorderWidth: 2,
                    pointRadius: 4,
                    pointHitRadius: 10,
                    // notice the gap in the data and the spanGaps: false
                    data: [
                        @foreach($statistics as $number)
                            "{{ $number->tests }}",
                        @endforeach
                    ],
                    spanGaps: false,
                }
            ]
        };

        // Notice the scaleLabel at the same level as Ticks
        var globalOptions = {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Évolution Quotidienne',
                        fontSize: 20
                    }
                }]
            }
        };

        // Chart declaration:
        var globalChart = new Chart(globalCtx, {
            type: 'line',
            data: globalData,
            options: globalOptions
        });


        //////////////////

        var citiesCasesCanvas = document.getElementById("citiesCasesChart");
        var dailyCitiesCasesCtx = citiesCasesCanvas.getContext('2d');

        var dailyCitiesCasesChartData = {
            labels: [
                @foreach($statistics as $number)
                    "{{ $number->created_at->format('d-M') }}",
                @endforeach
            ],
            datasets: [
                @foreach($citiesFormattedStats as $city => $values)
                {
                label: "{{ $city }}",
                fill: false,
                lineTension: 0.1,
                backgroundColor: hashCode(),
                borderCapStyle: 'square',
                borderDash: [], // try [5, 15] for instance
                borderDashOffset: 0.0,
                pointBorderWidth: 1,
                pointHoverRadius: 8,
                pointHoverBorderWidth: 2,
                pointRadius: 4,
                pointHitRadius: 10,
                // notice the gap in the data and the spanGaps: true
                data: [
                    @foreach($values as $number)
                        "{{ $number->cases }}",
                    @endforeach
                ],
                spanGaps: true,
                },
                @endforeach
            ]
        };

        // Notice the scaleLabel at the same level as Ticks
        var dailyCitiesCasesChartOptions = {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Évolution des Cas par Gouvernorat',
                        fontSize: 20
                    }
                }]
            }
        };

        // Chart declaration:
        var dailyCitiesCasesChart = new Chart(dailyCitiesCasesCtx, {
            type: 'line',
            data: dailyCitiesCasesChartData,
            options: dailyCitiesCasesChartOptions
        });

        ///////////////

        var citiesDeathsCanvas = document.getElementById("citiesDeathsChart");
        var dailyDeathsCasesCtx = citiesDeathsCanvas.getContext('2d');

        var dailyCitiesDeathsChartData = {
            labels: [
                @foreach($statistics as $number)
                    "{{ $number->created_at->format('d-M') }}",
                @endforeach
            ],
            datasets: [
                    @foreach($citiesFormattedStats as $city => $values)
                {
                    label: "{{ $city }}",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: hashCode(),
                    borderCapStyle: 'square',
                    borderDash: [], // try [5, 15] for instance
                    borderDashOffset: 0.0,
                    pointBorderWidth: 1,
                    pointHoverRadius: 8,
                    pointHoverBorderWidth: 2,
                    pointRadius: 4,
                    pointHitRadius: 10,
                    // notice the gap in the data and the spanGaps: true
                    data: [
                        @foreach($values as $number)
                            "{{ $number->deaths }}",
                        @endforeach
                    ],
                    spanGaps: true,
                },
                @endforeach
            ]
        };

        // Notice the scaleLabel at the same level as Ticks
        var dailyCitiesDeathsChartOptions = {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Évolution des Décès par Gouvernorat',
                        fontSize: 20
                    }
                }]
            }
        };

        // Chart declaration:
        var dailyCitiesDeathsChart = new Chart(dailyDeathsCasesCtx, {
            type: 'line',
            data: dailyCitiesDeathsChartData,
            options: dailyCitiesDeathsChartOptions
        });

    </script>
@endsection

@section('footer')
    @include('frontOffice.inc.footer')
@endsection
