@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - Contact',
    'description' => 'Contact - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header',
    [
        'actual' => 'contact',
        'cat' => ''
    ])
@endsection

@section('content')
    <div class="content">
        <!--  section  -->
        <section class="parallax-section single-par" data-scrollax-parent="true">
            <div class="bg par-elem "  data-bg="{{ asset('img/bg/18.jpg') }}" data-scrollax="properties: { translateY: '30%' }"></div>
            <div class="overlay op7"></div>
            <div class="container">
                <div class="section-title center-align big-title">
                    <h2><span>Contactez Nous</span></h2>
                    <span class="section-separator"></span>
                    <div class="breadcrumbs fl-wrap"><a href="{{ route('showHome') }}">Accueil</a><span>Contact</span></div>
                </div>
            </div>
            <div class="header-sec-link">
                <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a>
            </div>
        </section>
        <!--  section  end-->
        <!--  section  -->
        <section   id="sec1" data-scrollax-parent="true">
            <div class="container">
                <!--about-wrap -->
                <div class="about-wrap">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="ab_text-title fl-wrap">
                                <h3>Contact</h3>
                                <span class="section-separator fl-sec-sep"></span>
                            </div>
                            <!--box-widget-item -->
                            <div class="box-widget-item fl-wrap block_box">
                                <div class="box-widget">
                                    <div class="box-widget-content bwc-nopad">
                                        <div class="list-author-widget-contacts list-item-widget-contacts bwc-padside">
                                            <ul class="no-list-style">
                                                <li><span><i class="fal fa-envelope"></i> Email :</span> <a href="#">hello@covid.tn</a></li>
                                                <li><span><i class="fal fa-envelope"></i> Email :</span> <a href="#">ceo@bluepenlabs.tn</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->
                            <!--box-widget-item -->
                            <div class="box-widget-item fl-wrap" style="margin-top:20px;">
                                <div class="banner-wdget fl-wrap">
                                    <div class="overlay op4"></div>
                                    <div class="bg"  data-bg="{{ asset('img/bg/19.jpg') }}"></div>
                                    <div class="banner-wdget-content fl-wrap">
                                        <h4>Covid.tn est Open Source, demandez l'accès au code a partir de ce formulaire.</h4>
                                        <a class="color-bg">Partageons notre savoir</a>
                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->
                        </div>
                        <div class="col-md-8">
                            <div class="ab_text">
                                <div class="ab_text-title fl-wrap">
                                    <h3>Une idée ? Suggestion ?</h3>
                                    <span class="section-separator fl-sec-sep"></span>
                                </div>
                                <p>Partagez avec nous vos suggestions, idées ou remarques afin de nous aider a améliorer ce service ou le réparer si vous trouvez une erreur quelque part. Nous sommes ouverts a tout type de partenariat, suggestion et collaboration.</p>
                                <div id="contact-form">
                                    <div id="message"></div>
                                    <form  class="custom-form" action="{{ route('handleContact') }}" method="post">
                                        {{ csrf_field() }}
                                        <fieldset>
                                            <label><i class="fal fa-user"></i></label>
                                            <input type="text" name="name" required placeholder="Nom *" @auth() value="{{ Auth::user()->name }}" @endauth/>
                                            <div class="clearfix"></div>
                                            <label><i class="fal fa-envelope"></i>  </label>
                                            <input type="email"  name="email" required placeholder="Adresse Email *" @auth() value="{{ Auth::user()->email }}" @endauth/>
                                            <label><i class="fal fa-envelope"></i>  </label>
                                            <input type="text"  name="subject" required placeholder="Sujet *" value=""/>
                                            <textarea name="message" cols="40" rows="3" placeholder="Votre Message:"></textarea>
                                        </fieldset>
                                        <button class="btn float-btn color2-bg" readonly="" type="submit">Envoyer<i class="fal fa-paper-plane"></i></button>
                                    </form>
                                </div>
                                <!-- contact form  end-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- about-wrap end  -->
            </div>
        </section>
    </div>
@endsection

@section('footer')
    @include('frontOffice.inc.footer')
@endsection
