@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - API pour les Développeurs',
    'description' => 'API pour les Développeurs - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header',
    [
        'actual' => 'api',
        'cat' => ''
    ])
@endsection

@section('content')
    <div class="content">
        <section class="gray-bg no-top-padding-sec" id="sec1">
            <div class="container">
                <div class="breadcrumbs inline-breadcrumbs fl-wrap block-breadcrumbs">
                    <a href="{{ route('showHome') }}">Accueil</a><span>API</span>
                    <div class="showshare brd-show-share color2-bg"><i class="fas fa-share"></i> Share</div>
                </div>
                <div class="share-holder hid-share sing-page-share top_sing-page-share">
                    <div class="share-container  isShare"></div>
                </div>
                <div class="post-container fl-wrap">
                    <div class="row">
                        <!-- blog content-->
                        <div class="col-md-8">
                            <!-- article> -->
                            <article class="post-article single-post-article">
                                <a name="1"></a>
                                <div class="list-single-main-item fl-wrap block_box">
                                    <h2 class="post-opt-title">Liste des Hôpitaux</h2>
                                    <div class="post-opt">
                                        <ul class="no-list-style">
                                            <li><i class="fal fa-spider-web"></i> <span>{{ route('showHome') }}/api/medical/hospitals/{limit}/{offset}</span>
                                            </li>
                                            <li><i class="fal fa-memory"></i> <span>GET</span></li>
                                        </ul>
                                    </div>
                                    <span class="fw-separator"></span>
                                    <div class="clearfix"></div>
                                    <p><strong>Paramètres : </strong><br> - Limit : <strong>(int)</strong> ; Défaut
                                        <strong>10</strong> <br> - Offset : <strong>(int)</strong> ; Défaut
                                        <strong>0</strong>
                                    </p>
                                    <blockquote>

                                        <p><strong>Exemple CURL :</strong><br>
                                            curl -X GET -k -i {{ route('showHome') }}/api/medical/hospitals/20/1
                                        </p>
                                    </blockquote>

                                </div>
                            </article>

                            <article class="post-article single-post-article">
                                <a name="2"></a>
                                <div class="list-single-main-item fl-wrap block_box">
                                    <h2 class="post-opt-title">Liste des Services d'un Hôpital</h2>
                                    <div class="post-opt">
                                        <ul class="no-list-style">
                                            <li><i class="fal fa-spider-web"></i> <span>{{ route('showHome') }}/api/medical/hospital/services/{id}/{limit}/{offset}</span>
                                            </li>
                                            <li><i class="fal fa-memory"></i> <span>GET</span></li>
                                        </ul>
                                    </div>
                                    <span class="fw-separator"></span>
                                    <div class="clearfix"></div>
                                    <p><strong>Paramètres : </strong>
                                        <br>
                                        - id :<strong>(int)</strong> ; Id de l'hôpital, Obligatoire
                                        <br>
                                        - Limit :<strong>(int)</strong> ; Défaut<strong>10</strong>
                                        <br>
                                        - Offset : <strong>(int)</strong> ; Défaut <strong>0</strong>
                                    </p>
                                    <blockquote>

                                        <p><strong>Exemple CURL :</strong><br>
                                            curl -X GET -k -i {{ route('showHome') }}/api/medical/hospital/services/2042/20/1
                                        </p>
                                    </blockquote>
                                </div>
                            </article>

                            <article class="post-article single-post-article">
                                <a name="3"></a>
                                <div class="list-single-main-item fl-wrap block_box">
                                    <h2 class="post-opt-title">Liste des Besoins <sup>(Avec Stock)</sup> d'un Service</h2>
                                    <div class="post-opt">
                                        <ul class="no-list-style">
                                            <li><i class="fal fa-spider-web"></i> <span>{{ route('showHome') }}/api/medical/hospital/service/{id}/needs/{limit}/{offset}</span>
                                            </li>
                                            <li><i class="fal fa-memory"></i> <span>GET</span></li>
                                        </ul>
                                    </div>
                                    <span class="fw-separator"></span>
                                    <div class="clearfix"></div>
                                    <p><strong>Paramètres : </strong>
                                        <br>
                                        - id :<strong>(int)</strong> ; Id du service, Obligatoire
                                        <br>
                                        - Limit :<strong>(int)</strong> ; Défaut<strong>10</strong>
                                        <br>
                                        - Offset : <strong>(int)</strong> ; Défaut <strong>0</strong>
                                    </p>
                                    <blockquote>

                                        <p><strong>Exemple CURL :</strong><br>
                                            curl -X GET -k -i {{ route('showHome') }}/api/medical/hospital/service/2042/needs/10/0
                                        </p>
                                    </blockquote>
                                </div>
                            </article>

                            <article class="post-article single-post-article">
                                <a name="4"></a>
                                <div class="list-single-main-item fl-wrap block_box">
                                    <h2 class="post-opt-title">Agent Chargé d'un Service</h2>
                                    <div class="post-opt">
                                        <ul class="no-list-style">
                                            <li><i class="fal fa-spider-web"></i>
                                                <span>{{ route('showHome') }}/api/medical/hospital/service/{id}/user</span>
                                            </li>
                                            <li><i class="fal fa-memory"></i> <span>GET</span></li>
                                        </ul>
                                    </div>
                                    <span class="fw-separator"></span>
                                    <div class="clearfix"></div>
                                    <p><strong>Paramètres : </strong>
                                        <br>
                                        - id :<strong>(int)</strong> ; Id du service, Obligatoire
                                        <br>
                                        - Limit :<strong>(int)</strong> ; Défaut<strong>10</strong>
                                        <br>
                                        - Offset : <strong>(int)</strong> ; Défaut <strong>0</strong>
                                    </p>
                                    <blockquote>
                                        <p><strong>Exemple CURL :</strong><br>
                                            curl -X GET -k -i {{ route('showHome') }}/api/medical/hospital/service/2042/user
                                        </p>
                                    </blockquote>
                                </div>
                            </article>

                            <article class="post-article single-post-article">
                                <a name="5"></a>
                                <div class="list-single-main-item fl-wrap block_box">
                                    <h2 class="post-opt-title">État du Stock par Service par Date</h2>
                                    <div class="post-opt">
                                        <ul class="no-list-style">
                                            <li><i class="fal fa-spider-web"></i>
                                                <span>{{ route('showHome') }}/api/medical/hospital/service/{id}/needs/{date}</span>
                                            </li>
                                            <li><i class="fal fa-memory"></i> <span>GET</span></li>
                                        </ul>
                                    </div>
                                    <span class="fw-separator"></span>
                                    <div class="clearfix"></div>
                                    <p><strong>Paramètres : </strong>
                                        <br>
                                        - id :<strong>(int)</strong> ; ID du Service, Obligatoire
                                        <br>
                                        - date :<strong>(string)</strong> ; Format <strong>YYYY-MM-DD</strong>, Obligatoire
                                    </p>
                                    <blockquote>

                                        <p><strong>Exemple CURL :</strong><br>
                                            curl -X GET -k -i {{ route('showHome') }}/api/medical/hospital/service/1021/needs/{{ now()->format('Y-m-d') }}
                                        </p>
                                    </blockquote>
                                </div>
                            </article>

                            <article class="post-article single-post-article">
                                <a name="6"></a>
                                <div class="list-single-main-item fl-wrap block_box">
                                    <h2 class="post-opt-title">Statistiques Globales par Date</h2>
                                    <div class="post-opt">
                                        <ul class="no-list-style">
                                            <li><i class="fal fa-spider-web"></i>
                                                <span>{{ route('showHome') }}/api/stats/global/{date}</span>
                                            </li>
                                            <li><i class="fal fa-memory"></i> <span>GET</span></li>
                                        </ul>
                                    </div>
                                    <span class="fw-separator"></span>
                                    <div class="clearfix"></div>
                                    <p><strong>Paramètres : </strong>
                                        <br>
                                        - date :<strong>(string)</strong> ; Format <strong>YYYY-MM-DD</strong>
                                    </p>
                                    <blockquote>

                                        <p><strong>Exemple CURL :</strong><br>
                                            curl -X GET -k -i {{ route('showHome') }}/api/stats/global/{{ now()->format('Y-m-d') }}
                                        </p>
                                    </blockquote>
                                </div>
                            </article>

                            <article class="post-article single-post-article">
                                <a name="7"></a>
                                <div class="list-single-main-item fl-wrap block_box">
                                    <h2 class="post-opt-title">Liste des Gouvernorats pour les Statistiques</h2>
                                    <div class="post-opt">
                                        <ul class="no-list-style">
                                            <li><i class="fal fa-spider-web"></i>
                                                <span>{{ route('showHome') }}/api/stats/cities/{limit}/{offset}</span>
                                            </li>
                                            <li><i class="fal fa-memory"></i> <span>GET</span></li>
                                        </ul>
                                    </div>
                                    <span class="fw-separator"></span>
                                    <div class="clearfix"></div>
                                    <p><strong>Paramètres : </strong>
                                        <br>
                                        - Limit :<strong>(int)</strong> ; Défaut<strong>10</strong>
                                        <br>
                                        - Offset : <strong>(int)</strong> ; Défaut <strong>0</strong>
                                    </p>
                                    <blockquote>

                                        <p><strong>Exemple CURL :</strong><br>
                                            curl -X GET -k -i {{ route('showHome') }}/api/stats/cities/10/0
                                        </p>
                                    </blockquote>
                                </div>
                            </article>

                            <article class="post-article single-post-article">
                                <a name="8"></a>
                                <div class="list-single-main-item fl-wrap block_box">
                                    <h2 class="post-opt-title">Statistiques par Gouvernorats par Date</h2>
                                    <div class="post-opt">
                                        <ul class="no-list-style">
                                            <li><i class="fal fa-spider-web"></i>
                                                <span>{{ route('showHome') }}/api/stats/city/{id}/{date}</span>
                                            </li>
                                            <li><i class="fal fa-memory"></i> <span>GET</span></li>
                                        </ul>
                                    </div>
                                    <span class="fw-separator"></span>
                                    <div class="clearfix"></div>
                                    <p><strong>Paramètres : </strong>
                                        <br>
                                        - id :<strong>(int)</strong> ; ID du Service, Obligatoire
                                        <br>
                                        - date :<strong>(string)</strong> ; Format <strong>YYYY-MM-DD</strong>, Obligatoire
                                    </p>
                                    <blockquote>

                                        <p><strong>Exemple CURL :</strong><br>
                                            curl -X GET -k -i {{ route('showHome') }}/api/stats/city/1021/{{ now()->format('Y-m-d') }}
                                        </p>
                                    </blockquote>
                                </div>
                            </article>

                        </div>
                        <!-- blog conten end-->
                        <!-- blog sidebar -->
                        <div class="col-md-4">
                            <div class="box-widget-wrap fl-wrap fixed-bar">
                                <!--box-widget-item -->
                                <div class="box-widget-item fl-wrap block_box">
                                    <div class="box-widget-item-header">
                                        <h3>Raccourcis API</h3>
                                    </div>
                                    <div class="box-widget fl-wrap">
                                        <div class="box-widget-content">
                                            <ul class="cat-item no-list-style">
                                                <li><a href="#1">Liste des Hôpitaux</a> <span>Publique</span></li>
                                                <li><a href="#2">Liste des Services d'un Hôpital</a> <span>Publique </span></li>
                                                <li><a href="#3">Liste des Besoins d'un Service</a> <span>Publique </span></li>
                                                <li><a href="#4">Agent Chargé d'un Service</a> <span>Publique </span></li>
                                                <li><a href="#5">État du Stock par Service</a> <span>Publique</span></li>
                                                <li><a href="#6">Statistiques Globales par Date</a> <span>Publique</span></li>
                                                <li><a href="#7">Liste des Gouvernorats pour les Statistiques</a> <span>Publique</span></li>
                                                <li><a href="#8">Statistiques par Gouvernorats par Date</a> <span>Publique</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-widget-item fl-wrap">
                                    <div class="banner-wdget fl-wrap">
                                        <div class="overlay op4"></div>
                                        <div class="bg"  data-bg="{{ asset('img/bg/19.jpg') }}"></div>
                                        <div class="banner-wdget-content fl-wrap">
                                            <h4>Covid.tn est Open Source, demandez l'accès au code a partir de notre formulaire de contact.</h4>
                                            <a href="{{ route('showContact') }}" class="color-bg">Partageons notre savoir</a>
                                        </div>
                                    </div>
                                </div>
                                <!--box-widget-item end -->
                            </div>
                        </div>
                        <!-- blog sidebar end -->
                    </div>
                </div>
            </div>
        </section>
        <div class="limit-box fl-wrap"></div>
    </div>
@endsection

@section('footer')
    @include('frontOffice.inc.footer')
@endsection
