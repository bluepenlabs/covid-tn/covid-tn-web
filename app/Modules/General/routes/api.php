<?php

Route::group(['module' => 'General', 'middleware' => ['api'], 'namespace' => 'App\Modules\General\Controllers'], function() {

    Route::get('/api/stats/global/{date}', 'ApiController@getGeneralStatisticsByDate')->name('getGeneralStatisticsByDate');
    Route::get('/api/stats/cities/{limit?}/{offset?}', 'ApiController@getStatisticsCities')->name('getStatisticsCities');
    Route::get('/api/stats/city/{id}/{date}', 'ApiController@getCityStatisticsByDate')->name('getCityStatisticsByDate');

});
