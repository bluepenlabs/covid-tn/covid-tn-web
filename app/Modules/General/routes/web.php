<?php

Route::group(['module' => 'General', 'middleware' => ['web'], 'namespace' => 'App\Modules\General\Controllers'], function() {

    Route::get('/', 'WebController@showHome')->name('showHome');
    Route::get('/statistics', 'WebController@showStatistics')->name('showStatistics');
    Route::get('/contact', 'WebController@showContact')->name('showContact');
    Route::get('/api', 'WebController@showAPI')->name('showAPI');
    Route::post('/contact', 'WebController@handleContact')->name('handleContact');

    Route::get('/manager', 'WebController@showManagerHome')->name('showManagerHome')->middleware('checkManagerAccess');
    Route::get('/manager/general/stats/sync', 'WebController@scheduleSyncStats')->name('scheduleSyncStats')->middleware('checkManagerAccess');
    Route::get('/manager/general', 'WebController@showManagerGeneralConfiguration')->name('showManagerGeneralConfiguration')->middleware('checkManagerAccess');
    Route::post('/manager/general/announcement/update', 'WebController@handleManagerEditAnnouncement')->name('handleManagerEditAnnouncement')->middleware('checkManagerAccess');

    Route::post('/manager/general/stats/gender/add', 'WebController@handleManagerAddGenderStatistics')->name('handleManagerAddGenderStatistics')->middleware('checkManagerAccess');
    Route::post('/manager/general/stats/gender/edit/{id}', 'WebController@handleManagerEditGenderStatistics')->name('handleManagerEditGenderStatistics')->middleware('checkManagerAccess');
    Route::get('/manager/general/stats/gender/delete/{id}', 'WebController@handleManagerDeleteGenderStatistics')->name('handleManagerDeleteGenderStatistics')->middleware('checkManagerAccess');

    Route::post('/manager/general/stats/city/add', 'WebController@handleManagerAddCityStatistics')->name('handleManagerAddCityStatistics')->middleware('checkManagerAccess');
    Route::post('/manager/general/stats/city/edit/{id}', 'WebController@handleManagerEditCityStatistics')->name('handleManagerEditCityStatistics')->middleware('checkManagerAccess');
    Route::get('/manager/general/stats/city/delete/{id}', 'WebController@handleManagerDeleteCityStatistics')->name('handleManagerDeleteCityStatistics')->middleware('checkManagerAccess');

    Route::post('/manager/general/stats/global/add', 'WebController@handleManagerAddGlobalStatistics')->name('handleManagerAddGlobalStatistics')->middleware('checkManagerAccess');
    Route::post('/manager/general/stats/global/edit/{id}', 'WebController@handleManagerEditGlobalStatistics')->name('handleManagerEditGlobalStatistics')->middleware('checkManagerAccess');
    Route::get('/manager/general/stats/global/delete/{id}', 'WebController@handleManagerDeleteGlobalStatistics')->name('handleManagerDeleteGlobalStatistics')->middleware('checkManagerAccess');

});
