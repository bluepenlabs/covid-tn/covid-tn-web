<?php

/**
 *	Notification Helper
 */

if (!function_exists('notifyDevices')) {
    /**
     * @param Illuminate\Support\Collection $deviceTokens
     * @param int $notificationTypeId
     * @return boolean
     */
    function notifyDevices($deviceTokens, $notificationTypeId)
    {
        try {
            // NotificationType represents a Table holding the notification message, title and sound
            $notificationType = \App\Modules\Notification\Models\NotificationType::find($notificationTypeId);

            // Preparing the notification
            $optionBuilder = new LaravelFCM\Message\OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);

            // Adding our notification parameters to the options
            $notificationBuilder = new LaravelFCM\Message\PayloadNotificationBuilder($notificationType->title);
            $notificationBuilder->setBody($notificationType->body)
                ->setSound($notificationType->sound);

            // Adding internal data that can be read on the device
            $dataBuilder = new LaravelFCM\Message\PayloadDataBuilder();
            $dataBuilder->addData(['notificationId' => $notificationType->id]);

            // Merging all together
            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            // Sending a notification to each token we have received
            foreach ($deviceTokens as $deviceToken) {
                $downstreamResponse = LaravelFCM\Facades\FCM::sendTo($deviceToken, $option, $notification, $data);

                // if a token must be modified, it will be received here, on key oldToken, holding the value of the new one
                foreach ($downstreamResponse->tokensToModify() as $item) {
                    \App\Modules\User\Models\UserDevice::where('token', '=', $deviceToken)->update([
                        'token' => $item
                    ]);
                }

                // As we are sending a notification to one device each time, the numberSuccess can only be 1 or 0
                if ($downstreamResponse->numberSuccess() == 1) {
                    $status = 1;
                } else {
                    $status = 3;
                }

                // We create a notification entry on the database, that will be updated in another place if the notification is opened
                $user = \App\Modules\User\Models\UserDevice::where('token', '=', $deviceToken)->first();
                \App\Modules\Notification\Models\Notification::create([
                    'type_id' => $notificationTypeId,
                    'receiver_id' => $user->id,
                    'status' => $status
                ]);
            }
            return true;
        } catch (Exception $exception){
            Illuminate\Support\Facades\Log::error('Push Notifications Error : ' . $exception);
            return false;
        }
    }
}
