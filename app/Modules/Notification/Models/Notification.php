<?php

namespace App\Modules\Notification\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_id',
        'receiver_id',
        'status',
    ];

    public function type()
    {
        return $this->hasOne('App\Modules\Notification\Models\NotificationType', 'id', 'type_id');
    }

    public function receiver()
    {
        return $this->hasOne('App\Modules\User\Models\User', 'id', 'receiver_id');
    }

}
