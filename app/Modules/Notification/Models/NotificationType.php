<?php

namespace App\Modules\Notification\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationType extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notification_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'body',
        'title',
        'sound',
        'icon',
        'color',
        'action',
    ];

    public function notifications()
    {
        return $this->hasMany('App\Modules\Notification\Models\Notification', 'type_id', 'id');
    }

}
