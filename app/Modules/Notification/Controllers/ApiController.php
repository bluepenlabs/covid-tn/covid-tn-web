<?php

namespace App\Modules\Notification\Controllers;

use App\Modules\Hospital\Models\HospitalService;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{

    public static function scheduleMedicalAgentReminder(){
        $notifiableServices = HospitalService::whereHas('user')->get();
        $toNotify = [];

        foreach($notifiableServices as $service){
            foreach($service->products as $product){
                $hasStock = $product->dayStock(now()->format('Y-m-d'))->first();
                if(!$hasStock){
                    array_push($toNotify, $service->user);
                    break;
                }
            }
        }
        $toNotify = collect($toNotify);

        $tokens = [];
        foreach($toNotify as $user){
            foreach($user->devices as $device){
                array_push($tokens, $device->token);
            }
        }

        notifyDevices(collect($tokens), 1);
    }
}
