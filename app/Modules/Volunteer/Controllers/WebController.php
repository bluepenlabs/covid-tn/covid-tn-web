<?php

namespace App\Modules\Volunteer\Controllers;

use App\Modules\General\Models\City;
use App\Modules\General\Models\General;
use App\Modules\Hospital\Models\Hospital;
use App\Modules\Hospital\Models\HospitalRequest;
use App\Modules\Hospital\Models\HospitalRequestEntry;
use App\Modules\Hospital\Models\HospitalService;
use App\Modules\Hospital\Models\HospitalServiceStock;
use App\Modules\Hospital\Models\Product;
use App\Modules\User\Models\User;
use App\Modules\Volunteer\Models\Category;
use App\Modules\Volunteer\Models\Organisation;
use App\Modules\Volunteer\Models\Volunteer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Kamaln7\Toastr\Facades\Toastr;

class WebController extends Controller
{

    public function showVolunteerUpdateProfile(){
        return view('Volunteer::frontOffice.private.profile', [
            'categories' => Category::all(),
            'cities' => City::all(),
            'organisations' => Organisation::all(),
            'services' => HospitalService::count(),
            'beds' => HospitalService::sum('beds'),
            'volunteers' => Volunteer::count(),
            'hospitals' => Hospital::count(),
        ]);
    }

    public function handleUpdateVolunteerProfile(Request $request){
        $data = $request->all();

        if(!isset($data['lat']) or !isset($data['lng']) or $data['lat'] == '' or $data['lng'] == ''){
            Toastr::error('Prière de vérifier l\'emplacement du marqueur sur la Map !');
            return back();
        }

        $days = new \stdClass();

        if(!isset($data['days'])){
            $days->d1 = $days->d2 = $days->d3 = $days->d4 = $days->d5 = $days->d6 = $days->d7 = 0;
        } else {
            $days->d1 = in_array(1, $data['days']) ? 1 : 0;
            $days->d2 = in_array(2, $data['days']) ? 1 : 0;
            $days->d3 = in_array(3, $data['days']) ? 1 : 0;
            $days->d4 = in_array(4, $data['days']) ? 1 : 0;
            $days->d5 = in_array(5, $data['days']) ? 1 : 0;
            $days->d6 = in_array(6, $data['days']) ? 1 : 0;
            $days->d7 = in_array(7, $data['days']) ? 1 : 0;
        }

        if(Auth::user()->volunteer){
            $volunteer = Volunteer::find(Auth::user()->volunteer->id);

            $volunteer->status = isset($data['status']) ? 1 : 0;
            $volunteer->organisation_id = $data['organisation'] != -1 ? $data['organisation'] : null;
            $volunteer->age = $data['age'];
            $volunteer->gender = $data['gender'];
            $volunteer->days = json_encode($days);
            $volunteer->lat = $data['lat'];
            $volunteer->lng = $data['lng'];
            $volunteer->city_id = $data['city_id'];

                Volunteer::find($volunteer->id)->categories()->detach();

                $volunteer->save();
        } else {
            $volunteer = Volunteer::create([
                'status' => isset($data['availability']) ? 1 : 0,
                'organisation_id' =>  $data['organisation'] != -1 ? $data['organisation'] : null,
                'age' => $data['age'],
                'user_id' => Auth::user()->id,
                'gender' => $data['gender'],
                'lng' => $data['lng'],
                'lat' => $data['lat'],
                'city_id' => $data['city_id'],
                'days' => json_encode($days),
            ]);
        }

        if($data['phone'] != null){
            $user = User::find(Auth::id());
            $user->phone = $data['phone'];
            $user->save();
        }

        if(isset($data['categories'])){
            foreach($data['categories'] as $category){
                Volunteer::find($volunteer->id)->categories()->attach($category);
            }
        }

        Toastr::success('Mise à jour effectuée avec succès !');
        return back();
    }

    public function showVolunteerPublicProfile($id){
        return view('Volunteer::frontOffice.public.single', [
            'volunteer' => Volunteer::find($id),
            'categories' => Category::all(),
            'hospital' => Hospital::find(1)
        ]);
    }

    public function showManagerVolunteersList(){
        return view('Volunteer::backOffice.volunteers', [
            'volunteers' => Volunteer::all(),
            'organisations' => Organisation::all(),
            'cities' => City::all(),
            'categories' => Category::all(),
            'users' => User::whereDoesntHave('volunteer')->get()
        ]);
    }

    public function showManagerVolunteersCategoriesList(){
        return view('Volunteer::backOffice.categories', [
            'categories' => Category::all(),
        ]);
    }

    public function showManagerVolunteersOrganisationsList(){
        return view('Volunteer::backOffice.organisations', [
            'organisations' => Organisation::all(),
        ]);
    }

    public function handleManagerEditVolunteer($id, Request $request){
        $data = $request->all();

        $volunteer = Volunteer::find($id);

        $volunteer->age = $data['age'];
        $volunteer->organisation_id = $data['organisation'] != -1 ? $data['organisation'] : null;
        $volunteer->gender = $data['gender'];
        $volunteer->lat = $data['lat'];
        $volunteer->lng = $data['lng'];
        $volunteer->city_id = $data['city_id'];

        $days = new \stdClass();

        if(!isset($data['days'])){
            $days->d1 = $days->d2 = $days->d3 = $days->d4 = $days->d5 = $days->d6 = $days->d7 = 0;
        } else {
            $days->d1 = in_array(1, $data['days']) ? 1 : 0;
            $days->d2 = in_array(2, $data['days']) ? 1 : 0;
            $days->d3 = in_array(3, $data['days']) ? 1 : 0;
            $days->d4 = in_array(4, $data['days']) ? 1 : 0;
            $days->d5 = in_array(5, $data['days']) ? 1 : 0;
            $days->d6 = in_array(6, $data['days']) ? 1 : 0;
            $days->d7 = in_array(7, $data['days']) ? 1 : 0;
        }

        $volunteer->days = json_encode($days);

        Volunteer::find($volunteer->id)->categories()->detach();
        foreach($data['categories'] as $category){
            Volunteer::find($volunteer->id)->categories()->attach($category);
        }

        $volunteer->save();

        Toastr::success('Mise à jour effectuée avec succès !');
        return back();
    }

    public function handleManagerAddVolunteer(Request $request){
        $data = $request->all();

        $days = new \stdClass();

        if(!isset($data['days'])){
            $days->d1 = $days->d2 = $days->d3 = $days->d4 = $days->d5 = $days->d6 = $days->d7 = 0;
        } else {
            $days->d1 = in_array(1, $data['days']) ? 1 : 0;
            $days->d2 = in_array(2, $data['days']) ? 1 : 0;
            $days->d3 = in_array(3, $data['days']) ? 1 : 0;
            $days->d4 = in_array(4, $data['days']) ? 1 : 0;
            $days->d5 = in_array(5, $data['days']) ? 1 : 0;
            $days->d6 = in_array(6, $data['days']) ? 1 : 0;
            $days->d7 = in_array(7, $data['days']) ? 1 : 0;
        }

        $volunteer = Volunteer::create([
            'status' => isset($data['status']) ? 1 : 0,
            'organisation_id' =>  $data['organisation'] != -1 ? $data['organisation'] : null,
            'age' => $data['age'],
            'user_id' => $data['user'],
            'gender' => $data['gender'],
            'city_id' => $data['city_id'],
            'lng' => $data['lng'],
            'lat' => $data['lat'],
            'days' => json_encode($days),
        ]);

        foreach($data['categories'] as $category){
            Volunteer::find($volunteer->id)->categories()->attach($category);
        }

        Toastr::success('Volontaire ajouté avec succès !');
        return back();
    }

    public function handleUserContactVolunteer($id, Request $request){
        $data = $request->all();

        $volunteer = Volunteer::find($id);

        $content = [
            'name' => $data['name'],
            'email' =>  $data['email'],
            'content' => $data['message'],
        ];

        Mail::send('Volunteer::mail.contact', $content, function ($message) use ($data, $volunteer) {
            $message->from($data['email']);
            $message->to($volunteer->user->email);
            $message->subject('Nouveau Message de : ' . $data['name']);
        });

        if(count(Mail::failures()) > 0 ) {
            Toastr::warning('L\'envoi des mails a échoué, contactez le support');
        }

        Toastr::success('Message envoyé à l\'administration');
        return redirect(route('showHome'));
    }

    public function handleManagerEditVolunteerCategory($id, Request $request){
        $data = $request->all();

        $category = Category::find($id);
        $category->title = $data['title'];
        $category->save();

        Toastr::success('Catégorie modifiée avec succès !');
        return back();
    }

    public function handleManagerEditVolunteerOrganisation($id, Request $request){
        $data = $request->all();

        $organisation = Organisation::find($id);
        $organisation->title = $data['title'];
        $organisation->save();

        Toastr::success('Organisation modifiée avec succès !');
        return back();
    }

    public function handleManagerAddVolunteerCategory(Request $request){
        $data = $request->all();

        Category::create([
            'title' => $data['title']
        ]);

        Toastr::success('Catégorie ajoutée avec succès !');
        return back();
    }

    public function handleManagerAddVolunteerOrganisation(Request $request){
        $data = $request->all();

        Organisation::create([
            'title' => $data['title']
        ]);

        Toastr::success('Organisation ajoutée avec succès !');
        return back();
    }

    public function handleManagerDeleteVolunteerCategory($id){
        $category = Category::find($id);

        if($category->volunteers->count() > 0){
            foreach($category->volunteers as $volunteer){
                Volunteer::find($volunteer->id)->categories()->detach($category);
            }
        }

        $category->destroy();

        Toastr::warning('Catégorie supprimée avec succès');
        return back();
    }

    public function handleManagerDeleteVolunteerOrganisation($id){
        $organisation = Organisation::find($id);

        if($organisation->volunteers->count() > 0){
            foreach($organisation->volunteers as $volunteer){
                $volunteer->organisation_id = null;
                $volunteer->save();
            }
        }

        $organisation->delete();

        Toastr::warning('Organisation supprimée avec succès');
        return back();
    }

    public function showVolunteers($offset = 1){
        $volunteers = Volunteer::all();

        return view('Volunteer::frontOffice.public.search', [
            'volunteers' => $volunteers,
            'volunteersOnMap' => $volunteers,
            'products' => Product::all(),
            'organisations' => Organisation::all(),
            'cities' => City::all(),
            'announcement' => General::all()[0]->announcement,
            'volunteersCategories' => Category::all(),
            'offset' => $offset,
        ]);
    }

    public function handleSearchVolunteer(Request $request, $offset = 1){
        $data = $request->all();

        $ages = explode(";",$data['age']);

        $volunteers = Volunteer::when($data['city'] != -1, function ($query) use ($data) {
                        $query->where('city_id', '=', $data['city']);
                    })->when($data['organisation'] != -1, function ($query) use ($data) {
                        $query->where('organisation_id', '=', $data['organisation']);
                    })->when($data['service'] != -1, function ($query) use ($data) {
                        $query->whereHas('categories', function ($query) use ($data) {
                            $query->where('category_id', '=', $data['service']);
                        });
                    })
                    ->whereBetween('age', [$ages[0], $ages[1]])
            ->skip(($offset - 1) * 10)
            ->limit(10)
            ->get();

        $count= $volunteers->count();

        if($count == 0){
            Toastr::warning('Aucun volontaire trouvé !');
            return redirect(route('showVolunteers'));
        }

        return view('Volunteer::frontOffice.public.search', [
            'volunteers' => $volunteers,
            'count' => $count,
            'volunteersOnMap' => Volunteer::all(),
            'products' => Product::all(),
            'cities' => City::all(),
            'organisations' => Organisation::all(),
            'announcement' => General::all()[0]->announcement,
            'volunteersCategories' => Category::all(),
            'offset' => $offset,
            'ages' => $ages,
        ]);
    }
}
