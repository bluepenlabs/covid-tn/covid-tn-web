<?php

namespace App\Modules\Volunteer\Models;

use Illuminate\Database\Eloquent\Model;

class Organisation extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'volunteer_organisations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'logo',
        'phone',
        'description',
    ];

    public function volunteers()
    {
        return $this->hasMany('App\Modules\Volunteer\Models\Volunteer', 'organisation_id', 'id');
    }
}
