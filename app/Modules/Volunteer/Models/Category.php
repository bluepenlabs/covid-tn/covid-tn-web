<?php

namespace App\Modules\Volunteer\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'volunteering_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
    ];

    public function volunteers()
    {
        return $this->belongsToMany(
            'App\Modules\Volunteer\Models\Volunteer',
            'volunteer_categories',
            'category_id',
            'volunteer_id'
        )->withTimestamps();
    }
}
