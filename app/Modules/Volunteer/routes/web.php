<?php

Route::group(['module' => 'Volunteer', 'middleware' => ['web'], 'namespace' => 'App\Modules\Volunteer\Controllers'], function() {

    Route::get('/volunteer/profile/update', 'WebController@showVolunteerUpdateProfile')->name('showVolunteerUpdateProfile');
    Route::post('/volunteer/profile/update', 'WebController@handleUpdateVolunteerProfile')->name('handleUpdateVolunteerProfile');

    Route::get('/volunteer/view/{id}', 'WebController@showVolunteerPublicProfile')->name('showVolunteerPublicProfile');
    Route::post('/volunteer/contact/{id}', 'WebController@handleUserContactVolunteer')->name('handleUserContactVolunteer');
    Route::get('/volunteers', 'WebController@showVolunteers')->name('showVolunteers');

    Route::post('/volunteers/search/{offset?}', 'WebController@handleSearchVolunteer')->name('handleSearchVolunteer');

    Route::get('/manager/volunteers', 'WebController@showManagerVolunteersList')->name('showManagerVolunteersList')->middleware('checkManagerAccess');
    Route::get('/manager/volunteers/categories', 'WebController@showManagerVolunteersCategoriesList')->name('showManagerVolunteersCategoriesList')->middleware('checkManagerAccess');
    Route::get('/manager/volunteers/organisations', 'WebController@showManagerVolunteersOrganisationsList')->name('showManagerVolunteersOrganisationsList')->middleware('checkManagerAccess');

    Route::post('/manager/volunteers/edit/{id}', 'WebController@handleManagerEditVolunteer')->name('handleManagerEditVolunteer')->middleware('checkManagerAccess');
    Route::post('/manager/volunteers/add', 'WebController@handleManagerAddVolunteer')->name('handleManagerAddVolunteer')->middleware('checkManagerAccess');

    Route::post('/manager/volunteers/category/edit/{id}', 'WebController@handleManagerEditVolunteerCategory')->name('handleManagerEditVolunteerCategory')->middleware('checkManagerAccess');
    Route::post('/manager/volunteers/category/add', 'WebController@handleManagerAddVolunteerCategory')->name('handleManagerAddVolunteerCategory')->middleware('checkManagerAccess');
    Route::get('/manager/volunteers/category/delete/{id}', 'WebController@handleManagerDeleteVolunteerCategory')->name('handleManagerDeleteVolunteerCategory')->middleware('checkManagerAccess');

    Route::post('/manager/volunteers/organisation/edit/{id}', 'WebController@handleManagerEditVolunteerOrganisation')->name('handleManagerEditVolunteerOrganisation')->middleware('checkManagerAccess');
    Route::post('/manager/volunteers/organisation/add', 'WebController@handleManagerAddVolunteerOrganisation')->name('handleManagerAddVolunteerOrganisation')->middleware('checkManagerAccess');
    Route::get('/manager/volunteers/organisation/delete/{id}', 'WebController@handleManagerDeleteVolunteerOrganisation')->name('handleManagerDeleteVolunteerOrganisation')->middleware('checkManagerAccess');

});
