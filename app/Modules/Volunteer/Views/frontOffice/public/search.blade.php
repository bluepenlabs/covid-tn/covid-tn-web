@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - Sauvons la Tunisie ENSEMBLE',
    'description' => 'Sauvons la Tunisie ENSEMBLE - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header',
    [
        'actual' => 'volunteers',
        'cat' => ''
    ])
@endsection

@section('content')
    <script>
        function paginate(current) {
            var form = document.getElementById('search');
            form.action = '{{ route('handleSearchVolunteer') }}/' + current;
            form.submit();
        }
    </script>
    <!-- content-->
    <div class="content">
        <!-- Map -->
        <div class="map-container  fw-map big_map hid-mob-map">
            <div id="map-main"></div>
            <div class="location-btn geoLocation tolt" data-microtip-position="top-left"
                 data-tooltip="Votre emplacement">
                <span><i class="fal fa-location"></i></span></div>
            <div class="map-close"><i class="fas fa-times"></i></div>
        </div>
        <!-- Map end -->
        <div class="clearfix"></div>
        <section class="gray-bg small-padding no-top-padding-sec">
            <div class="container">
                <div class="breadcrumbs inline-breadcrumbs fl-wrap block-breadcrumbs">
                    <a href=""><strong>ANNONCES</strong></a><span>{!! $announcement !!}</span>
                </div>
                <div class="fl-wrap">
                    <div class="mob-nav-content-btn mncb_half color2-bg show-list-wrap-search ntm fl-wrap"><i
                                class="fal fa-filter"></i> Filtres
                    </div>
                    <div class="mob-nav-content-btn mncb_half color2-bg schm ntm fl-wrap"><i
                                class="fal fa-map-marker-alt"></i> Ouvrir la Map
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class=" fl-wrap  lws_mobile  tabs-act block_box">
                                <div class="filter-sidebar-header fl-wrap" id="filters-column">
                                    <ul class="tabs-menu fl-wrap no-list-style">
                                        <li><a href="#filters-search"> <i class="fal fa-sliders-h"></i>
                                                Hôpitaux </a></li>
                                        <li class="current"><a href="#category-search"> <i class="fal fa-image"></i>Volontaires
                                            </a></li>
                                    </ul>
                                </div>
                                <div class="filter-sidebar fs-viscon">
                                    <!--tabs -->
                                    <div class="tabs-container fl-wrap">
                                        <form action="{{ route('handleSearchHospital') }}" method="post">
                                            {{ csrf_field() }}
                                            <div class="tab">
                                                <div id="filters-search" class="tab-content">
                                                    <!-- listsearch-input-item-->
                                                    <div class="listsearch-input-item">
                                                        <span class="iconn-dec"><i class="far fa-bookmark"></i></span>
                                                        <input type="text" name="title"
                                                               placeholder="Hôpital spécifique ?"/>
                                                    </div>
                                                    <!-- listsearch-input-item end-->
                                                    <!-- listsearch-input-item-->
                                                    <div class="listsearch-input-item">
                                                        <select data-placeholder="Type"
                                                                class="chosen-select no-search-select" name="type">
                                                            <option value="-1">Tout Type</option>
                                                            <option disabled>--------------</option>
                                                            <option value="1">Publique</option>
                                                            <option value="2">Privé</option>
                                                            <option value="3">Forces de l'Ordre</option>
                                                            <option value="4">Universitaire</option>
                                                        </select>
                                                    </div>
                                                    <!-- listsearch-input-item end-->
                                                    <!-- listsearch-input-item-->
                                                    <div class="listsearch-input-item">
                                                        <select data-placeholder="Cité/Gouvernorat" name="city"
                                                                class="chosen-select no-search-select">
                                                            <option value="-1">Tunisie</option>
                                                            <option disabled>--------------</option>
                                                            @foreach($cities as $city)
                                                                <option value="{{ $city->id }}">{{ $city->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="listsearch-input-item">
                                                        <select multiple data-placeholder="Besoins" name="needs[]"
                                                                class="chosen-select bes-select">
                                                            @foreach($products as $product)
                                                                <option  @if(isset($data) and isset($data['needs']) and in_array($type->id, $data['needs'])) selected @endif value="{{ $product->id }}">{{ $product->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <!-- listsearch-input-item end-->
                                                    <!-- listsearch-input-item-->
                                                    <div class="listsearch-input-item">
                                                        <button class="header-search-button color-bg"
                                                                type="submit"><i
                                                                    class="far fa-search"></i><span>Rechercher</span>
                                                        </button>
                                                    </div>
                                                    <!-- listsearch-input-item end-->
                                                    <button style="background: transparent;border: 0;" type="reset"
                                                            class="clear-filter-btn"><i class="far fa-redo"></i>
                                                        Réinitialiser
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <form action="{{ route('handleSearchVolunteer') }}" id="search" method="post">
                                            <div class="tab">
                                                <div id="category-search" class="tab-content first-tab">
                                                    <div class="fl-wrap">

                                                        {{ csrf_field() }}
                                                        <div class="listsearch-input-item">
                                                            <select data-placeholder="Organisation" name="organisation"
                                                                    class="chosen-select no-search-select">
                                                                <option value="-1">Aucune</option>
                                                                <option disabled>--------------</option>
                                                                @foreach($organisations as $organisation)
                                                                    <option value="{{ $organisation->id }}">{{ $organisation->title }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="listsearch-input-item">
                                                            <select data-placeholder="Cité/Gouvernorat" name="city"
                                                                    class="chosen-select no-search-select">
                                                                <option value="-1">Tunisie</option>
                                                                <option disabled>--------------</option>
                                                                @foreach($cities as $city)
                                                                    <option  @if(isset($data) and $data['city'] == $city->id) selected @endif value="{{ $city->id }}">{{ $city->title }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="listsearch-input-item">
                                                            <select data-placeholder="Cité/Gouvernorat" name="service"
                                                                    class="chosen-select no-search-select">
                                                                <option value="-1">Tout Service</option>
                                                                <option disabled>--------------</option>
                                                                @foreach($volunteersCategories as $category)
                                                                    <option  @if(isset($data) and $data['service'] == $category->id) selected @endif value="{{ $category->id }}">{{ $category->title }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="listsearch-input-item">
                                                            <div class="price-rage-wrap fl-wrap">
                                                                <div class="price-rage-wrap-title"><i
                                                                            class="fal fa-user"></i> Age :
                                                                </div>
                                                                <div class="price-rage-item fl-wrap">
                                                                    <input type="text" name="age" class="range-slider"
                                                                           data-min="16" @if(isset($ages)) data-from="{{ $ages[0] }}" data-to="{{ $ages[1] }}" @endif data-max="65" data-step="1"
                                                                           value="ans">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="listsearch-input-item">
                                                            <button class="header-search-button color-bg"
                                                                    type="submit"><i
                                                                        class="far fa-search"></i><span>Rechercher</span>
                                                            </button>
                                                        </div>
                                                        <!-- listsearch-input-item end-->
                                                        <button style="background: transparent;border: 0;" type="reset"
                                                                class="clear-filter-btn"><i class="far fa-redo"></i>
                                                            Réinitialiser
                                                        </button>

                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <!--tab end-->
                                    </div>
                                    <!--tabs end-->
                                </div>
                            </div>
                            <a class="back-tofilters color2-bg custom-scroll-link fl-wrap" href="#filters-column">Retourner
                                aux filtres <i class="fas fa-caret-up"></i></a>
                        </div>
                        <div class="col-md-8">
                            <!-- list-main-wrap-header-->
                            <div class="list-main-wrap-header fl-wrap block_box no-vis-shadow">
                                <!-- list-main-wrap-title-->
                                <div class="list-main-wrap-title">
                                    @if(isset($searchCount)) <h2>{{ $searchCount }} </h2> <span>Résultat(s) Trouvé(s)</span> @else<h2>{{ count($volunteers) }} <span>Volontaires</span></h2>@endif
                                </div>
                                <!-- list-main-wrap-opt end-->
                            </div>
                            <!-- list-main-wrap-header end-->
                            <!-- listing-item-container -->
                            <div class="listing-item-container init-grid-items fl-wrap nocolumn-lic">

                                @foreach($volunteers as $volunteer)
                                    <div class="listing-item">
                                        <article class="geodir-category-listing fl-wrap">
                                            <div class="geodir-category-img">
                                                <a class="geodir-category-img-wrap fl-wrap">
                                                    <img @if($volunteer->user->photo) src="{{ asset($volunteer->user->photo) }}"
                                                         @else src="{{ asset('img/vlnt.jpg') }}" @endif alt="">
                                                </a>
                                                <div class="geodir_status_date @if($volunteer->status == 1) gsd_open @else gsd_close @endif">
                                                    <i class="fal @if($volunteer->status == 1) fa-check @else fa-times @endif"></i>{{ $volunteer->status == 1 ? 'Disponible' : 'Indispo.' }}
                                                </div>
                                                <div class="geodir-category-opt">
                                                    <div class="listing-rating-count-wrap">
                                                        <div class="review-score">{{ $volunteer->age }} ans</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="geodir-category-content fl-wrap">
                                                <div class="geodir-category-content-title fl-wrap">
                                                    <div class="geodir-category-content-title-item">
                                                        <h3 class="title-sin_map"><a
                                                                    href="{{ route('showVolunteerPublicProfile', ['id' => $volunteer->id]) }}">{{ $volunteer->user->name }}</a>
                                                        </h3>
                                                        <div class="geodir-category-location fl-wrap"><a href="#1"
                                                                                                         class="map-item"><i
                                                                        class="fas fa-map-marker-alt"></i> @if($volunteer->city_id) {{ $volunteer->city->title }} @endif
                                                            </a></div>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-text fl-wrap">
                                                    @if(count($volunteer->categories) == 0)
                                                        <p class="small-text">Aucune catégorie d'aide spécifiée</p>
                                                    @else
                                                        <div class="list-single-tags tags-stylwrap">
                                                            @foreach($volunteer->categories as $category)
                                                                <a>{{ $category->title }} </a>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="geodir-category-footer fl-wrap">
                                                    <a href="{{ route('showVolunteerPublicProfile', ['id' => $volunteer->id]) }}"
                                                       class="listing-item-category-wrap">
                                                        <div class="listing-item-category green-bg"><i
                                                                    class="fal fa-info"></i></div>
                                                        <span>Plus de Détails</span>
                                                    </a>
                                                    <div class="geodir-opt-list">
                                                        <ul class="no-list-style">
                                                            @if($volunteer->user->phone)
                                                                <li><a href="#" class="show_gcc"><i
                                                                                class="fal fa-phone"></i><span
                                                                                class="geodir-opt-tooltip">Téléphone</span></a>
                                                                </li>
                                                            @endif
                                                            <li><a href="#{{$loop->iteration}}" class="map-item"><i
                                                                            class="fal fa-map-marker-alt"></i><span
                                                                            class="geodir-opt-tooltip">Sur la Map</span>
                                                                </a></li>

                                                        </ul>
                                                    </div>

                                                    <div class="geodir-category_contacts">
                                                        <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                                                        <ul class="no-list-style">
                                                            <li><span><i class="fal fa-phone"></i> Téléphone : </span><a
                                                                        href="">{{ $volunteer->user->phone }}</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                @endforeach

                                <div class="pagination fwmpag">
                                    @if(count($volunteers) > 10)
                                        @for($i = 0; $i < (round(count($volunteers)/10)); $i++)
                                            <a class="@if($i === $offset - 1) current-page @endif"
                                               onclick="paginate({{$i + 1}})">{{ $i + 1}}</a>
                                        @endfor
                                    @endif
                                </div>
                            </div>
                            <!-- listing-item-container end -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="limit-box fl-wrap"></div>
    </div>
    <!--content end-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRJQqA2pOFX-shd3sSVlMQb9vXdAZQsjo&libraries=places&callback=initAutocomplete"></script>
    <script src="{{ asset('js/frontOffice/map-plugins.js')}}"></script>
    <script>
        (function ($) {
            "use strict";

            function mainMap() {
                function volunteerData(volunteerURL, volunteerCategories, volunteerTitle, volunteerPicture, volunteerOrganisation) {
                    let categories = "";
                    if (volunteerCategories.length > 0) {
                        for (i = 0; i < volunteerCategories.length; i++) {
                            categories = categories + '<div class="map-popup-location-info" style="margin-top:2px;font-size: 13px;"><i class="fas fa-hand-holding"></i>' + volunteerCategories[i] + '</div>'
                        }
                    } else {
                        categories = '<div class="map-popup-location-info" style="margin-top:2px;font-size: 13px;"><i class="fas fa-minus"></i>Aucune catégorie pour ce volontaire </div>'
                    }
                    return (
                        '<div class="map-popup-wrap">' +
                        '   <div class="map-popup">' +
                        '       <div class="infoBox-close">' + '<i class="fal fa-times"></i>' + '</div>' +
                        '       <a href="' + volunteerURL + '" class="listing-img-content fl-wrap">' +
                        '           <img style="height:180px" src="' + volunteerPicture + '" alt="">' +
                        '           <div class="card-popup-raining"><span class="map-popup-reviews-count">' + volunteerOrganisation + '</span></div>' +
                        '       </a> ' +
                        '   <div class="listing-content">' +
                        '       <div class="listing-content-item fl-wrap">' +
                        '               <div class="listing-title fl-wrap">' +
                        '                   <h4 style="margin-bottom: 13px;"><a href=' + volunteerURL + '>' + volunteerTitle + '</a></h4>' +
                        categories +
                        '               </div>' +
                        '           <div class="map-popup-footer">' +
                        '               <a href=' + volunteerURL + ' class="main-link">Plus de Détails <i class="fal fa-long-arrow-right"></i></a>' +
                        '           </div>' +
                        '       </div>' +
                        '   </div>' +
                        '</div> ')
                }

                var volunteerLocations = [
                        @foreach($volunteersOnMap as $volunteer)
                    [volunteerData(
                        '{{ route('showVolunteerPublicProfile', ['id' => $volunteer->id]) }}',
                        [
                            @foreach($volunteer->categories as $category)
                                "{{ $category->title }}",
                            @endforeach
                        ]
                        ,
                        '{{ $volunteer->user->name }}',
                        '{{ $volunteer->user->photo != '' ? asset($volunteer->user->photo) : asset('img/vlnt.jpg')  }}',
                        '{{ $volunteer->organisation }}'
                    ),
                        {{ $volunteer->lat }}, {{ $volunteer->lng }}, '{{ asset('img/volun.png') }}'],
                    @endforeach
                ];

                //   Map Infoboxes end ------------------
                var map = new google.maps.Map(document.getElementById('map-main'), {
                    zoom: 6,
                    scrollwheel: true,
                    center: new google.maps.LatLng(34.2651487, 9.4225926),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoomControl: false,
                    mapTypeControl: false,
                    scaleControl: false,
                    panControl: false,
                    fullscreenControl: true,
                    navigationControl: false,
                    streetViewControl: false,
                    animation: google.maps.Animation.BOUNCE,
                    gestureHandling: 'cooperative',
                    styles: [
                        {
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#212121"
                                }
                            ]
                        },
                        {
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "color": "#212121"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.country",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#9e9e9e"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.land_parcel",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.locality",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#bdbdbd"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.medical",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#ff122f"
                                },
                                {
                                    "lightness": 100
                                },
                                {
                                    "weight": 8
                                }
                            ]
                        },
                        {
                            "featureType": "poi.medical",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#ff122f"
                                },
                                {
                                    "lightness": 100
                                },
                                {
                                    "weight": 8
                                }
                            ]
                        },
                        {
                            "featureType": "poi.medical",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "color": "#ff122f"
                                },
                                {
                                    "lightness": 100
                                }
                            ]
                        },
                        {
                            "featureType": "poi.medical",
                            "elementType": "labels.text",
                            "stylers": [
                                {
                                    "color": "#ff122f"
                                },
                                {
                                    "lightness": 100
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#181818"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#616161"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "color": "#1b1b1b"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#2c2c2c"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#8a8a8a"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#373737"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#3c3c3c"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway.controlled_access",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#4e4e4e"
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#616161"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#000000"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#3d3d3d"
                                }
                            ]
                        }
                    ]
                });
                var boxText = document.createElement("div");
                boxText.className = 'map-box'
                var currentInfobox;
                var boxOptions = {
                    content: boxText,
                    disableAutoPan: true,
                    alignBottom: true,
                    maxWidth: 0,
                    pixelOffset: new google.maps.Size(-150, -55),
                    zIndex: null,
                    boxStyle: {
                        width: "300px"
                    },
                    closeBoxMargin: "0",
                    closeBoxURL: "",
                    infoBoxClearance: new google.maps.Size(1, 1),
                    isHidden: false,
                    pane: "floatPane",
                    enableEventPropagation: false,
                };

                var markerCluster, overlay, i;
                var allMarkers = [];

                var clusterStyles = [
                    {
                        textColor: 'white',
                        url: '',
                        height: 50,
                        width: 50
                    }
                ];

                var ib = new InfoBox();
                google.maps.event.addListener(ib, "domready", function () {
                    cardRaining();

                });
                var markerImg;
                var markerCount;

                for (var j = 0; j < volunteerLocations.length; j++) {
                    markerImg = volunteerLocations[j][3];
                    markerCount = j;
                    var overlaypositions = new google.maps.LatLng(volunteerLocations[j][1], volunteerLocations[j][2]),

                        overlay = new CustomMarker(
                            overlaypositions, map, {marker_id: j}, markerImg, ''
                        );

                    allMarkers.push(overlay);

                    google.maps.event.addDomListener(overlay, 'click', (function (overlay, j) {

                        return function () {
                            ib.setOptions(boxOptions);
                            boxText.innerHTML = volunteerLocations[j][0];
                            ib.close();
                            ib.open(map, overlay);
                            currentInfobox = volunteerLocations[j][3];

                            var latLng = new google.maps.LatLng(volunteerLocations[j][1], volunteerLocations[j][2]);
                            map.panTo(latLng);
                            map.panBy(0, -160);

                            google.maps.event.addListener(ib, 'domready', function () {
                                $('.infoBox-close').click(function (e) {
                                    e.preventDefault();
                                    ib.close();
                                    $('.map-marker-container').removeClass('clicked infoBox-opened');
                                });

                            });

                        }
                    })(overlay, j));
                }
                var options2 = {
                    imagePath: '',
                    styles: clusterStyles,
                    minClusterSize: 2
                };
                markerCluster = new MarkerClusterer(map, allMarkers, options2);
                google.maps.event.addDomListener(window, "resize", function () {
                    var center = map.getCenter();
                    google.maps.event.trigger(map, "resize");
                    map.setCenter(center);
                });

                $('.map-item').on("click", function (e) {
                    e.preventDefault();
                    map.setZoom(10);

                    var marker_index = parseInt($(this).attr('href').split('#')[1], 10);
                    google.maps.event.trigger(allMarkers[marker_index - 1], "click");
                    if ($(window).width() > 1064) {
                        if ($(".map-container").hasClass("fw-map")) {
                            $('html, body').animate({
                                scrollTop: $(".map-container").offset().top + "-110px"
                            }, 1000)
                            return false;
                        }
                    }
                });

                var zoomControlDiv = document.createElement('div');
                var zoomControl = new ZoomControl(zoomControlDiv, map);

                function ZoomControl(controlDiv, map) {
                    zoomControlDiv.index = 1;
                    map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);
                    controlDiv.style.padding = '5px';
                    var controlWrapper = document.createElement('div');
                    controlDiv.appendChild(controlWrapper);
                    var zoomInButton = document.createElement('div');
                    zoomInButton.className = "mapzoom-in";
                    controlWrapper.appendChild(zoomInButton);
                    var zoomOutButton = document.createElement('div');
                    zoomOutButton.className = "mapzoom-out";
                    controlWrapper.appendChild(zoomOutButton);
                    google.maps.event.addDomListener(zoomInButton, 'click', function () {
                        map.setZoom(map.getZoom() + 1);
                    });
                    google.maps.event.addDomListener(zoomOutButton, 'click', function () {
                        map.setZoom(map.getZoom() - 1);
                    });
                }


                // Geo Location Button
                $(".geoLocation, .input-with-icon.location a").on("click", function (e) {
                    e.preventDefault();
                    geolocate();
                });

                function geolocate() {

                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (position) {
                            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                            map.setCenter(pos);
                            map.setZoom(12);

                            var icon = {
                                url: '{{ asset('img/pin.png') }}',
                                scaledSize: new google.maps.Size(30, 30), // scaled size
                                origin: new google.maps.Point(0, 0), // origin
                                anchor: new google.maps.Point(0, 0) // anchor
                            };

                            new google.maps.Marker({
                                position: pos,
                                map: map,
                                icon: icon,
                                title: 'Votre emplacement'
                            });

                        });
                    }
                }
            }

            // Custom Map Marker
            // ----------------------------------------------- //

            function CustomMarker(latlng, map, args, markerImg, markerCount) {
                this.latlng = latlng;
                this.args = args;

                this.markerImg = markerImg;
                this.markerCount = markerCount;
                this.setMap(map);
            }

            CustomMarker.prototype = new google.maps.OverlayView();

            CustomMarker.prototype.draw = function () {

                var self = this;

                var div = this.div;

                if (!div) {

                    div = this.div = document.createElement('div');
                    div.className = 'map-marker-container';

                    if (self.markerCount !== '') {
                        div.innerHTML = '<div class="marker-container">' +
                            '<span class="marker-count">' + self.markerCount + '</span>' +
                            '<div class="marker-card">' +
                            '<div class="marker-holder"><img src="' + self.markerImg + '" alt=""></div>' +
                            '</div>' +
                            '</div>'
                    } else {
                        div.innerHTML = '<div class="marker-container">' +
                            '<div class="marker-card">' +
                            '<div class="marker-holder"><img src="' + self.markerImg + '" alt=""></div>' +
                            '</div>' +
                            '</div>'
                    }

                    // Clicked marker highlight
                    google.maps.event.addDomListener(div, "click", function (event) {
                        $('.map-marker-container').removeClass('clicked infoBox-opened');
                        google.maps.event.trigger(self, "click");
                        $(this).addClass('clicked infoBox-opened');
                    });


                    if (typeof (self.args.marker_id) !== 'undefined') {
                        div.dataset.marker_id = self.args.marker_id;
                    }

                    var panes = this.getPanes();
                    panes.overlayImage.appendChild(div);
                }

                var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

                if (point) {
                    div.style.left = (point.x) + 'px';
                    div.style.top = (point.y) + 'px';
                }
            };

            CustomMarker.prototype.remove = function () {
                if (this.div) {
                    this.div.parentNode.removeChild(this.div);
                    this.div = null;
                    $(this).removeClass('clicked');
                }
            };

            CustomMarker.prototype.getPosition = function () {
                return this.latlng;
            };

            // -------------- Custom Map Marker / End -------------- //


            var head = document.getElementsByTagName('head')[0];

// Save the original method
            var insertBefore = head.insertBefore;

// Replace it!
            head.insertBefore = function (newElement, referenceElement) {
                if (newElement.href && newElement.href.indexOf('https://fonts.googleapis.com/css?family=Roboto') === 0) {
                    return;
                }
                insertBefore.call(head, newElement, referenceElement);
            };

            var map = document.getElementById('map-main');
            if (typeof (map) != 'undefined' && map != null) {
                google.maps.event.addDomListener(window, 'load', mainMap);
            }

        })(this.jQuery);

    </script>
@endsection

@section('footer')
    @include('frontOffice.inc.footer')
@endsection

