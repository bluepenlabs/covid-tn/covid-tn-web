@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - Profil Bénévole ' . $volunteer->user->name,
    'description' => 'Profil Bénévole ' . $volunteer->user->name . ' - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header',
    [
        'actual' => 'stats',
        'cat' => ''
    ])
@endsection

@section('content')

    <div class="content">
        <!--section-->
        <section class="gray-bg small-padding no-top-padding-sec" id="sec1">
            <div class="container">
                <div class="breadcrumbs inline-breadcrumbs fl-wrap block-breadcrumbs">
                    <a href="{{ route('showHome') }}">Accueil</a><a href="{{ route('showVolunteers') }}">Bénévoles</a><span>{{ $volunteer->user->name }}</span>
                    <div class="showshare brd-show-share color2-bg"><i class="fas fa-share"></i> Partager</div>
                </div>
                <div class="share-holder hid-share sing-page-share top_sing-page-share">
                    <div class="share-container  isShare">
                    </div>
                </div>
                <div class="mob-nav-content-btn  color2-bg show-list-wrap-search ntm fl-wrap"><i
                            class="fal fa-filter"></i> Filtres
                </div>
                <div class="fl-wrap">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="fl-wrap block_box product-header">
                                <div class="product-header-details">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="list-single-main-item fl-wrap">
                                                <h2 class="post-opt-title" style="margin: 20px; margin-bottom: 0px;"><a
                                                    >Besoin de {{ $volunteer->user->name }} ?</a></h2>
                                                <span class="fw-separator"></span>
                                                <div class="clearfix"></div>
                                                <div class="box-widget-content">
                                                    <form class="add-comment custom-form"  method="post" action="{{ route('handleUserContactVolunteer', ['id' => $volunteer->id]) }}">
                                                        {{ csrf_field() }}
                                                        <fieldset>
                                                            <label><i class="fal fa-user"></i></label>
                                                            <input type="text" name="name" required placeholder="Votre Nom" value="@auth() {{ Auth::user()->name }} @endauth">
                                                            <div class="clearfix"></div>
                                                            <label><i class="fal fa-envelope"></i>  </label>
                                                            <input type="email" name="email" required placeholder="Votre Email" value="@auth() {{ Auth::user()->email }} @endauth">
                                                            <textarea cols="40" rows="3" name="message" required placeholder="Message"></textarea>
                                                        </fieldset>
                                                        <button class="btn float-btn color2-bg">Envoyer<i class="fal fa-paper-plane"></i></button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-3">

                            <!--box-widget-item -->
                            <div class="box-widget-item fl-wrap block_box">
                                <div class="box-widget-item-header">
                                    <h3>Informations Générales  </h3>
                                </div>
                                <div class="box-widget">
                                    <div class="box-widget-content bwc-nopad">
                                        <div class="list-author-widget-contacts list-item-widget-contacts bwc-padside">
                                            <ul class="no-list-style">
                                                <li><span><i class="fal fa-calendar"></i> Statut :</span>
                                                    <div style="right: 66px;" class="geodir_status_date @if($volunteer->status == 1) gsd_open @else gsd_close @endif">
                                                        <i class="fal @if($volunteer->status == 1) fa-check @else fa-times @endif"></i>{{ $volunteer->status == 1 ? 'Disponible' : 'Indispo.' }}
                                                    </div>
                                                </li>
                                                <li><span><i class="fal fa-birthday-cake"></i> Age :</span> <a href="#">{{ $volunteer->age }}</a></li>
                                                <li><span><i class="fal fa-users"></i> Sexe :</span> <a href="#">{{ $volunteer->gender == 1 ? 'Homme' : 'Femme' }}</a></li>
                                                <li><span><i class="fal fa-phone"></i> Téléphone :</span> <a href="#">{{ $volunteer->user->phone }}</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="box-widget-item fl-wrap block_box">
                                <div class="box-widget-item-header">
                                    <h3>Disponibilité</h3>
                                </div>
                                <div class="box-widget fl-wrap">
                                    <div class="box-widget-content">
                                        <div class="list-single-tags tags-stylwrap">
                                            <a @if($volunteer->days != null and json_decode($volunteer->days)->d1 == 1) style="background-color: #5ECFB1; color: #fff;" @else style="background-color: #E9776D; color: #fff;" @endif>Lundi </a>
                                            <a @if($volunteer->days != null and json_decode($volunteer->days)->d2 == 1) style="background-color: #5ECFB1; color: #fff;" @else style="background-color: #E9776D; color: #fff;" @endif>Mardi </a>
                                            <a @if($volunteer->days != null and json_decode($volunteer->days)->d3 == 1) style="background-color: #5ECFB1; color: #fff;" @else style="background-color: #E9776D; color: #fff;" @endif>Mercredi </a>
                                            <a @if($volunteer->days != null and json_decode($volunteer->days)->d4 == 1) style="background-color: #5ECFB1; color: #fff;" @else style="background-color: #E9776D; color: #fff;" @endif>Jeudi </a>
                                            <a @if($volunteer->days != null and json_decode($volunteer->days)->d5 == 1) style="background-color: #5ECFB1; color: #fff;" @else style="background-color: #E9776D; color: #fff;" @endif>Vendredi </a>
                                            <a @if($volunteer->days != null and json_decode($volunteer->days)->d6 == 1) style="background-color: #5ECFB1; color: #fff;" @else style="background-color: #E9776D; color: #fff;" @endif>Samedi </a>
                                            <a @if($volunteer->days != null and json_decode($volunteer->days)->d7 == 1) style="background-color: #5ECFB1; color: #fff;" @else style="background-color: #E9776D; color: #fff;" @endif>Dimanche </a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->
                            <!--box-widget-item -->
                            <!--box-widget-item end -->
                        </div>

                        <div class="col-md-3">
                            <div class="box-widget-item fl-wrap block_box">
                                <div class="box-widget-item-header">
                                    <h3>Aide Possible</h3>
                                </div>
                                <div class="box-widget fl-wrap">
                                    <div class="box-widget-content">
                                        <div class="list-single-tags tags-stylwrap">
                                            @foreach($volunteer->categories as $category)
                                                <a>{{ $category->title }} </a>
                                                @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="fl-wrap lws_mobile vishidelem">
                                <div class="box-widget-item fl-wrap block_box">
                                    <div class="box-widget-item-header">
                                        <h3>Localisation </h3>
                                    </div>
                                    <div class="box-widget">
                                        <div class="map-container">
                                            <div id="singleMap" data-latitude="{{ $volunteer->lat }}"
                                                 data-longitude="{{ $volunteer->lng }}"
                                                 data-mapTitle="{{ $volunteer->user->name }}"></div>
                                        </div>
                                    </div>
                                </div>
                                <!--box-widget-item end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--section end-->
        <div class="limit-box fl-wrap"></div>
    </div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRJQqA2pOFX-shd3sSVlMQb9vXdAZQsjo&libraries=places&callback=initAutocomplete"></script>
    <script src="{{ asset('js/frontOffice/map-single.js')}}"></script>

@endsection

@section('footer')
    @include('frontOffice.inc.footer')
@endsection
