@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - J\'AIDE',
    'description' => 'J\'AIDE - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header',
        [
            'actual' => 'volunteers',
            'cat' => 'self-volunteer'
        ])
@endsection

@section('content')
    <link href="{{ asset('css/frontOffice/dashboard-style.css') }}" rel="stylesheet">

    <div class="content">
        <!--  section  -->
        <section class="parallax-section dashboard-header-sec gradient-bg" data-scrollax-parent="true">
            <div class="container">
                <div class="dashboard-breadcrumbs breadcrumbs"><a href="{{ route('showHome') }}">Accueil</a><a href="{{ route('showVolunteers') }}">Bénévoles</a><span>J'AIDE</span></div>
                <!--Tariff Plan menu-->
                <div class="tfp-btn"><a href="{{ route('showContact') }}"> <strong>Besoin d'Aide ?</strong></a></div>
                <!--Tariff Plan menu end-->
                <div class="dashboard-header_conatiner fl-wrap dashboard-header_title">
                    <h1>Salut <span>{{ Auth::user()->name }}</span></h1>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="dashboard-header fl-wrap">
                <div class="container">
                    <div class="dashboard-header_conatiner fl-wrap">
                        <div class="dashboard-header-avatar">
                            <img @if(Auth::user()->picture) src="{{ asset(Auth::user()->picture) }}" @else src="{{ asset('img/unknown.png') }}" @endif alt="">
                            <a href="{{ route('showUserProfile') }}" class="color-bg edit-prof_btn"><i class="fal fa-edit"></i></a>
                        </div>
                        <div class="dashboard-header-stats-wrap" style="width:100%">
                            <div class="dashboard-header-stats">
                                <div class="" style="display: flex;">
                                    <div class="swiper-slide swiper-slide-active" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-hand-peace"></i>
                                            Volontaires
                                            <span>{{ $volunteers }}</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-active" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-hospital"></i>
                                            Hôpitaux
                                            <span>{{ $hospitals }}</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-active" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-hospital-symbol"></i>
                                            Services
                                            <span>{{ $services }}</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-active" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-bed"></i>
                                            Lits Disponibles
                                            <span>{{ $beds }}</span>
                                        </div>
                                    </div>
                            </div>
                            <!--  dashboard-header-stats  end -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
            <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
            <div class="circle-wrap" style="left: 120px; bottom: 120px; transform: translateZ(0px) translateY(35.0877px);" data-scrollax="properties: { translateY: '-200px' }">
                <div class="circle_bg-bal circle_bg-bal_small"></div>
            </div>
            <div class="circle-wrap" style="right: 420px; bottom: -70px; transform: translateZ(0px) translateY(-26.3158px);" data-scrollax="properties: { translateY: '150px' }">
                <div class="circle_bg-bal circle_bg-bal_big"></div>
            </div>
            <div class="circle-wrap" style="left: 420px; top: -70px; transform: translateZ(0px) translateY(-17.5439px);" data-scrollax="properties: { translateY: '100px' }">
                <div class="circle_bg-bal circle_bg-bal_big"></div>
            </div>
            <div class="circle-wrap" style="left:40%;bottom:-70px;">
                <div class="circle_bg-bal circle_bg-bal_middle"></div>
            </div>
            <div class="circle-wrap" style="right:40%;top:-10px;">
                <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }" style="transform: translateZ(0px) translateY(61.4035px);"></div>
            </div>
            <div class="circle-wrap" style="right:55%;top:90px;">
                <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }" style="transform: translateZ(0px) translateY(61.4035px);"></div>
            </div>
            </div>
        </section>
        <!--  section  end-->
        <!--  section  -->
        <section class="gray-bg main-dashboard-sec" id="sec1">
            <div class="container">
                <!--  dashboard-menu-->
                <div class="col-md-3">
                    <div class="mob-nav-content-btn color2-bg init-dsmen fl-wrap"><i class="fal fa-bars"></i> Menu</div>
                    <div class="clearfix"></div>
                    <div class="fixed-bar fl-wrap" id="dash_menu">
                        <div class="user-profile-menu-wrap fl-wrap block_box">
                            <!-- user-profile-menu-->
                            <div class="user-profile-menu">
                                <h3>Mon Menu</h3>
                                <ul class="no-list-style">
                                    <li><a href="{{ route('showUserProfile') }}"><i class="fal fa-user"></i>Mon Profil</a></li>
                                    <li><a class="user-profile-act" href="{{ route('showVolunteerUpdateProfile') }}"><i class="fal fa-user-nurse"></i>Profil Bénévole</a></li>
                                </ul>
                            </div>
                            <a href="{{ route('handleLogout') }}" class="logout_btn color2-bg">Déconnexion <i class="fas fa-sign-out"></i></a>
                        </div>
                    </div>
                    <a class="back-tofilters color2-bg custom-scroll-link fl-wrap" href="#dash_menu" style="z-index: 12;">Retour au Menu<i class="fas fa-caret-up"></i></a><div></div>
                    <div class="clearfix"></div>
                </div>
                <!-- dashboard-menu  end-->
                <!-- dashboard content-->
                <div class="col-md-9">
                    <form method="post" action="{{ route('handleUpdateVolunteerProfile') }}">
                        {{ csrf_field() }}
                    <div class="dashboard-title   fl-wrap">
                        <h3>Informations Bénévole</h3>
                    </div>
                    <!-- profile-edit-container-->
                    <div class="profile-edit-container fl-wrap block_box">
                        <div class="custom-form">
                            <div class="row">
                                <div class="col-md-8">
                                    <label>Organisation</label>
                                    <div class="listsearch-input-item">
                                        <select data-placeholder="Organisation" name="organisation" class="nice-select chosen-select no-search-select">
                                            <option value="-1">Aucune</option>
                                            <option disabled>--------------</option>
                                        @foreach($organisations as $organisation)
                                            <option @if(Auth::user()->volunteer and Auth::user()->volunteer->organisation_id == $organisation->id) selected @endif value="{{ $organisation->id }}">{{ $organisation->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Statut</label>
                                    <div class="act-widget fl-wrap">
                                        <div class="act-widget-header" style="padding: 4px 20px;">
                                            <h4>Je suis Disponible</h4>
                                            <div class="onoffswitch">
                                                <input value="3" type="checkbox" name="status" class="onoffswitch-checkbox" id="myonoffswitch3" @if(Auth::user()->volunteer and Auth::user()->volunteer->status == 1) checked @endif>
                                                <label class="onoffswitch-label" for="myonoffswitch3">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Sexe</label>
                                    <div class="listsearch-input-item">
                                        <select data-placeholder="Sexe" name="gender" class="nice-select chosen-select no-search-select">
                                            <option @if(Auth::user()->volunteer and Auth::user()->volunteer->gender == 1) selected @endif value="1">Homme</option>
                                            <option @if(Auth::user()->volunteer and Auth::user()->volunteer->gender == 2) selected @endif value="2">Femme</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Age <i class="fal fa-user"></i></label>
                                    <input name="age" type="number" value="{{ Auth::user()->volunteer ? Auth::user()->volunteer->age : '' }}" min="16">
                                </div>
                                <div class="col-md-4">
                                    <label>Téléphone <i class="fal fa-phone"></i></label>
                                    <input type="number" name="phone" placeholder="" value="{{ Auth::user()->phone }}" min="0">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- profile-edit-container end-->
                    <div class="dashboard-title  dt-inbox fl-wrap">
                        <h3>Catégories</h3>
                    </div>
                    <!-- profile-edit-container-->
                    <div class="profile-edit-container fl-wrap block_box">
                        <div class="custom-form">
                            <!-- Checkboxes -->
                            <ul class="fl-wrap filter-tags no-list-style ds-tg">
                                @foreach($categories as $category)
                                <li>
                                    <input @if(Auth::user()->volunteer and Auth::user()->volunteer->categories->contains($category->id)) checked @endif id="check-aaa{{ $loop->iteration }}" type="checkbox" name="categories[]" value="{{ $category->id }}" autocomplete="off">
                                    <label for="check-aaa{{ $loop->iteration }}">{{ $category->title }}</label>
                                </li>
                                @endforeach
                            </ul>
                            <!-- Checkboxes end -->
                        </div>
                    </div>
                    <!-- profile-edit-container end-->
                    <div class="dashboard-title  dt-inbox fl-wrap">
                        <h3>Disponibilité</h3>
                    </div>
                    <!-- profile-edit-container-->
                    <div class="profile-edit-container fl-wrap block_box">
                        <div class="custom-form">
                            <div class="row">
                                <div class="col-md-3">
                                    <!-- act-widget-->
                                    <div class="act-widget fl-wrap">
                                        <div class="act-widget-header">
                                            <h4>Lundi</h4>
                                            <div class="onoffswitch">
                                                <input value="1" type="checkbox" name="days[]" class="onoffswitch-checkbox" id="myonoffswitch1" @if(Auth::user()->volunteer and Auth::user()->volunteer->days != null and json_decode(Auth::user()->volunteer->days)->d1 == 1) checked @endif>
                                                <label class="onoffswitch-label" for="myonoffswitch1">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- act-widget end-->
                                </div>
                                <div class="col-md-3">
                                    <!-- act-widget-->
                                    <div class="act-widget fl-wrap">
                                        <div class="act-widget-header">
                                            <h4>Mardi</h4>
                                            <div class="onoffswitch">
                                                <input value="2" type="checkbox" name="days[]" class="onoffswitch-checkbox" id="myonoffswitch2" @if(Auth::user()->volunteer and Auth::user()->volunteer->days != null and json_decode(Auth::user()->volunteer->days)->d2 == 1) checked @endif>
                                                <label class="onoffswitch-label" for="myonoffswitch2">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- act-widget end-->
                                </div>
                                <div class="col-md-3">
                                    <!-- act-widget-->
                                    <div class="act-widget fl-wrap">
                                        <div class="act-widget-header">
                                            <h4>Mercredi</h4>
                                            <div class="onoffswitch">
                                                <input value="3" type="checkbox" name="days[]" class="onoffswitch-checkbox" id="myonoffswitch3" @if(Auth::user()->volunteer and Auth::user()->volunteer->days != null and json_decode(Auth::user()->volunteer->days)->d3 == 1) checked @endif>
                                                <label class="onoffswitch-label" for="myonoffswitch3">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- act-widget end-->
                                </div>
                                <div class="col-md-3">
                                    <!-- act-widget-->
                                    <div class="act-widget fl-wrap">
                                        <div class="act-widget-header">
                                            <h4>Jeudi</h4>
                                            <div class="onoffswitch">
                                                <input value="4" type="checkbox" name="days[]" class="onoffswitch-checkbox" id="myonoffswitch4" @if(Auth::user()->volunteer and Auth::user()->volunteer->days != null and json_decode(Auth::user()->volunteer->days)->d4 == 1) checked @endif>
                                                <label class="onoffswitch-label" for="myonoffswitch4">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- act-widget end-->
                                </div>
                                <div class="col-md-4">
                                    <!-- act-widget-->
                                    <div class="act-widget fl-wrap">
                                        <div class="act-widget-header">
                                            <h4>Vendredi</h4>
                                            <div class="onoffswitch">
                                                <input value="5" type="checkbox" name="days[]" class="onoffswitch-checkbox" id="myonoffswitch5" @if(Auth::user()->volunteer and Auth::user()->volunteer->days != null and json_decode(Auth::user()->volunteer->days)->d5 == 1) checked @endif>
                                                <label class="onoffswitch-label" for="myonoffswitch5">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- act-widget end-->
                                </div>
                                <div class="col-md-4">
                                    <!-- act-widget-->
                                    <div class="act-widget fl-wrap">
                                        <div class="act-widget-header">
                                            <h4>Samedi</h4>
                                            <div class="onoffswitch">
                                                <input value="6" type="checkbox" name="days[]" class="onoffswitch-checkbox" id="myonoffswitch6" @if(Auth::user()->volunteer and Auth::user()->volunteer->days != null and json_decode(Auth::user()->volunteer->days)->d6 == 1) checked @endif>
                                                <label class="onoffswitch-label" for="myonoffswitch6">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- act-widget end-->
                                </div>
                                <div class="col-md-4">
                                    <!-- act-widget-->
                                    <div class="act-widget fl-wrap">
                                        <div class="act-widget-header">
                                            <h4>Dimanche</h4>
                                            <div class="onoffswitch">
                                                <input value="7" type="checkbox" name="days[]" class="onoffswitch-checkbox" id="myonoffswitch7" @if(Auth::user()->volunteer and Auth::user()->volunteer->days != null and json_decode(Auth::user()->volunteer->days)->d7 == 1) checked @endif>
                                                <label class="onoffswitch-label" for="myonoffswitch7">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- act-widget end-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- profile-edit-container end-->
                    <div class="dashboard-title  dt-inbox fl-wrap">
                        <h3>Localisation</h3>
                    </div>
                    <!-- profile-edit-container-->
                    <div class="profile-edit-container fl-wrap block_box">
                        <div class="custom-form">
                            <input type="hidden" name="lat" autocomplete="off" @if(Auth::user()->volunteer) value="{{ Auth::user()->lat }}" @endif>
                            <input type="hidden" name="lng" autocomplete="off" @if(Auth::user()->volunteer) value="{{ Auth::user()->lng }}" @endif>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Gouvernorat</label>
                                    <div class="listsearch-input-item">
                                        <select data-placeholder="City" class="nice-select chosen-select no-search-select" name="city_id">
                                            @foreach($cities as $city)
                                                <option @if(Auth::user()->volunteer and Auth::user()->volunteer->city_id == $city->id) selected @endif value="{{ $city->id }}">{{ $city->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div id="map" style="height: 400px; width: 100%;">
                            </div>


                        </div>
                    </div>
                    <!-- profile-edit-container end-->
                    <button type="submit" class="btn color2-bg" style="float: right;border-color: transparent;">Enregistrer<i class="fal fa-save"></i></button>
                    </form>
                </div>
                <!-- dashboard content end-->
            </div>
        </section>
        <!--  section  end-->
        <div class="limit-box fl-wrap"></div>
    </div>


    <script>
        let map;
        let marker;

        var icon = {
            url: '{{ asset('img/pin.png') }}',
            scaledSize: new google.maps.Size(30, 30), // scaled size
            origin: new google.maps.Point(0,0), // origin
            anchor: new google.maps.Point(0, 0) // anchor
        };

        function placeMarker(location) {
            if ( marker ) {
                marker.setPosition(location);
            } else {
                marker = new google.maps.Marker({
                    position: location,
                    draggable: true,
                    icon: icon,
                    map: map
                });
            }
        }

        function initialize() {

            var center =
                {lat: @if(Auth::user()->volunteer and Auth::user()->volunteer->lat != null) {{ Auth::user()->volunteer->lat }} @else 34.2651487 @endif
                , lng: @if(Auth::user()->volunteer and Auth::user()->volunteer->lng != null) {{ Auth::user()->volunteer->lng }} @else 9.4225926 @endif};

            map = new google.maps.Map(
                document.getElementById('map'),
                {zoom: 12, center: center, styles: [
                        {
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#212121"
                                }
                            ]
                        },
                        {
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "color": "#212121"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.country",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#9e9e9e"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.land_parcel",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.locality",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#bdbdbd"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.medical",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#ff122f"
                                },
                                {
                                    "lightness": 100
                                },
                                {
                                    "weight": 8
                                }
                            ]
                        },
                        {
                            "featureType": "poi.medical",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#ff122f"
                                },
                                {
                                    "lightness": 100
                                },
                                {
                                    "weight": 8
                                }
                            ]
                        },
                        {
                            "featureType": "poi.medical",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "color": "#ff122f"
                                },
                                {
                                    "lightness": 100
                                }
                            ]
                        },
                        {
                            "featureType": "poi.medical",
                            "elementType": "labels.text",
                            "stylers": [
                                {
                                    "color": "#ff122f"
                                },
                                {
                                    "lightness": 100
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#181818"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#616161"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "color": "#1b1b1b"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#2c2c2c"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#8a8a8a"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#373737"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#3c3c3c"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway.controlled_access",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#4e4e4e"
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#616161"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#000000"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#3d3d3d"
                                }
                            ]
                        }
                    ]});

            google.maps.event.addListener(map, 'click', function(event) {
                placeMarker(event.latLng);
                document.getElementsByName('lat')[0].value = event.latLng.lat();
                document.getElementsByName('lng')[0].value = event.latLng.lng();
            });

            @if(!Auth::user()->volunteer or (Auth::user()->volunteer->lat != null and Auth::user()->volunteer->lng != null))
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    placeMarker(pos);
                    document.getElementsByName('lat')[0].value = position.coords.latitude;
                    document.getElementsByName('lng')[0].value = position.coords.longitude;
                    map.setCenter(pos);
                }, function() {
                    handleLocationError(true, '', map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, '', map.getCenter());
            }
            @endif

        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }

    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRJQqA2pOFX-shd3sSVlMQb9vXdAZQsjo&callback=initialize">
    </script>

@endsection

@section('footer')
    @include('frontOffice.inc.footer')
@endsection

