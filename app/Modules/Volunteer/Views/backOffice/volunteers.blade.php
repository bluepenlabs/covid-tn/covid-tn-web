@extends('backOffice.layout')

@section('head')
    @include('backOffice.inc.head',
    ['title' => config('app.name') . ' - Gestion des Volontaires',
    'description' => 'Gestion des Volontaires - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('backOffice.inc.header')
@endsection

@section('sidebar')
    @include('backOffice.inc.sidebar', [
        'current' => 'volunteers'
    ])
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable.js') }}"></script>

    <script type="text/javascript">

        /* Datatables responsive */

        $(document).ready(function () {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "Rechercher...");
        });

    </script>

    <div class="breadcrumb">
        <h1>Volontaires</h1>
        <ul>
            <li><a href="{{ route('showManagerHome') }}">Tableau de Bord</a></li>
            <li>Synthèse</li>
        </ul>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row mb-4">

        <div class="col-md-8 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <div class="card-title mb-3">Synthèse des Volontaires</div>

                    <table id="datatable-responsive"
                           class="display table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Téléphone</th>
                            <th>Catégories</th>
                            <th>Statut</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Téléphone</th>
                            <th width="350px">Catégories</th>
                            <th>Statut</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach($volunteers as $volunteer)
                            <tr>
                                <td>{{ $volunteer->user->name }} <sup>{{ $volunteer->organisation }}</sup></td>
                                <td>{{ $volunteer->user->email }}</td>
                                <td>{{ $volunteer->user->phone }}</td>
                                <td>
                                    <div class="skin skin-flat row col-lg-12" style="margin-bottom:15px;">
                                        @foreach($volunteer->categories as $category)
                                            <span class="badge badge-pill badge-outline-secondary p-2 m-1">{{ $category->title }}</span>
                                        @endforeach
                                    </div>
                                </td>
                                <td>
                                    <span class="label @if($volunteer->status == 1) label-success @else label-important @endif" >@if($volunteer->status == 1) Disponible @else Indispo. @endif</span>
                                </td>
                                <td style="text-align: center">
                                    <span class="badge badge-pill  badge-outline-success p-2 m-1 " title="Modifier" data-toggle="modal" data-target="#edit-v-{{ $volunteer->id }}"><i class="nav-icon i-Edit"></i></span>
                                </td>
                            </tr>
                            <div class="modal fade" id="edit-v-{{ $volunteer->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form method="post" enctype="multipart/form-data" action="{{ route('handleManagerEditVolunteer', ['id' => $volunteer->id]) }}">
                                            {{ csrf_field() }}
                                            <div class="modal-header">
                                                <h5 class="modal-title">Modifier {{ $volunteer->user->name }}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Organisation</label>
                                                        <select class="form-control" name="organisation">
                                                            <option value="-1">Aucune</option>
                                                            @foreach($organisations as $organisation)
                                                            <option @if($organisation->id == $volunteer->organisation_id) selected @endif value="{{ $organisation->id }}">{{ $organisation->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Age</label>
                                                        <input name="age" type="number" class="form-control" value="{{ $volunteer->age }}">
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <label class="control-label">Disponibilité</label>
                                                        <select class="form-control" name="status">
                                                            <option @if($volunteer->status == 0) selected @endif value="0">Indisponible</option>
                                                            <option @if($volunteer->status == 1) selected @endif value="1">Disponible</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <label class="control-label">Sexe</label>
                                                        <select class="form-control" name="gender">
                                                            <option @if($volunteer->gender == 1) selected @endif value="1">Homme</option>
                                                            <option @if($volunteer->gender == 2) selected @endif value="2">Femme</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label class="control-label">Gouvernorat</label>
                                                        <select class="form-control" name="city_id">
                                                            @foreach($cities as $city)
                                                                <option @if($city->id == $volunteer->city_id) selected @endif value="{{ $city->id }}">{{ $city->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label class="control-label">Latitude</label>
                                                        <input name="lat" type="text" class="form-control" value="{{ $volunteer->lat }}">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label class="control-label">Longitude</label>
                                                        <input name="lng" type="text" class="form-control" value="{{ $volunteer->lng }}">
                                                    </div>

                                                    <div class="col-lg-12">
                                                        <label class="control-label">Disponibilité</label><br>

                                                        <label class="switch switch-primary mr-2 mb-3">
                                                            <span>Lundi</span>
                                                            <input name="days[]" value="1" type="checkbox" @if($volunteer->days != null and json_decode($volunteer->days)->d1 == 1) checked @endif autocomplete="off">
                                                            <span class="slider"></span>
                                                        </label>

                                                        <label class="switch switch-primary mr-2 mb-3">
                                                            <span>Mardi</span>
                                                            <input name="days[]" value="2" type="checkbox" @if($volunteer->days != null and json_decode($volunteer->days)->d2 == 1) checked @endif autocomplete="off">
                                                            <span class="slider"></span>
                                                        </label>

                                                        <label class="switch switch-primary mr-2 mb-3">
                                                            <span>Mercredi</span>
                                                            <input name="days[]" value="3" type="checkbox" @if($volunteer->days != null and json_decode($volunteer->days)->d3 == 1) checked @endif autocomplete="off">
                                                            <span class="slider"></span>
                                                        </label>

                                                        <label class="switch switch-primary mr-2 mb-3">
                                                            <span>Jeudi</span>
                                                            <input name="days[]" value="4" type="checkbox" @if($volunteer->days != null and json_decode($volunteer->days)->d4 == 1) checked @endif autocomplete="off">
                                                            <span class="slider"></span>
                                                        </label>

                                                        <label class="switch switch-primary mr-2 mb-3">
                                                            <span>Vendredi</span>
                                                            <input name="days[]" value="5" type="checkbox" @if($volunteer->days != null and json_decode($volunteer->days)->d5 == 1) checked @endif autocomplete="off">
                                                            <span class="slider"></span>
                                                        </label>

                                                        <label class="switch switch-primary mr-2 mb-3">
                                                            <span>Samedi</span>
                                                            <input name="days[]" value="6" type="checkbox" @if($volunteer->days != null and json_decode($volunteer->days)->d6 == 1) checked @endif autocomplete="off">
                                                            <span class="slider"></span>
                                                        </label>

                                                        <label class="switch switch-primary mr-2 mb-3">
                                                            <span>Dimanche</span>
                                                            <input name="days[]" value="7" type="checkbox" @if($volunteer->days != null and json_decode($volunteer->days)->d7 == 1) checked @endif autocomplete="off">
                                                            <span class="slider"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label class="control-label">Catégories</label><br>
                                                        @foreach($categories as $category)
                                                            <label class="checkbox checkbox-outline-secondary mr-1 mb-2" style="display: inline-block">
                                                                <input type="checkbox" name="categories[]" value="{{ $category->id }}">
                                                                <span>{{ $category->title }}</span>
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                                <button type="submit" class="btn btn-primary">Envoyer</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="col-md-4 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <div class="card-title mb-3">Nouveau Volontaire</div>
                    <form enctype="multipart/form-data" role="form" action="{{ route('handleManagerAddVolunteer') }}"
                          method="post">
                        {{ csrf_field() }}
                        <div class="row">

                            <div class="col-md-6 form-group mb-3">
                                <label for="firstName1">Organisation</label>
                                <select class="form-control" name="organisation">
                                    <option value="-1">Aucune</option>
                                    @foreach($organisations as $organisation)
                                        <option id="{{ $organisation->id }}">{{ $organisation->title }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="firstName1">Age</label>
                                <input class="form-control" required value="" name="age" type="number">
                            </div>

                            <div class="col-md-4 form-group mb-3">
                                <label for="firstName1">Utilisateur</label>
                                <select class="form-control" name="user">
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4 form-group mb-3">
                                <label for="firstName1">Sexe</label>
                                <select class="form-control" name="gender">
                                    <option value="1">Homme</option>
                                    <option value="2">Femme</option>
                                </select>
                            </div>

                            <div class="col-md-4 form-group mb-3">
                                <label for="firstName1">Statut</label>
                                <select class="form-control" name="status">
                                    <option value="1">Dispo.</option>
                                    <option value="2">Indispo.</option>
                                </select>
                            </div>

                            <div class="col-md-4 form-group mb-3">
                                <label for="firstName1">Gouvernorat</label>
                                <select class="form-control" name="city_id">
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label for="firstName1">Latitude</label>
                                <input class="form-control" required value="" name="lat" type="text">
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label for="firstName1">Longitude</label>
                                <input class="form-control" required value="" name="lng" type="text">
                            </div>

                            <div class="col-md-12 form-group mb-3">
                                <label for="firstName1">Disponibilité</label><br>
                                <label class="switch switch-primary mr-2 mb-3">
                                    <span>Lundi</span>
                                    <input name="days[]" value="1" type="checkbox" checked>
                                    <span class="slider"></span>
                                </label>

                                <label class="switch switch-primary mr-2 mb-3">
                                    <span>Mardi</span>
                                    <input name="days[]" value="2" type="checkbox" checked>
                                    <span class="slider"></span>
                                </label>

                                <label class="switch switch-primary mr-2 mb-3">
                                    <span>Mercredi</span>
                                    <input name="days[]" value="3" type="checkbox" checked>
                                    <span class="slider"></span>
                                </label>

                                <label class="switch switch-primary mr-2 mb-3">
                                    <span>Jeudi</span>
                                    <input name="days[]" value="4" type="checkbox" checked>
                                    <span class="slider"></span>
                                </label>

                                <label class="switch switch-primary mr-2 mb-3">
                                    <span>Vendredi</span>
                                    <input name="days[]" value="5" type="checkbox" checked>
                                    <span class="slider"></span>
                                </label>

                                <label class="switch switch-primary mr-2 mb-3">
                                    <span>Samedi</span>
                                    <input name="days[]" value="6" type="checkbox" checked>
                                    <span class="slider"></span>
                                </label>

                                <label class="switch switch-primary mr-2 mb-3">
                                    <span>Dimanche</span>
                                    <input name="days[]" value="7" type="checkbox" checked>
                                    <span class="slider"></span>
                                </label>
                            </div>

                            <div class="col-md-12 form-group mb-3">
                                <label for="firstName1">Catégories</label><br>
                                @foreach($categories as $category)
                                    <label class="checkbox checkbox-outline-secondary mr-1 mb-2" style="display: inline-block">
                                        <input type="checkbox" name="categories[]" value="{{ $category->id }}">
                                        <span>{{ $category->title }}</span>
                                        <span class="checkmark"></span>
                                    </label>
                                @endforeach
                            </div>

                            <div class="col-md-12 pull-right">
                                <button type="reset" class="btn btn-warning">Réinitialiser</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

