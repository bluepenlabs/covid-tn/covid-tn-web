@extends('backOffice.layout')

@section('head')
    @include('backOffice.inc.head',
    ['title' => config('app.name') . ' - Gestion des Catégories de Volontaires',
    'description' => 'Gestion des Catégories de Volontaires - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('backOffice.inc.header')
@endsection

@section('sidebar')
    @include('backOffice.inc.sidebar', [
        'current' => 'volunteers'
    ])
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable.js') }}"></script>

    <script type="text/javascript">

        /* Datatables responsive */

        $(document).ready(function () {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });

            $('.dataTables_filter input').attr("placeholder", "Rechercher...");
        });

    </script>

    <div class="breadcrumb">
        <h1>Volontaires</h1>
        <ul>
            <li><a href="{{ route('showManagerHome') }}">Tableau de Bord</a></li>
            <li>Synthèse des Catégories</li>
        </ul>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row mb-4">

        <div class="col-md-8 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <div class="card-title mb-3">Synthèse des Catégories</div>

                    <table id="datatable-responsive"
                           class="display table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Titre</th>
                            <th>Nombre de Volontaires</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th>Titre</th>
                            <th>Nombre de Volontaires</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->title }}</td>
                                <td>{{ $category->volunteers->count() }}</td>

                                <td style="text-align: center">
                                    <span class="badge badge-pill  badge-outline-success p-2 m-1" title="Modifier" data-toggle="modal" data-target="#edit-cat-{{ $category->id }}"><i class="nav-icon i-Edit"></i></span>
                                    <a href="{{ route('handleManagerDeleteVolunteerCategory', ['id' => $category->id]) }}"><span class="badge badge-pill  badge-outline-danger p-2 m-1"><i class="nav-icon i-File-Trash"></i></span></a>

                                </td>
                            </tr>
                            <div class="modal fade" id="edit-cat-{{ $category->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form method="post" enctype="multipart/form-data" action="{{ route('handleManagerEditVolunteerCategory', ['id' => $category->id]) }}">
                                            {{ csrf_field() }}
                                            <div class="modal-header">
                                                <h5 class="modal-title">Modifier {{ $category->title }}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="control-label">Titre</label>
                                                        <input name="title" type="text" class="form-control" value="{{ $category->title }}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                                <button type="submit" class="btn btn-primary">Envoyer</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-md-4 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <div class="card-title mb-3">Nouvelle Catégorie</div>
                    <form enctype="multipart/form-data" role="form" action="{{ route('handleManagerAddVolunteerCategory') }}"
                          method="post">
                        {{ csrf_field() }}
                        <div class="row">

                            <div class="col-md-12 form-group mb-3">
                                <label for="firstName1">Titre</label>
                                <input class="form-control" required value="" name="title" type="text">
                            </div>

                            <div class="col-md-12 pull-right">
                                <button type="reset" class="btn btn-warning">Réinitialiser</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

