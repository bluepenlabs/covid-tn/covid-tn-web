<?php

if (!function_exists('transformHospitalType')) {
    /**
     * @param integer $id
     * @return string
     */
    function transformHospitalType($id)
    {
        switch ($id) {
            case 1 :
                return 'Publique';
            case 2 :
                return 'Privé';
            case 3 :
                return 'Forces de l\'ordre';
            case 4 :
                return 'Universitaire';
            default :
                return '';
        }
    }
}


if (!function_exists('transformCategory')) {
    /**
     * @param integer $id
     * @return string
     */
    function transformCategory($id)
    {
        switch ($id) {
            case 1 :
                return 'Consommable';
            case 2 :
                return 'Matériel';
            case 3 :
                return 'R.H.';
            default :
                return '';
        }
    }
}
