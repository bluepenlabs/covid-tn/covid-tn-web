@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => $hospital->title . ' - ' . config('app.name') . ' - Sauvons la Tunisie ENSEMBLE',
    'description' => 'Sauvons la Tunisie ENSEMBLE - ' . config('app.name') . ' - ' . $hospital->title
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header',
    [
        'actual' => 'help',
        'cat' => ''
    ])
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable-custom.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable-custom.js') }}"></script>

    <script type="text/javascript">

        /* Datatables responsive */

        $(document).ready(function () {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "Rechercher...");
        });

    </script>

    <div class="content">
        <!--section-->
        <section class="gray-bg small-padding no-top-padding-sec" id="sec1">
            <div class="container">
                <div class="breadcrumbs inline-breadcrumbs fl-wrap block-breadcrumbs">
                    <a href="{{ route('showHome') }}">Accueil</a><span>{{ $hospital->title }}</span>
                    <div class="showshare brd-show-share color2-bg"><i class="fas fa-share"></i> Partager</div>
                </div>
                <div class="share-holder hid-share sing-page-share top_sing-page-share">
                    <div class="share-container  isShare">
                    </div>
                </div>
                <div class="mob-nav-content-btn  color2-bg show-list-wrap-search ntm fl-wrap"><i
                            class="fal fa-filter"></i> Filters
                </div>
                <div class="fl-wrap">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="fl-wrap lws_mobile vishidelem">
                                <div class="box-widget-item fl-wrap block_box">
                                    <div class="box-widget-item-header">
                                        <h3>Localisation </h3>
                                    </div>
                                    <div class="box-widget">
                                        <div class="map-container">
                                            <div id="singleMap" data-latitude="{{ $hospital->lat }}"
                                                 data-longitude="{{ $hospital->lng }}"
                                                 data-mapTitle="{{ $hospital->title }}"></div>
                                        </div>
                                    </div>
                                </div>
                                <!--box-widget-item end -->
                            </div>

                            @if($hospital->bank != '')
                                <div class="fl-wrap lws_mobile vishidelem">
                                    <div class="box-widget-item fl-wrap block_box">
                                        <div class="box-widget-item-header">
                                            <h3>Infos Bancaires </h3>
                                        </div>
                                        <div class="box-widget">
                                            <div class="box-widget-content bwc-nopad">
                                                <div class="list-author-widget-contacts list-item-widget-contacts bwc-padside">
                                                    <ul class="no-list-style">
                                                        <li><span><i class="fal fa-comment-dollar"></i> RIB :</span> <a
                                                                    href="#">{{ $hospital->rib }}</a>
                                                        </li>
                                                        <li><span><i class="fal fa-search-dollar"></i> IBAN :</span>
                                                            <a href="#">{{ $hospital->iban }}</a></li>
                                                        <li><span><i class="fal fa-globe"></i> BIC/Swift :</span> <a
                                                                    href="#">{{ $hospital->bic_swift  }}</a>
                                                        </li>
                                                        <li><span><i class="fal fa-piggy-bank"></i> Banque :</span> <a
                                                                    href="#">{{ $hospital->bank  }}</a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!--box-widget-item end -->
                                </div>
                                @endif
                        </div>
                        <div class="col-md-8">
                            <div class="list-single-header list-single-header-inside block_box fl-wrap">
                                <div class="list-single-header-item  fl-wrap">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <h1>{{ $hospital->title }}
                                            </h1>
                                            <div class="geodir-category-location fl-wrap">
                                                @if($hospital->adress != '')
                                                    <a href=""><i
                                                                class="fas fa-map-marker-alt"></i> {{ $hospital->adress }}
                                                    </a>
                                                @endif
                                                @if($hospital->phone)
                                                    <a href="#"> <i class="fal fa-phone"></i>{{ $hospital->phone }}</a>
                                                @endif

                                                <a href=""><i
                                                            class="fal fa-map-marker"></i> {{ $hospital->city->title }}
                                                </a></div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="fl-wrap list-single-header-column  block_box">
                                                <div class="listing-rating-count-wrap single-list-count">
                                                    <div class="review-score">{{  $hospital->needs }}</div>
                                                    <div class="listing-rating"
                                                         style="margin-top: 6px; font-weight: bold">Demandes
                                                    </div>
                                                    <br>
                                                    <div class="reviews-count">Le {{ now()->format('d-m-y') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="list-single-header_bottom fl-wrap">
                                    <a class="listing-item-category-wrap" href="">
                                        <div class="listing-item-category  green-bg"><i class="fal fa-hospital"></i></div>
                                        <span>{{ transformHospitalType($hospital->type) }}</span>
                                    </a>
                                    <div class="list-single-author">
                                        <a>

                                        </a>

                                    </div>

                                    <div class="list-single-stats">
                                        <ul class="no-list-style">
                                            <li><span class="bookmark-counter"><i class="fas fa-bed"></i> {{ $hospital->services->sum('beds') }} Lits Disponibles </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="fl-wrap block_box product-header">
                                <div class="product-header-details">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="list-single-main-item fl-wrap">
                                                <h2 class="post-opt-title" style="margin: 20px; margin-bottom: 0;"><a
                                                            >Besoins du Jour</a></h2>
                                                <span class="fw-separator"></span>
                                                <div class="clearfix"></div>
                                                <table id="datatable-responsive"
                                                       class="display table table-striped table-bordered"
                                                       cellspacing="0"
                                                       width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Service</th>
                                                        <th>Demandes</th>
                                                        <th>Géré Par</th>
                                                    </tr>
                                                    </thead>

                                                    <tfoot>
                                                    <tr>
                                                        <th>Service</th>
                                                        <th>Demandes</th>
                                                        <th>Géré Par</th>
                                                    </tr>
                                                    </tfoot>

                                                    <tbody>
                                                    @foreach($hospital->services as $service)
                                                        <tr>
                                                            <td style="vertical-align: top;width: 30%">{{ $service->service->title }}</td>
                                                            <td style="vertical-align: top;width: 40%">
                                                                <ul class="cat-item no-list-style">

                                                                    @foreach($service->products as $product)
                                                                        <li style="margin-bottom: 0"><a>{{ $product->product->title }}</a><span>
                                                                                @if($product->auto_renew == 1)
                                                                                {{ $product->dayStock(today()->format('Y-m-d'))->first() ? $product->needed - $product->dayStock(today()->format('Y-m-d'))->first()->quantity : $product->needed }}
                                                                            @else
                                                                                    {{ $product->needed - $product->stocks->sum('quantity') }}
                                                                            @endif
                                                                            </span></li>
                                                                    @endforeach
                                                                </ul>
                                                            </td>

                                                            <td style="padding-top: 18px;padding-bottom: 1px;vertical-align: top;">
                                                                @if($service->user_id)
                                                                    <span class="author_avatar">
                                                                    <img alt="" @if($service->user->photo)src="{{ asset($service->user->photo) }}" @else src="{{ asset('img/unknown.png') }}" @endif>
                                                                    </span>
                                                                    {{ $service->user->name }}
                                                                @else
                                                                    <span class="author_avatar">
                                                                                <img alt="" src="{{ asset('img/unknown.png') }}">
                                                                            </span>
                                                                    @auth
                                                                        <a href="{{ route('handleUserRequestServiceManagement', ['id' => $service->id]) }}">Je gère ce service</a>
                                                                    @else
                                                                    Non attribué
                                                                        @endauth
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--section end-->
        <div class="limit-box fl-wrap"></div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRJQqA2pOFX-shd3sSVlMQb9vXdAZQsjo&libraries=places&callback=initAutocomplete"></script>
    <script src="{{ asset('js/frontOffice/map-single.js')}}"></script>

@endsection

@section('footer')
    @include('frontOffice.inc.footer')
@endsection
