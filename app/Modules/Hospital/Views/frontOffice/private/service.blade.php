@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - Gestion de ' . $service->title,
    'description' => 'Gestion de ' . $service->title . ' - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header',
        [
            'actual' => 'medical',
            'cat' => 'self-hospital'
        ])
@endsection

@section('content')
    <link href="{{ asset('plugins/fullcalendar/packages/core/main.css') }}" rel='stylesheet'/>
    <link href="{{ asset('plugins/fullcalendar/packages/daygrid/main.css') }}" rel='stylesheet'/>

    <script src='{{ asset('plugins/fullcalendar/packages/core/main.js') }}'></script>
    <script src='{{ asset('plugins/fullcalendar/packages/daygrid/main.js') }}'></script>
    <script src='{{ asset('plugins/fullcalendar/packages/interaction/main.js') }}'></script>
    <script src='{{ asset('plugins/fullcalendar/packages/core/locales-all.js') }}'></script>
    <script src='{{ asset('plugins/fullcalendar/packages/core/locales/fr.js') }}'></script>

    <link href="{{ asset('css/frontOffice/dashboard-style.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable-custom.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable-custom.js') }}"></script>

    <script type="text/javascript">

        /* Datatables responsive */

        $(document).ready(function () {
                    @if($service->products->count() > 0)
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
                displayEventTime: false,
                locale: 'fr',
                defaultView: 'dayGridWeek',
                plugins: ['dayGrid', 'interaction'],
                dateClick: function (info) {
                    if (moment(info.dateStr).isBefore(moment())) {
                        getDayStock(info.dateStr);
                        $("#target_date").text(info.dateStr);
                        $("#input_date").val(info.dateStr);
                    } else {
                        toastr.error('Vous ne pouvez pas mettre à jour une date future !')
                        return false;
                    }

                },
                events: [
                        @foreach($service->products as $product)
                        @foreach($product->stocks as $stock)
                    {
                        id: '{{$product->id}}',
                        title: '{{$stock->quantity . ' ' . $product->product->title}}',
                        start: '{{$stock->created_at}}'
                    },
                    @endforeach
                    @endforeach
                ]
            });

            calendar.render();

            function getDayStock(date) {

                var url = "{{ route('getStockByDay', ['id'=> $service->id,'day'=> ':date']) }}";

                $.get(url.replace(':date', date)).done(function (res) {

                    $('#day_modal , .reg-overlay').fadeIn(200);
                    $("#day_main").addClass("vis_mr");
                    $("html, body").addClass("hid-body");
                    $('#stock-day').html(" ");

                    $.each(res.data, function (i, value) {
                        $('#stock-day').append(
                            '<tr>' +
                            '   <td style="display:none">' +
                            '       <input hidden name=product_id[] value="' + value.product_id + '">' +
                            '       <input hidden name=id[] value="' + value.stock_id + '">' +
                            '   </td>' +
                            '   <td>' +
                            '       <label style="color: #7d93b2;font-size: 14px">' + value.title + '</label>' +
                            '   </td>' +
                            '   <td>' +
                            '       <input class="form-control" max="'+ Number(value.needed) +'" value="' + Number(value.quantity) + '"   name="quantity[]" type="number">' +
                            '   </td>' +
                            '</tr>'
                        );
                    })


                });

            }
            @endif
            // $('#exampleModal').modal();
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "Rechercher...");
        });

    </script>
    <div class="content">
        <!--  section  -->
        <section class="parallax-section dashboard-header-sec gradient-bg" data-scrollax-parent="true">
            <div class="container">
                <div class="dashboard-breadcrumbs breadcrumbs"><a href="{{ route('showHome') }}">Accueil</a><a
                            href="{{ route('showHome') }}">Médical</a><span>{{ $service->service->title }}</span></div>
                <!--Tariff Plan menu-->
                <div class="tfp-btn"><a href="{{ route('showContact') }}"> <strong>Besoin d'Aide ?</strong></a></div>
                <!--Tariff Plan menu end-->
                <div class="dashboard-header_conatiner fl-wrap dashboard-header_title">
                    <h1><span>{{ $service->hospital->title }}</span></h1>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="dashboard-header fl-wrap">
                <div class="container">
                    <div class="dashboard-header_conatiner fl-wrap">
                        <div class="dashboard-header-avatar">
                            <img @if($service->hospital->img) src="{{ asset($service->hospital->img) }}"
                                 @else src="{{ asset('img/hospi_unk.jpg') }}" @endif alt="">
                        </div>
                        <div class="dashboard-header-stats-wrap" style="width:100%">
                            <div class="dashboard-header-stats">
                                <div class="" style="display: flex;">
                                    <div class="swiper-slide swiper-slide-active"
                                         style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-hand-peace"></i>
                                            Volontaires
                                            <span>{{ $volunteers }}</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-active"
                                         style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-hospital"></i>
                                            Hôpitaux
                                            <span>{{ $hospitals }}</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-active"
                                         style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-hospital-symbol"></i>
                                            Services
                                            <span>{{ $services }}</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-active"
                                         style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-bed"></i>
                                            Lits Disponibles
                                            <span>{{ $service->beds }}</span>
                                        </div>
                                    </div>
                                </div>
                                <!--  dashboard-header-stats  end -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
                <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
                <div class="circle-wrap"
                     style="left: 120px; bottom: 120px; transform: translateZ(0px) translateY(35.0877px);"
                     data-scrollax="properties: { translateY: '-200px' }">
                    <div class="circle_bg-bal circle_bg-bal_small"></div>
                </div>
                <div class="circle-wrap"
                     style="right: 420px; bottom: -70px; transform: translateZ(0px) translateY(-26.3158px);"
                     data-scrollax="properties: { translateY: '150px' }">
                    <div class="circle_bg-bal circle_bg-bal_big"></div>
                </div>
                <div class="circle-wrap"
                     style="left: 420px; top: -70px; transform: translateZ(0px) translateY(-17.5439px);"
                     data-scrollax="properties: { translateY: '100px' }">
                    <div class="circle_bg-bal circle_bg-bal_big"></div>
                </div>
                <div class="circle-wrap" style="left:40%;bottom:-70px;">
                    <div class="circle_bg-bal circle_bg-bal_middle"></div>
                </div>
                <div class="circle-wrap" style="right:40%;top:-10px;">
                    <div class="circle_bg-bal circle_bg-bal_versmall"
                         data-scrollax="properties: { translateY: '-350px' }"
                         style="transform: translateZ(0px) translateY(61.4035px);"></div>
                </div>
                <div class="circle-wrap" style="right:55%;top:90px;">
                    <div class="circle_bg-bal circle_bg-bal_versmall"
                         data-scrollax="properties: { translateY: '-350px' }"
                         style="transform: translateZ(0px) translateY(61.4035px);"></div>
                </div>
            </div>
        </section>
        <!--  section  end-->
        <!--  section  -->
        <section class="gray-bg main-dashboard-sec" id="sec1">
            <div class="container">
                <!--  dashboard-menu-->
                <div class="col-md-3">
                    <div class="mob-nav-content-btn color2-bg init-dsmen fl-wrap"><i class="fal fa-bars"></i> Menu</div>
                    <div class="clearfix"></div>
                    <div class="fixed-bar fl-wrap" id="dash_menu">
                        <div class="user-profile-menu-wrap fl-wrap block_box">
                            <div class="user-profile-menu">
                                <h3>Mes Services</h3>
                                <ul class="no-list-style">
                                    @foreach(Auth::user()->services as $service)
                                        <li><a class="user-profile-act"
                                               href="{{ route('showMedicalAgentHospitalService', ['id' => $service->id]) }}"><i
                                                        class="fal fa-hospital-symbol"></i>{{ $service->service->title }}
                                            </a></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="user-profile-menu">
                                <h3>Mon Hôpital</h3>
                                <ul class="no-list-style">
                                    <li>
                                        <a href="{{ route('showMedicalAgentHospital', ['id' => $service->hospital->id]) }}"><i
                                                    class="fal fa-hospital-symbol"></i>Informations Générales
                                        </a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="dashboard-title  fl-wrap" style="margin-top: 25px;margin-bottom: 15px;">
                            <h3>Lits Dispo.</h3>
                        </div>
                        <!-- profile-edit-container-->
                        <form method="post" action="{{ route('handleMedicalAgentEditServiceBeds', ['id' => $service->id]) }}" id="subscribe">
                            {{ csrf_field() }}
                            <input class="enteremail fl-wrap error" value="{{ $service->beds }}" name="beds"
                                   id="subscribe-email" type="number">
                            <button type="submit" style="padding-left: 70px;" id="subscribe-button"
                                    class="subscribe-button"><i class="fal fa-arrow-right"></i></button>
                        </form>
                    </div>
                    <a class="back-tofilters color2-bg custom-scroll-link fl-wrap" href="#dash_menu"
                       style="z-index: 12;">Retour au Menu<i class="fas fa-caret-up"></i></a>
                    <div></div>
                    <div class="clearfix"></div>
                </div>
                <!-- dashboard-menu  end-->
                <!-- dashboard content-->
                <div class="col-md-9">
                    <div class="dashboard-title fl-wrap">
                        <h3>Gestion du Stock</h3>
                    </div>
                    <div class="profile-edit-container fl-wrap block_box">
                        @if($service->products->count() > 0)
                            <div id='calendar'></div>
                        @else
                            Vous devez au moins avoir un besoin pour gérer le stock
                        @endif
                    </div>

                    <div class="row">

                        <div class="col-lg-12">
                            <div class="dashboard-title  fl-wrap">
                                <h3>Nouveau Besoin</h3>
                            </div>
                            <!-- profile-edit-container-->
                            <div class="profile-edit-container fl-wrap block_box">
                                <div class="custom-form">
                                    <form action="{{ route('handleMedicalAgentAddProduct', ['id' => $service->id ]) }}"
                                          method="post">
                                        {{ csrf_field() }}

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label>Besoin <i class="fal fa-battery-quarter"></i></label>
                                                <select
                                                        class="nice-select chosen-select no-search-select"
                                                        name="product_id">
                                                    @foreach($products as $product)
                                                        @if(!$service->products->contains('product_id', $product->id))
                                                            <option value="{{ $product->id }}">{{ $product->title }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-lg-3">
                                                <label>Nécessité<i class="fal fa-battery-quarter"></i></label>
                                                <input class="form-control" value="1" min="1" required name="quantity"
                                                       type="number">

                                            </div>

                                            <div class="col-lg-2">
                                                <label>Besoin Quotidien <i class="fal fa-battery-quarter"></i></label>
                                                <div class="onoffswitch">
                                                    <input value="1" type="checkbox" name="renew"
                                                           class="onoffswitch-checkbox" id="myonoffswitch1">
                                                    <label class="onoffswitch-label" for="myonoffswitch1">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <button type="submit" class="btn color2-bg"
                                                        style="float: right;border-color: transparent;">Ajouter<i
                                                            class="fal fa-save"></i></button>
                                            </div>
                                        </div>



                                    </form>


                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="dashboard-title fl-wrap" style="margin-top: 15px;">
                                <h3>Besoins du Service</h3>
                            </div>
                            <!-- profile-edit-container-->
                            <div class="profile-edit-container fl-wrap block_box">
                                <table id="datatable-responsive"
                                       class="display table table-striped table-bordered"
                                       cellspacing="0"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th>Besoin</th>
                                        <th>Répéter</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tfoot>
                                    <tr>
                                        <th>Besoin</th>
                                        <th>Répéter</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>

                                    <tbody>
                                    @foreach($service->products as $product)
                                        <tr>
                                            <td style="vertical-align: top;width: 40%">
                                                {{ $product->needed }} {{ $product->product->title }}
                                            </td>
                                            <td>
                                                @if($product->auto_renew == 1)
                                                    Chaque {{ $product->renew }} jours
                                                @else
                                                    Non
                                                @endif
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- dashboard content end-->
                </div>
            </div>
        </section>
        <!--  section  end-->
        <div class="limit-box fl-wrap"></div>
    </div>


    <div class="main-register-wrap modal" id="day_modal">
        <div class="reg-overlay"></div>
        <div class="main-register-holder tabs-act">
            <div class="main-register fl-wrap  modal_main" id="day_main">
                <div class="main-register_title">Stock du <span><strong id="target_date">Covid</strong></span></div>
                <div class="close-reg"><i class="fal fa-times"></i></div>
                <div class="container">
                    <form action="{{ route('handleMedicalAgentEditProductStock', ['id' => $service->id]) }}" method="POST" style="display: inline-table;">
                        {{ csrf_field() }}
                        <table class="table" style="border-spacing: 15px;border-collapse: initial;">
                            <thead>
                            <input type="text" hidden name="date" id="input_date">
                            <tr>
                                <th>Produit</th>
                                <th>Stock</th>
                            </tr>
                            </thead>
                            <tbody id="stock-day">
                            </tbody>
                        </table>
                        <button type="submit" style="margin-top: 8px" class="btn color2-bg"> Maitre a jour <i
                                    class="fas fa-caret-right"></i></button>
                    </form>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>


@endsection

@section('footer')
    @include('frontOffice.inc.footer')
@endsection
