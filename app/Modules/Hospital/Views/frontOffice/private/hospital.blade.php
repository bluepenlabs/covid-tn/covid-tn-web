@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head',
    ['title' => config('app.name') . ' - Gestion de ' . $hospital->title,
    'description' => 'Gestion de ' . $hospital->title . ' - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('frontOffice.inc.header',
        [
            'actual' => 'medical',
            'cat' => 'self-hospital'
        ])
@endsection

@section('content')
    <link href="{{ asset('css/frontOffice/dashboard-style.css') }}" rel="stylesheet">

    <div class="content">
        <!--  section  -->
        <section class="parallax-section dashboard-header-sec gradient-bg" data-scrollax-parent="true">
            <div class="container">
                <div class="dashboard-breadcrumbs breadcrumbs"><a href="{{ route('showHome') }}">Accueil</a><a href="{{ route('showHome') }}">Médical</a><span>{{ $hospital->title }}</span></div>
                <!--Tariff Plan menu-->
                <div class="tfp-btn"><a href="{{ route('showContact') }}"> <strong>Besoin d'Aide ?</strong></a></div>
                <!--Tariff Plan menu end-->
                <div class="dashboard-header_conatiner fl-wrap dashboard-header_title">
                    <h1><span>{{ $hospital->title }}</span></h1>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="dashboard-header fl-wrap">
                <div class="container">
                    <div class="dashboard-header_conatiner fl-wrap">
                        <div class="dashboard-header-avatar">
                            <img @if($hospital->img) src="{{ asset($hospital->img) }}" @else src="{{ asset('img/hospi_unk.jpg') }}" @endif alt="">
                        </div>
                        <div class="dashboard-header-stats-wrap" style="width:100%">
                            <div class="dashboard-header-stats">
                                <div class="" style="display: flex;">
                                    <div class="swiper-slide swiper-slide-active" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-hand-peace"></i>
                                            Volontaires
                                            <span>{{ $volunteers }}</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-active" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-hospital"></i>
                                            Hôpitaux
                                            <span>{{ $hospitals }}</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-active" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-hospital-symbol"></i>
                                            Services
                                            <span>{{ $services }}</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-active" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-bed"></i>
                                            Lits Disponibles
                                            <span>{{ $hospital->services->sum('beds') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <!--  dashboard-header-stats  end -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
                <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
                <div class="circle-wrap" style="left: 120px; bottom: 120px; transform: translateZ(0px) translateY(35.0877px);" data-scrollax="properties: { translateY: '-200px' }">
                    <div class="circle_bg-bal circle_bg-bal_small"></div>
                </div>
                <div class="circle-wrap" style="right: 420px; bottom: -70px; transform: translateZ(0px) translateY(-26.3158px);" data-scrollax="properties: { translateY: '150px' }">
                    <div class="circle_bg-bal circle_bg-bal_big"></div>
                </div>
                <div class="circle-wrap" style="left: 420px; top: -70px; transform: translateZ(0px) translateY(-17.5439px);" data-scrollax="properties: { translateY: '100px' }">
                    <div class="circle_bg-bal circle_bg-bal_big"></div>
                </div>
                <div class="circle-wrap" style="left:40%;bottom:-70px;">
                    <div class="circle_bg-bal circle_bg-bal_middle"></div>
                </div>
                <div class="circle-wrap" style="right:40%;top:-10px;">
                    <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }" style="transform: translateZ(0px) translateY(61.4035px);"></div>
                </div>
                <div class="circle-wrap" style="right:55%;top:90px;">
                    <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }" style="transform: translateZ(0px) translateY(61.4035px);"></div>
                </div>
            </div>
        </section>
        <!--  section  end-->
        <!--  section  -->
        <section class="gray-bg main-dashboard-sec" id="sec1">
            <div class="container">
                <!--  dashboard-menu-->
                <div class="col-md-3">
                    <div class="mob-nav-content-btn color2-bg init-dsmen fl-wrap"><i class="fal fa-bars"></i> Menu</div>
                    <div class="clearfix"></div>
                    <div class="fixed-bar fl-wrap" id="dash_menu">
                        <div class="user-profile-menu-wrap fl-wrap block_box">
                            <div class="user-profile-menu">
                                <h3>Mes Services</h3>
                                <ul class="no-list-style">
                                    @foreach(Auth::user()->services as $service)
                                    <li><a href="{{ route('showMedicalAgentHospitalService', ['id' => $service->id]) }}"><i class="fal fa-hospital-symbol"></i>{{ $service->service->title }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="user-profile-menu">
                                <h3>Mon Hôpital</h3>
                                <ul class="no-list-style">
                                    <li><a class="user-profile-act" href="{{ route('showMedicalAgentHospital', ['id' => $hospital->id]) }}"><i class="fal fa-hospital-symbol"></i>Informations Générales</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <a class="back-tofilters color2-bg custom-scroll-link fl-wrap" href="#dash_menu" style="z-index: 12;">Retour au Menu<i class="fas fa-caret-up"></i></a><div></div>
                    <div class="clearfix"></div>
                </div>
                <!-- dashboard-menu  end-->
                <!-- dashboard content-->
                <div class="col-md-9">
                    <form method="post" action="{{ route('handleMedicalAgentEditHospital', ['id' => $hospital->id]) }}">
                        {{ csrf_field() }}
                        <div class="dashboard-title   fl-wrap">
                            <h3>Mon Hôpital</h3>
                        </div>
                        <!-- profile-edit-container-->
                        <div class="profile-edit-container fl-wrap block_box">
                            <div class="custom-form">
                                <label>Titre <i class="fal fa-briefcase"></i></label>
                                <input type="text" value="{{ $hospital->title }}" disabled>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Type <i class="fal fa-search"></i></label>
                                        <select class="nice-select chosen-select no-search-select" name="type">
                                            <option @if($hospital->type == 1) selected @endif value="1">Publique</option>
                                            <option @if($hospital->type == 2) selected @endif value="2">Privé</option>
                                            <option @if($hospital->type == 3) selected @endif value="3">Forces de l'ordre</option>
                                            <option @if($hospital->type == 4) selected @endif value="4">Universitaire</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Téléphone <i class="fal fa-photo-video"></i></label>
                                        <input type="number" name="phone" value="{{ $hospital->phone }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Gouvernorat <i class="fal fa-map"></i></label>
                                        <select  class="nice-select chosen-select no-search-select" name="city">
                                            @foreach($cities as $city)
                                                <option @if($city->id == $hospital->city_id) selected @endif value="{{ $city->id }}">{{ $city->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <label>Adresse <i class="fal fa-address-book"></i></label>
                                <input type="text" name="address" value="{{ $hospital->address }}">

                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Banque <i class="fal fa-piggy-bank"></i></label>
                                        <input type="text" disabled value="{{ $hospital->bank }}">
                                    </div>
                                    <div class="col-md-6">
                                        <label>RIB <i class="fal fa-comment-dollar"></i></label>
                                        <input type="text" disabled value="{{ $hospital->rib }}">
                                    </div>
                                    <div class="col-md-6">
                                        <label>IBAN <i class="fal fa-search-dollar"></i></label>
                                        <input type="text" disabled value="{{ $hospital->iban }}">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Bic/Swift <i class="fal fa-globe"></i></label>
                                        <input type="text" disabled value="{{ $hospital->bic_swift }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- profile-edit-container end-->
                        <button type="submit" class="btn color2-bg" style="float: right;border-color: transparent;">Enregistrer<i class="fal fa-save"></i></button>
                    </form>
                </div>
                <!-- dashboard content end-->
            </div>
        </section>
        <!--  section  end-->
        <div class="limit-box fl-wrap"></div>
    </div>
@endsection

@section('footer')
    @include('frontOffice.inc.footer')
@endsection
