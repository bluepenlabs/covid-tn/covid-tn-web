@extends('backOffice.layout')

@section('head')
    @include('backOffice.inc.head',
    ['title' => config('app.name') . ' - Gestion des Produits',
    'description' => 'Gestion des Produits - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('backOffice.inc.header')
@endsection

@section('sidebar')
    @include('backOffice.inc.sidebar', [
        'current' => 'hospitals'
    ])
@endsection


@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable.js') }}"></script>

    <script type="text/javascript">

        /* Datatables responsive */

        $(document).ready(function () {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "Rechercher...");
        });

    </script>

    <div class="breadcrumb">
        <h1>Produits</h1>
        <ul>
            <li><a href="{{ route('showManagerHome') }}">Tableau de Bord</a></li>
            <li>Synthèse des Produits</li>
        </ul>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row mb-4">

        <div class="col-md-8 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <div class="card-title mb-3">Synthèse des Produits</div>
                    <table id="datatable-responsive"
                           class="display table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Titre</th>
                            <th>Type</th>
                            <th>Description</th>
                            <th>Prix</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th>Titre</th>
                            <th>Catégorie</th>
                            <th>Description</th>
                            <th>Prix</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{{ $product->title }}</td>
                                <td>{{ transformCategory($product->category) }}</td>
                                <td>{{ $product->description }}</td>
                                <td>{{ $product->min }} - {{ $product->max }}</td>
                                <td style="text-align: center">
                                    <span class="badge badge-pill badge-outline-success p-2 m-1" title="Modifier"
                                          data-toggle="modal" data-target="#edit-{{ $product->id }}"><i
                                                class="nav-icon i-Edit"></i></span>
                                    <a href="{{ route('handleManagerDeleteHospitalProduct', ['id' => $product->id]) }}"><span
                                                class="badge badge-pill badge-outline-warning p-2 m-1"><i
                                                    class="nav-icon i-File-Trash"></i></span></a>
                                </td>
                            </tr>
                            <div class="modal fade" id="edit-{{ $product->id }}" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form method="post"
                                              action="{{ route('handleManagerEditHospitalProduct', ['id' => $product->id]) }}">
                                            {{ csrf_field() }}
                                            <div class="modal-header">
                                                <h5 class="modal-title">Modifier {{ $product->title }}</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">×
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="control-label">Titre</label>
                                                        <input name="title" required class="form-control"
                                                               placeholder="Titre" value="{{ $product->title }}">
                                                    </div>

                                                    <div class="col-lg-12">
                                                        <label class="control-label">Description</label>
                                                        <input name="description" class="form-control" placeholder=""
                                                               value="{{ $product->description }}">
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label class="control-label">Catégorie</label>
                                                        <select class="form-control" name="category">
                                                            <option @if($product->category == 1) selected
                                                                    @endif value="1">Consommable
                                                            </option>
                                                            <option @if($product->category == 2) selected
                                                                    @endif value="2">Matériel
                                                            </option>
                                                            <option @if($product->category == 3) selected
                                                                    @endif value="3">Ressource Humaine
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Minimum</label>
                                                        <input name="min" class="form-control" placeholder=""
                                                               value="{{ $product->min }}">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Maximum</label>
                                                        <input name="max" class="form-control" placeholder=""
                                                               value="{{ $product->max }}">
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Annuler
                                                </button>
                                                <button type="submit" class="btn btn-primary">Envoyer</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-4">
            <div class="row">
                <div class="col-lg-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <div class="card-title mb-3">Affectation de Besoin</div>
                            <form action="{{ route('handleManagerAffectProductService') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12 form-group mb-3">
                                        <label for="firstName1">Service</label>
                                        <select name="service_id" class="form-control">
                                            @foreach($hospitals as $hospital)
                                                <optgroup label="{{ $hospital->title }}">
                                                    @foreach($hospital->services as $service)
                                                        <option value="{{ $service->service->id }}">{{ $service->service->title }}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12 form-group mb-3">
                                        <label for="firstName1">Besoin</label>
                                        <select name="product_id" class="form-control">
                                            @foreach($products as $product)
                                                <option value="{{ $product->id }}">{{ $product->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12 form-group mb-3">
                                        <label for="firstName1">Quantité</label>
                                        <input class="form-control" required value="" name="needed" type="number">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName1">Renouvellement</label>

                                        <label class="switch switch-primary mr-2 mb-3">
                                            <span>Quotidien</span>
                                            <input name="auto_renew" value="1" type="checkbox" autocomplete="off">
                                            <span class="slider"></span>
                                        </label>
                                    </div>

                                    <div class="col-md-12 pull-right">
                                        <button type="reset" class="btn btn-warning">Réinitialiser</button>
                                        <button type="submit" class="btn btn-primary">Ajouter</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="card text-left">

                        <div class="card-body">
                            <div class="card-title mb-3">Nouveau Besoin</div>
                            <form action="{{ route('handleManagerAddHospitalProduct') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12 form-group mb-3">
                                        <label for="firstName1">Titre</label>
                                        <input class="form-control" required value="" name="title" type="text">
                                    </div>

                                    <div class="col-md-12 form-group mb-3">
                                        <label for="firstName1">Catégorie</label>
                                        <select class="form-control" name="category">
                                            <option value="1">Consommable</option>
                                            <option value="2">Matériel</option>
                                            <option value="3">Ressource Humaine</option>
                                        </select>
                                    </div>

                                    <div class="col-md-12 form-group mb-3">
                                        <label for="firstName1">Description</label>
                                        <input class="form-control" required value="" name="description" type="text">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName1">Minimum</label>
                                        <input class="form-control" required value="0" name="min" type="number">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName1">Maximum</label>
                                        <input class="form-control" required value="0" name="max" type="number">
                                    </div>

                                    <div class="col-md-12 pull-right">
                                        <button type="reset" class="btn btn-warning">Réinitialiser</button>
                                        <button type="submit" class="btn btn-primary">Ajouter</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
