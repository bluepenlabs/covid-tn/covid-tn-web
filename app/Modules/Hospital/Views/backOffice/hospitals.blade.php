@extends('backOffice.layout')

@section('head')
    @include('backOffice.inc.head',
    ['title' => config('app.name') . ' - Gestion des Hôpitaux',
    'description' => 'Gestion des Hôpitaux - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('backOffice.inc.header')
@endsection

@section('sidebar')
    @include('backOffice.inc.sidebar', [
        'current' => 'hospitals'
    ])
@endsection


@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable.js') }}"></script>

    <script type="text/javascript">

        /* Datatables responsive */

        $(document).ready(function () {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "Rechercher...");
        });

    </script>

    <div class="breadcrumb">
        <h1>Hôpitaux</h1>
        <ul>
            <li><a href="{{ route('showManagerHome') }}">Tableau de Bord</a></li>
            <li>Synthèse des Hôpitaux</li>
        </ul>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row mb-4">

        <div class="col-md-8 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <div class="card-title mb-3">Synthèse des Hôpitaux</div>
                    <table id="datatable-responsive"
                           class="display table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Titre</th>
                            <th>Type</th>
                            <th>Gouvernorat</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th>Titre</th>
                            <th>Type</th>
                            <th>Gouvernorat</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach($hospitals as $hospital)
                            <tr>
                                <td>{{ $hospital->title }}</td>
                                <td>{{ transformHospitalType($hospital->type) }}</td>
                                <td>{{ $hospital->city->title }}</td>
                                <td style="text-align: center">
                                    <span class="badge badge-pill badge-outline-success p-2 m-1" title="Modifier" data-toggle="modal" data-target="#edit-{{ $hospital->id }}"><i class="nav-icon i-Edit"></i></span>
                                    <a href="{{ route('handleManagerDeleteHospital', ['id' => $hospital->id]) }}"><span class="badge badge-pill badge-outline-warning p-2 m-1"><i class="nav-icon i-File-Trash"></i></span></a>
                                </td>
                            </tr>
                            <div class="modal fade" id="edit-{{ $hospital->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form method="post" action="{{ route('handleManagerEditHospital', ['id' => $hospital->id]) }}">
                                            {{ csrf_field() }}
                                            <div class="modal-header">
                                                <h5 class="modal-title">Modifier {{ $hospital->title }}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Titre</label>
                                                        <input name="title" required class="form-control" placeholder="Titre" value="{{ $hospital->title }}">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Type</label>
                                                        <select class="form-control" name="type" autocomplete="off">
                                                            <option @if($hospital->type == 1) selected @endif value="1">Publique</option>
                                                            <option @if($hospital->type == 2) selected @endif value="2">Privé</option>
                                                            <option @if($hospital->type == 3) selected @endif value="3">Forces de l'ordre</option>
                                                            <option @if($hospital->type == 4) selected @endif value="4">Universitaire</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Gouvernorat</label>
                                                        <select class="form-control" name="city" autocomplete="off">
                                                            @foreach($cities as $city)
                                                                <option @if($hospital->city_id == $city->id) selected @endif value="{{ $city->id }}">{{ $city->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <label class="control-label">Adresse</label>
                                                        <input name="address" class="form-control" placeholder="" value="{{ $hospital->address }}">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Latitude</label>
                                                        <input name="lat" required class="form-control" placeholder="" value="{{ $hospital->lat }}">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Longitude</label>
                                                        <input name="lng" required class="form-control" placeholder="" value="{{ $hospital->lng }}">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">Téléphone</label>
                                                        <input name="phone" class="form-control" placeholder="" value="{{ $hospital->phone }}" type="number">
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <label class="control-label">IBAN</label>
                                                        <input name="iban" class="form-control" placeholder="" value="{{ $hospital->iban }}" >
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="control-label">RIB</label>
                                                        <input name="rib" class="form-control" type="number" value="{{ $hospital->rib }}">
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label class="control-label">Banque</label>
                                                        <input name="bank" class="form-control" placeholder="" value="{{ $hospital->bank }}">
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label class="control-label">Bic/Swift</label>
                                                        <input name="bs" class="form-control" placeholder="" value="{{ $hospital->bs }}">
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                                <button type="submit" class="btn btn-primary">Envoyer</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <div class="card-title mb-3">Nouvel Hôpital</div>
                    <form action="{{ route('handleManagerAddHospital') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                        <div class="col-md-12 form-group mb-3">
                            <label for="firstName1">Titre</label>
                            <input class="form-control" required value="" name="title" type="text">
                        </div>

                        <div class="col-md-6 form-group mb-3">
                            <label for="firstName1">Type</label>
                            <select class="form-control" name="type">
                                <option value="1">Publique</option>
                                <option value="2">Privé</option>
                                <option value="3">Forces de l'ordre</option>
                                <option value="4">Universitaire</option>
                            </select>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                            <label for="firstName1">Gouvernorat</label>
                            <select class="form-control" name="city">
                                @foreach($cities as $city)
                                    <option value="{{ $city->id }}">{{ $city->title }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-12 form-group mb-3">
                            <label for="firstName1">Adresse</label>
                            <input class="form-control" required value="" name="address" type="text">
                        </div>

                        <div class="col-md-6 form-group mb-3">
                            <label for="firstName1">Latitude</label>
                            <input class="form-control" required value="" name="lat" type="text">
                        </div>

                        <div class="col-md-6 form-group mb-3">
                            <label for="firstName1">Longitude</label>
                            <input class="form-control" required value="" name="lng" type="text">
                        </div>

                        <div class="col-md-12 form-group mb-3">
                            <label for="firstName1">Téléphone</label>
                            <input class="form-control" required value="" name="phone" type="number">
                        </div>

                        <div class="col-md-12 form-group mb-3">
                            <label for="firstName1">IBAN</label>
                            <input class="form-control" value="" name="iban" type="text">
                        </div>

                        <div class="col-md-12 form-group mb-3">
                            <label for="firstName1">RIB</label>
                            <input class="form-control" value="" name="rib" type="number">
                        </div>

                        <div class="col-md-6 form-group mb-3">
                            <label for="firstName1">Banque</label>
                            <input class="form-control" value="" name="bank" type="text">
                        </div>

                        <div class="col-md-6 form-group mb-3">
                            <label for="firstName1">BIC/Swift</label>
                            <input class="form-control" value="" name="bs" type="text">
                        </div>

                        <div class="col-md-12 pull-right">
                            <button type="reset" class="btn btn-warning">Réinitialiser</button>
                            <button type="submit" class="btn btn-primary">Ajouter</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection
