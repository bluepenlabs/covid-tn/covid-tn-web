@extends('backOffice.layout')

@section('head')
    @include('backOffice.inc.head',
    ['title' => config('app.name') . ' - Gestion des Services d\'Hôpitaux',
    'description' => 'Gestion des Services d\'Hôpitaux - ' . config('app.name')
    ])
@endsection

@section('header')
    @include('backOffice.inc.header')
@endsection

@section('sidebar')
    @include('backOffice.inc.sidebar', [
        'current' => 'hospitals'
    ])
@endsection


@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable.js') }}"></script>

    <script type="text/javascript">

        /* Datatables responsive */

        $(document).ready(function () {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('#datatable-responsive-2').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/fr.json') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "Rechercher...");
        });

    </script>

    <div class="breadcrumb">
        <h1>Services d'Hôpitaux</h1>
        <ul>
            <li><a href="{{ route('showManagerHome') }}">Tableau de Bord</a></li>
            <li>Synthèse des Services d'Hôpitaux</li>
        </ul>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row mb-4">

        <div class="col-md-8 mb-4">
            <div class="row">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <div class="card-title mb-3">Nouveau Service</div>
                            <form action="{{ route('handleManagerAddHospitalService') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12 form-group mb-3">
                                        <label for="firstName1">Titre</label>
                                        <input class="form-control" required value="" name="title" type="text">
                                    </div>

                                    <div class="col-md-12 pull-right">
                                        <button type="reset" class="btn btn-warning">Réinitialiser</button>
                                        <button type="submit" class="btn btn-primary">Ajouter</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            <div class="card-title mb-3">Synthèse des Services d'Hôpitaux</div>
                            <table id="datatable-responsive"
                                   class="display table table-striped table-bordered" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>Titre</th>
                                    <th>Hôpitaux</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tfoot>
                                <tr>
                                    <th>Titre</th>
                                    <th>Hôpitaux</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>

                                <tbody>
                                @foreach($services as $service)
                                    <tr>
                                        <td>{{ $service->title }}</td>
                                        <td>{{ $service->hospitals->count() }}</td>
                                        <td style="text-align: center">
                                            <span class="badge badge-pill badge-outline-success p-2 m-1" title="Modifier" data-toggle="modal" data-target="#edit-{{ $service->id }}"><i class="nav-icon i-Edit"></i></span>
                                            <a href="{{ route('handleManagerDeleteHospitalService', ['id' => $service->id]) }}"><span class="badge badge-pill badge-outline-warning p-2 m-1"><i class="nav-icon i-File-Trash"></i></span></a>
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="edit-{{ $service->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <form method="post" action="{{ route('handleManagerEditHospitalService', ['id' => $service->id]) }}">
                                                    {{ csrf_field() }}
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Modifier {{ $service->title }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <label class="control-label">Titre</label>
                                                                <input name="title" required class="form-control" placeholder="Titre" value="{{ $service->title }}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                                        <button type="submit" class="btn btn-primary">Envoyer</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            <div class="card-title mb-3">Demandes D'affectation</div>
                            <table id="datatable-responsive-2"
                                   class="display table table-striped table-bordered" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>Utilisateur</th>
                                    <th>Service</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tfoot>
                                <tr>
                                    <th>Utilisateur</th>
                                    <th>Service</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>

                                <tbody>
                                @foreach($requests as $request)
                                    <tr>
                                        <td>{{ $request->user->name }}</td>
                                        <td>{{ $request->service->title }}</td>
                                        <td style="text-align: center">
                                            <a href="{{ route('handleManagerDecideHospitalServiceRequest', ['request' => $request->id, 'decision' => 1]) }}"> <span class="badge badge-pill badge-outline-success p-2 m-1" title="Accepter"><i class="nav-icon i-Edit"></i></span></a>
                                            <a href="{{ route('handleManagerDecideHospitalServiceRequest', ['request' => $request->id, 'decision' => 2]) }}"> <span class="badge badge-pill badge-outline-danger p-2 m-1" title="Refuser"><i class="nav-icon i-File-Trash"></i></span></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-4">
            <div class="row">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <div class="card-title mb-3">Affectation de Service</div>
                            <form action="{{ route('handleManagerAffectHospitalService') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12 form-group mb-3">
                                        <label for="firstName1">Service</label>
                                        <select name="service_id" class="form-control">
                                            @foreach($services as $service)
                                                <option value="{{ $service->id }}">{{ $service->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12 form-group mb-3">
                                        <label for="firstName1">Hôpital</label>
                                        <select name="hospital_id" class="form-control">
                                            @foreach($hospitals as $hospital)
                                                <option value="{{ $hospital->id }}">{{ $hospital->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-12 pull-right">
                                        <button type="reset" class="btn btn-warning">Réinitialiser</button>
                                        <button type="submit" class="btn btn-primary">Ajouter</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection
