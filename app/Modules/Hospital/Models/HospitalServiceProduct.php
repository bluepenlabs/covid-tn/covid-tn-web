<?php

namespace App\Modules\Hospital\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalServiceProduct extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hospital_service_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service_id',
        'product_id',
        'needed',
        'auto_renew',
    ];

    public function product()
    {
        return $this->hasOne('App\Modules\Hospital\Models\Product', 'id', 'product_id');
    }

    public function stocks()
    {
        return $this->hasMany('App\Modules\Hospital\Models\HospitalServiceStock', 'product_id', 'id');
    }

    public function dayStock($date)
    {
        return $this->hasOne('App\Modules\Hospital\Models\HospitalServiceStock', 'product_id', 'id')
            ->whereDate('created_at', $date);
    }

    public function service()
    {
        return $this->hasOne('App\Modules\Hospital\Models\HospitalService', 'id', 'service_id');
    }

}
