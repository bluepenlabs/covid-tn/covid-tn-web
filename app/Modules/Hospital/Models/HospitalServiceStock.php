<?php

namespace App\Modules\Hospital\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalServiceStock extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hospital_service_stock';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'quantity',
        'created_at',
    ];

    public function product()
    {
        return $this->hasOne('App\Modules\Hospital\Models\HospitalServiceProduct', 'id', 'product_id');
    }

}
