<?php

namespace App\Modules\Hospital\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalService extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hospital_services';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service_id',
        'hospital_id',
        'user_id',
        'beds',
    ];

    public function user()
    {
        return $this->hasOne('App\Modules\User\Models\User', 'id', 'user_id');
    }

    public function service()
    {
        return $this->hasOne('App\Modules\Hospital\Models\Service', 'id', 'service_id');
    }

    public function hospital()
    {
        return $this->hasOne('App\Modules\Hospital\Models\Hospital', 'id', 'hospital_id');
    }

    public function products()
    {
        return $this->hasMany('App\Modules\Hospital\Models\HospitalServiceProduct', 'service_id', 'id');
    }
}
