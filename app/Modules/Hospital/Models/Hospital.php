<?php

namespace App\Modules\Hospital\Models;

use Illuminate\Database\Eloquent\Model;

class Hospital extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hospitals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'type',
        'lat',
        'lng',
        'iban',
        'rib',
        'city_id',
        'phone',
        'address',
        'bic_swift',
        'bank',
    ];

    public function city()
    {
        return $this->hasOne('App\Modules\General\Models\City', 'id', 'city_id');
    }

    public function services()
    {
        return $this->hasMany('App\Modules\Hospital\Models\HospitalService', 'hospital_id', 'id');
    }

}
