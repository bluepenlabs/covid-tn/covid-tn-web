<?php

namespace App\Modules\Hospital\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hospitals_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'category',
        'picture',
        'description',
        'min',
        'max',
    ];

}
