<?php

namespace App\Modules\Hospital\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalServiceRequest extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hospital_service_request';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service_id',
        'user_id',
    ];

    public function user()
    {
        return $this->hasOne('App\Modules\User\Models\User', 'id', 'user_id');
    }

    public function service()
    {
        return $this->hasOne('App\Modules\Hospital\Models\Service', 'id', 'service_id');
    }
}
