<?php

namespace App\Modules\Hospital\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hospitals_services';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
    ];

    public function hospitals()
    {
        return $this->belongsToMany(
            'App\Modules\Hospital\Models\Hospital',
            'hospital_services',
            'service_id',
            'hospital_id'
        )->withTimestamps();
    }
}
