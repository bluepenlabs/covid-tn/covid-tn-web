<?php

Route::group(['module' => 'Hospital', 'middleware' => ['web'], 'namespace' => 'App\Modules\Hospital\Controllers'], function() {

    Route::get('/manager/hospitals', 'WebController@showManagerHospitalsList')->name('showManagerHospitalsList')->middleware('checkManagerAccess');
    Route::get('/manager/hospitals/services', 'WebController@showManagerHospitalsServicesList')->name('showManagerHospitalsServicesList')->middleware('checkManagerAccess');

    Route::get('/manager/hospitals/products', 'WebController@showManagerHospitalsProductsList')->name('showManagerHospitalsProductsList')->middleware('checkManagerAccess');

    Route::get('/manager/hospitals/needs/delete/{id}', 'WebController@handleManagerDeleteHospitalProduct')->name('handleManagerDeleteHospitalProduct')->middleware('checkManagerAccess');
    Route::post('/manager/hospitals/needs/edit/{id}', 'WebController@handleManagerEditHospitalProduct')->name('handleManagerEditHospitalProduct')->middleware('checkManagerAccess');
    Route::post('/manager/hospitals/needs/add', 'WebController@handleManagerAddHospitalProduct')->name('handleManagerAddHospitalProduct')->middleware('checkManagerAccess');

    Route::post('/manager/hospitals/add', 'WebController@handleManagerAddHospital')->name('handleManagerAddHospital')->middleware('checkManagerAccess');
    Route::post('/manager/hospitals/edit/{id}', 'WebController@handleManagerEditHospital')->name('handleManagerEditHospital')->middleware('checkManagerAccess');
    Route::get('/manager/hospitals/delete/{id}', 'WebController@handleManagerDeleteHospital')->name('handleManagerDeleteHospital')->middleware('checkManagerAccess');

    Route::post('/manager/hospitals/service/add', 'WebController@handleManagerAddHospitalService')->name('handleManagerAddHospitalService')->middleware('checkManagerAccess');
    Route::post('/manager/hospitals/service/hospital/affect', 'WebController@handleManagerAffectHospitalService')->name('handleManagerAffectHospitalService')->middleware('checkManagerAccess');
    Route::get('/manager/hospitals/service/hospital/decide/{request}/{decision}', 'WebController@handleManagerDecideHospitalServiceRequest')->name('handleManagerDecideHospitalServiceRequest')->middleware('checkManagerAccess');
    Route::post('/manager/hospitals/service/product/affect', 'WebController@handleManagerAffectProductService')->name('handleManagerAffectProductService')->middleware('checkManagerAccess');
    Route::post('/manager/hospitals/service/edit/{id}', 'WebController@handleManagerEditHospitalService')->name('handleManagerEditHospitalService')->middleware('checkManagerAccess');
    Route::get('/manager/hospitals/service/delete/{id}', 'WebController@handleManagerDeleteHospitalService')->name('handleManagerDeleteHospitalService')->middleware('checkManagerAccess');

    Route::get('/medical/hospital/service/{id}', 'WebController@showMedicalAgentHospitalService')->name('showMedicalAgentHospitalService')->middleware('checkMedicalAccess');

    Route::get('/medical/edit/{id}', 'WebController@showMedicalAgentHospital')->name('showMedicalAgentHospital')->middleware('checkMedicalAccess');

    Route::post('/medical/hospital/service/{id}/edit/', 'WebController@handleMedicalAgentEditHospital')->name('handleMedicalAgentEditHospital')->middleware('checkMedicalAccess');
    Route::post('/medical/hospital/service/{id}/delete/{product_id}', 'WebController@handleMedicalAgentDeleteProduct')->name('handleMedicalAgentDeleteProduct')->middleware('checkMedicalAccess');
    Route::post('/medical/hospital/service/{id}/edit/beds', 'WebController@handleMedicalAgentEditServiceBeds')->name('handleMedicalAgentEditServiceBeds')->middleware('checkMedicalAccess');
    Route::post('/medical/hospital/service/{id}/needs/add', 'WebController@handleMedicalAgentAddProduct')->name('handleMedicalAgentAddProduct')->middleware('checkMedicalAccess');

    Route::get('/medical/hospital/service/{id}/needs', 'WebController@showMedicalAgentHospitalRequests')->name('showMedicalAgentHospitalRequests')->middleware('checkMedicalAccess');
    Route::get('/hospital/view/{id}', 'WebController@showSingleHospital')->name('showSingleHospital');

    Route::post('/hospital/search/{offset?}', 'WebController@handleSearchHospital')->name('handleSearchHospital');

    Route::get('/medical/hospital/service/{id}/day/{date}', 'WebController@getStockByDay')->name('getStockByDay')->middleware('checkMedicalAccess');
    Route::post('/medical/hospital/service/{id}/update', 'WebController@handleMedicalAgentEditProductStock')->name('handleMedicalAgentEditProductStock')->middleware('checkMedicalAccess');
    Route::get('/medical/hospital/service/{id}/request', 'WebController@handleUserRequestServiceManagement')->name('handleUserRequestServiceManagement');

});
