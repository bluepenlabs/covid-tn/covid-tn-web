<?php

Route::group(['module' => 'Hospital', 'middleware' => ['api'], 'namespace' => 'App\Modules\Hospital\Controllers'], function() {

    Route::get('/api/medical/hospitals/{limit?}/{offset?}', 'ApiController@getHospitals');
    Route::get('/api/medical/products/{limit?}/{offset?}', 'ApiController@getProducts');
    Route::get('/api/medical/hospital/services/{id}', 'ApiController@getHospitalServices');
    Route::get('/api/medical/hospital/service/{id}/needs', 'ApiController@getHospitalServiceProducts');
    Route::get('/api/medical/hospital/service/{id}/user', 'ApiController@getHospitalServiceMedicalAgent');
    Route::get('/api/medical/hospital/service/{id}/needs/stocks/{date}', 'ApiController@getHospitalServiceStockByDate');

    Route::post('/api/medical/hospital/service/{id}/request', 'ApiController@handleMedicalAgentRequestServiceAccess');
    Route::post('/api/medical/hospital/service/{id}/beds', 'ApiController@handleMedicalAgentUpdateServiceBeds');
    Route::post('/api/medical/hospital/service/{id}/needs/add', 'ApiController@handleMedicalAgentAddProduct');
    Route::post('/api/medical/hospital/service/{id}/needs/stock', 'ApiController@handleMedicalAgentEditProductStock');
    Route::post('/api/medical/hospital/service/{id}/needs/delete', 'ApiController@handleMedicalAgentDeleteProduct');

});
