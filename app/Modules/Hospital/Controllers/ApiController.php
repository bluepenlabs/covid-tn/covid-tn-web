<?php

namespace App\Modules\Hospital\Controllers;

use App\Modules\General\Models\City;
use App\Modules\General\Models\General;
use App\Modules\Hospital\Models\Hospital;
use App\Modules\Hospital\Models\HospitalRequest;
use App\Modules\Hospital\Models\HospitalRequestEntry;
use App\Modules\Hospital\Models\HospitalService;
use App\Modules\Hospital\Models\HospitalServiceProduct;
use App\Modules\Hospital\Models\HospitalServiceRequest;
use App\Modules\Hospital\Models\HospitalServiceStock;
use App\Modules\Hospital\Models\Product;
use App\Modules\User\Models\User;
use App\Modules\Volunteer\Models\Category;
use App\Modules\Volunteer\Models\Volunteer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Kamaln7\Toastr\Facades\Toastr;
use phpDocumentor\Reflection\Types\Collection;

class ApiController extends Controller
{
    public function getHospitals($limit = 10, $offset = 0)
    {
        if($limit > 100){ $limit = 100; }

        $hospitals = Hospital::select('id', 'title', 'lat', 'lng', 'created_at', 'city_id')->with('city')
            ->limit($limit)
            ->skip($offset * $limit)->get();

        foreach($hospitals as $hospital){
            $hospital->id = $hospital->id * 1021;
        }

        if(!$hospitals or count($hospitals) == 0){
            return response()->json(['data' => [], 'status' => ['message' => 'No data found', 'code' => 404]]);
        }

        return response()->json(['data' => $hospitals, 'error' => ['message' => '', 'code' => 200]]);
    }

    public function getProducts($limit = 10, $offset = 0)
    {
        if($limit > 100){ $limit = 100; }

        $products = Product::select('id', 'title', 'description', 'min', 'max', 'category')
            ->limit($limit)
            ->skip($offset * $limit)->get();

        foreach($products as $product){
            $product->id = $product->id * 1021;
        }

        if(!$products or count($products) == 0){
            return response()->json(['data' => [], 'status' => ['message' => 'No data found', 'code' => 404]]);
        }

        return response()->json(['data' => $products, 'error' => ['message' => '', 'code' => 200]]);
    }

    public function getHospitalServices($id, $limit = 10, $offset = 0)
    {
        if($limit > 100){ $limit = 100; }

        $services = HospitalService::where('hospital_id', $id)->select('id', 'service_id', 'user_id', 'beds')->with('user', 'service')
            ->limit($limit)
            ->skip($offset * $limit)->get();

        foreach($services as $service){
            $service->id = $service->id * 1021;
        }

        if(!$services or count($services) == 0){
            return response()->json(['data' => [], 'status' => ['message' => 'No data found', 'code' => 404]]);
        }

        return response()->json(['data' => $services, 'error' => ['message' => '', 'code' => 200]]);
    }

    public function getHospitalServiceProducts($id, $limit = 10, $offset = 0)
    {
        $decryptedId = $id / 1021;
        if($limit > 100){ $limit = 100; }

        $needs = HospitalServiceProduct::where('service_id', $decryptedId)->take($limit)->skip($offset)
            ->with('product')->with('stocks')->get();

        if(!$needs or count($needs) == 0){
            return response()->json(['data' => [], 'status' => ['message' => 'No data found', 'code' => 404]]);
        }

        return response()->json(['data' => $needs, 'error' => ['message' => '', 'code' => 200]]);
    }


    public function getHospitalServiceMedicalAgent($id)
    {
        $decryptedId = $id / 1021;

        $medicalAgent = User::whereHas('services', function ($query) use ($decryptedId) {
                                $query->where('id', '=', $decryptedId);
                            })
                            ->select('name', 'created_at')
                            ->first();

        if(!$medicalAgent){
            return response()->json(['data' => [], 'status' => ['message' => 'No data found', 'code' => 404]]);
        }

        return response()->json(['data' => $medicalAgent, 'error' => ['message' => '', 'code' => 200]]);
    }

    public function getHospitalServiceStockByDate($id, $date)
    {
        $decryptedId = $id / 1021;
        $date = Carbon::createFromFormat('Y-m-d', $date)->format('Y-m-d');

        $entries = HospitalServiceStock::whereDate('hospital_service_stock.created_at', '=', $date)
            ->whereHas('product', function ($query) use($decryptedId) {
                $query->whereHas('service', function ($query) use($decryptedId) {
                    $query->where('id', '=', $decryptedId);
                });
            })
            ->with('product')
            ->get();

        if(!$entries or count($entries) == 0){
            return response()->json(['data' => [], 'status' => ['message' => 'No data found', 'code' => 404]]);
        }

        return response()->json(['data' => $entries, 'error' => ['message' => '', 'code' => 200]]);
    }

    public function handleMedicalAgentUpdateServiceBeds(Request $request)
    {
        $data = $request->json()->all();

        if (!isset($data['token']) or !checkApiToken($data['token'])) {
            return response()->json(['data' => [], 'status' => ['message' => 'Token mismatch', 'code' => 403]]);
        }

        if (!isset($data['id']) or !isset($data['beds']))
        {
            return response()->json(['data' => [], 'status' => ['message' => 'Required data missing', 'code' => 404]]);
        }

        $decryptedId = $data['id'] / 1021;

        HospitalService::where('id', $decryptedId)->update([
            'beds' => $data['beds']
        ]);

        return response()->json(['data' => [], 'status' => ['message' => 'Update Successful', 'code' => 200]]);

    }

    public function handleMedicalAgentAddProduct(Request $request)
    {
        $data = $request->json()->all();

        if (!isset($data['token']) or !checkApiToken($data['token'])) {
            return response()->json(['data' => [], 'status' => ['message' => 'Token mismatch', 'code' => 403]]);
        }

        if (!isset($data['service_id']) or
            !isset($data['product_id']) or
            !isset($data['needed']) or
            !isset($data['auto_renew'])
        )
        {
            return response()->json(['data' => [], 'status' => ['message' => 'Required data missing', 'code' => 404]]);
        }

        HospitalServiceProduct::updateOrCreate([
            'service_id' =>  $data['service_id'],
            'product_id' => $data['product_id'],
        ],[
            'needed' => $data['needed'],
            'auto_renew' => $data['renew'], // 1 Yes / 0 No
        ]);

        return response()->json(['data' => [], 'status' => ['message' => 'Product added successfully', 'code' => 200]]);

    }


    public function handleMedicalAgentRequestServiceAccess(Request $request){
        $data = $request->json()->all();

        if (!isset($data['token']) or !checkApiToken($data['token'])) {
            return response()->json(['data' => [], 'status' => ['message' => 'Token mismatch', 'code' => 403]]);
        }

        if (!isset($data['email']) or
            !isset($data['password']) or
            !isset($data['name']) or
            !isset($data['service_id'])
        )
        {
            return response()->json(['data' => [], 'status' => ['message' => 'Required data missing', 'code' => 404]]);
        }

        $credentials = [
            'email' => $data['email'],
            'password' => $data['password'],
        ];

        $check = HospitalService::where('service_id', $data['service_id'])->first();

        if($check->user){
            return response()->json(['data' => [], 'status' => ['message' => 'Service already attached to user', 'code' => 304]]);
        }

        $userSocial = User::where('email', $data['email'])->where('provider_id', '=', $data['provider_id'])->first();
        if ($userSocial){
            $user = $userSocial;
            if ($user->status === 3) {
                return response()->json(['data' => [], 'status' => ['message' => 'User banned', 'code' => 500]]);
            }
        }
        elseif (Auth::validate($credentials)) {
            $user = User::where('email', $data['email'])->first();

            if ($user->status === 3) {
                return response()->json(['data' => [], 'status' => ['message' => 'User banned', 'code' => 500]]);
            }
        } else {
            $user = User::where('email', $data['email'])->first();

            if($user){
                return response()->json(['data' => [], 'status' => ['message' => 'Wrong Email or Password', 'code' => 401]]);
            }

            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password'])
            ]);

            User::find($user->id)->roles()->attach(2);
        }

        $checkRequest = HospitalServiceRequest::where('service_id', $data['service_id'])
            ->where('user_id', $user->id)
            ->first();

        if($checkRequest){
            return response()->json(['data' => [], 'status' => ['message' => 'Service already requested', 'code' => 306]]);
        }

        HospitalServiceRequest::create([
            'user_id' => $user->id,
            'service_id' => $data['service_id'],
        ]);

        return response()->json(['data' => [], 'status' => ['message' => 'Service requested', 'code' => 200]]);
    }

    public function handleMedicalAgentEditProductStock(Request $request)
    {
        $data = $request->json()->all();

        if (!isset($data['token']) or !checkApiToken($data['token'])) {
            return response()->json(['data' => [], 'status' => ['message' => 'Token mismatch', 'code' => 403]]);
        }

        if (!isset($data['product_id']) or
            !isset($data['id']) or
            !isset($data['date']) or
            !isset($data['quantity'])
        )
        {
            return response()->json(['data' => [], 'status' => ['message' => 'Required data missing', 'code' => 404]]);
        }

        $date = Carbon::createFromFormat('Y-m-d', $data['date'])->setTime(12, 0, 0);

        $product_stock =  HospitalServiceStock::where('product_id', $data['product_id'])->whereDate('created_at', $date->format('Y-m-d'))->first();

        $productNeedCheck = HospitalServiceProduct::find($data['product_id']);

        if($productNeedCheck->needed < $data['quantity']){
            return response()->json(['data' => [], 'status' => ['message' => 'Quantity higher than needed', 'code' => 402]]);
        }

        if ($product_stock) {
            $product_stock->quantity = $data['quantity'];
            $product_stock->save();
        } else {
            HospitalServiceStock::create([
                'product_id' => $data['product_id'],
                'quantity' => $data['quantity'],
                'created_at' => $date,
            ]);
        }

        return response()->json(['data' => [], 'status' => ['message' => 'Stock updated successfully', 'code' => 200]]);


    }



    public function handleMedicalAgentDeleteProduct(Request $request){
        $data = $request->json()->all();

        if (!isset($data['token']) or !checkApiToken($data['token'])) {
            return response()->json(['data' => [], 'status' => ['message' => 'Token mismatch', 'code' => 403]]);
        }

        if (!isset($data['product_id']) or
            !isset($data['service_id']))
        {
            return response()->json(['data' => [], 'status' => ['message' => 'Required data missing', 'code' => 404]]);
        }

        $product = HospitalServiceProduct::where('product_id', $data['product_id'])->where('service_id', $data['service_id'])->first();

        if(count($product->stocks) > 0) {
            HospitalServiceStock::where('product_id', $product->id)->delete();
        }

        $product->destroy();

        return response()->json(['data' => [], 'status' => ['message' => 'Product successfully deleted', 'code' => 200]]);
    }

}
