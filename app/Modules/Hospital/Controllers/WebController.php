<?php

namespace App\Modules\Hospital\Controllers;

use App\Modules\General\Models\City;
use App\Modules\General\Models\General;
use App\Modules\Hospital\Models\Hospital;
use App\Modules\Hospital\Models\HospitalService;
use App\Modules\Hospital\Models\HospitalServiceProduct;
use App\Modules\Hospital\Models\HospitalServiceRequest;
use App\Modules\Hospital\Models\HospitalServiceStock;
use App\Modules\Hospital\Models\Product;
use App\Modules\Hospital\Models\Service;
use App\Modules\User\Models\User;
use App\Modules\Volunteer\Models\Category;
use App\Modules\Volunteer\Models\Organisation;
use App\Modules\Volunteer\Models\Volunteer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Kamaln7\Toastr\Facades\Toastr;

class WebController extends Controller
{

    public function showManagerHospitalsList()
    {
        return view('Hospital::backOffice.hospitals', [
            'hospitals' => Hospital::all(),
            'cities' => City::all(),
        ]);
    }

    public function showManagerHospitalsServicesList()
    {
        return view('Hospital::backOffice.services', [
            'services' => Service::all(),
            'hospitals' => Hospital::all(),
            'requests' => HospitalServiceRequest::all(),
        ]);
    }

    public function handleManagerDecideHospitalServiceRequest($request, $decision){

        $request = HospitalServiceRequest::find($request);

        if($decision == 1){
            $service = HospitalService::where('service_id', $request->service_id)->whereHas('user')->first();

            if($service){
                Toastr::error('Service déjà attaché à un utilisateur !');
                return back();
            }

            HospitalService::where('service_id', $request->service_id)->update([
                'user_id' => $request->user_id
            ]);

            $user = User::find($request->user_id);
            $user->roles()->attach(3);

            $request->delete();
            Toastr::success('Service attaché avec succès !');
            return back();
        } else {
            $request->delete();
            Toastr::warning('Rattachement refusé !');
            return back();
        }
    }

    public function handleManagerAffectHospitalService(Request $request)
    {
        $data = $request->all();

        $check = HospitalService::where('hospital_id', $data['hospital_id'])->where('service_id', $data['service_id'])->count();

        if($check > 0){
            Toastr::error('Service déjà attaché à cet hôpital !');
            return back();
        }

        HospitalService::create([
            'hospital_id' => $data['hospital_id'],
            'service_id' => $data['service_id'],
        ]);

        Toastr::success('Service attaché avec succès !');
        return back();
    }

    public function handleManagerAffectProductService(Request $request)
    {
        $data = $request->all();

        $check = HospitalServiceProduct::where('service_id', $data['service_id'])->where('product_id', $data['product_id'])->count();

        if($check > 0){
            Toastr::error('Produit déjà attaché à ce service !');
            return back();
        }

        HospitalServiceProduct::create([
            'product_id' => $data['product_id'],
            'service_id' => $data['service_id'],
            'needed' => $data['needed'],
            'auto_renew' => isset($data['auto_renew']) ? 1 : 0,
        ]);

        Toastr::success('Produit attaché avec succès !');
        return back();
    }

    public function showManagerHospitalsProductsList()
    {
        return view('Hospital::backOffice.products', [
            'products' => Product::all(),
            'hospitals' => Hospital::all(),
        ]);
    }

    public function handleManagerAddHospital(Request $request)
    {
        $data = $request->all();

        Hospital::create([
            'title' => $data['title'],
            'type' => $data['type'],
            'city_id' => $data['city'],
            'address' => isset($data['address']) ? $data['address'] : null,
            'lat' => $data['lat'],
            'lng' => $data['lng'],
            'phone' => isset($data['phone']) ? $data['phone'] : null,
            'iban' => isset($data['iban']) ? $data['iban'] : null,
            'rib' => isset($data['rib']) ? $data['rib'] : null,
            'bank' => isset($data['bank']) ? $data['bank'] : null,
            'bic_swift' => isset($data['bs']) ? $data['bs'] : null,
        ]);

        Toastr::success('Hôpital ajouté avec succès !');
        return back();
    }

    public function handleManagerAddHospitalService(Request $request)
    {
        $data = $request->all();

        Service::create([
            'title' => $data['title'],
        ]);

        Toastr::success('Service ajouté avec succès !');
        return back();
    }

    public function handleManagerAddHospitalProduct(Request $request)
    {
        $data = $request->all();

        Product::create([
            'title' => $data['title'],
            'description' => $data['description'],
            'category' => $data['category'],
            'min' => $data['min'],
            'max' => $data['max'],
        ]);

        Toastr::success('Produit ajouté avec succès !');
        return back();
    }

    public function handleManagerDeleteHospital($id)
    {
        $hospital = Hospital::find($id);

        if (!$hospital) {
            Toastr::error('Hôpital introuvable');
            return back();
        }

        if ($hospital->services->count() > 0) {
            Toastr::error('Hôpital utilisé');
            return back();
        }

        $hospital->delete();

        Toastr::warning('Hôpital supprimé');
        return back();

    }

    public function handleManagerDeleteHospitalService($id)
    {
        $service = Service::find($id);

        if (!$service) {
            Toastr::error('Service introuvable');
            return back();
        }

        $exists = HospitalService::where('service_id', $id)->count();

        if($exists > 0){
            Toastr::error('Service utilisé');
            return back();
        }

        $service->delete();

        Toastr::warning('Service supprimé');
        return back();

    }

    public function handleManagerEditHospital($id, Request $request)
    {
        $data = $request->all();

        $hospital = Hospital::find($id);

        $hospital->title = isset($data['title']) ? $data['title'] : $hospital->title;
        $hospital->address = isset($data['address']) ? $data['address'] : $hospital->address;
        $hospital->city_id = $data['city'];
        $hospital->lat = isset($data['lat']) ? $data['lat'] : $hospital->lat;
        $hospital->lng = isset($data['lng']) ? $data['lng'] : $hospital->lng;
        $hospital->phone = isset($data['phone']) ? $data['phone'] : $hospital->phone;
        $hospital->iban = isset($data['iban']) ? $data['iban'] : $hospital->iban;
        $hospital->rib = isset($data['rib']) ? $data['rib'] : $hospital->rib;
        $hospital->bank = isset($data['bank']) ? $data['bank'] : $hospital->bank;
        $hospital->bic_swift = isset($data['bs']) ? $data['bs'] : $hospital->bic_swift;

        $hospital->save();

        Toastr::success('Hôpital modifié');
        return back();
    }

    public function handleManagerEditHospitalService($id, Request $request)
    {
        $data = $request->all();

        $service = Service::find($id);

        $service->title = $data['title'];

        $service->save();

        Toastr::success('Service modifié');
        return back();
    }

    public function handleManagerEditHospitalProduct($id, Request $request)
    {
        $data = $request->all();

        $product = Product::find($id);

        $product->title = $data['title'];
        $product->category = $data['category'];
        $product->description = $data['description'];
        $product->min = $data['min'];
        $product->max = $data['max'];

        $product->save();

        Toastr::success('Produit modifié');
        return back();
    }

    public function showMedicalAgentHospitalService($id)
    {
        $service = HospitalService::find($id);

        if (!Auth::user() or $service->user_id != Auth::user()->id) {
            Toastr::error('Vous n\'êtes pas authorisé a accéder à cette page !');
            return back();
        }
        return view('Hospital::frontOffice.private.service', [
            'service' => $service,
            'products' => Product::all(),
            'cities' => City::all(),
            'services' => HospitalService::count(),
            'volunteers' => Volunteer::count(),
            'hospitals' => Hospital::count(),
        ]);
    }

    public function showMedicalAgentHospital($id)
    {
        $hospital = Hospital::find($id);

        $authorized = false;
        if (!Auth::user()) {
            $authorized = false;
        } else {
            foreach($hospital->services as $service){
                if($service->user_id == Auth::user()->id){
                    $authorized = true;
                    break;
                }
            }
        }

        if (!$authorized) {
            Toastr::error('Vous n\'êtes pas authorisé a accéder à cette page !');
            return back();
        }

        return view('Hospital::frontOffice.private.hospital', [
            'hospital' => $hospital,
            'types' => HospitalServiceStock::all(),
            'cities' => City::all(),
            'services' => HospitalService::count(),
            'volunteers' => Volunteer::count(),
            'hospitals' => Hospital::count(),
        ]);
    }

    public function handleMedicalAgentAddProduct($id, Request $request){
        $data = $request->all();

        HospitalServiceProduct::create([
           'service_id' =>  $id,
            'product_id' => $data['product_id'],
            'needed' => $data['quantity'],
            'auto_renew' => isset($data['renew']) ? 1 : 0,
        ]);

        Toastr::success('Profil ajouté avec succès !');
        return back();

    }

    public function handleMedicalAgentEditHospital($id, Request $request)
    {
        $hospital = Hospital::find($id);

        if ($hospital->user_id != Auth::user()->id) {
            Toastr::error('Hôpital non relié a votre profil !');
            return back();
        }

        $data = $request->all();

        $hospital->type = $data['type'];
        $hospital->phone = $data['phone'];
        $hospital->city_id = $data['city'];
        $hospital->address = $data['address'];

        $hospital->save();

        Toastr::success('Hôpital mis à jour avec succès !');
        return back();
    }

    public function handleMedicalAgentEditServiceBeds($id, Request $request){
        $data = $request->all();

        HospitalService::where('id', $id)->update([
            'beds' => $data['beds']
        ]);

        Toastr::success('Nombre de lits mis à jour !');
        return back();
    }

    public function showSingleHospital($id)
    {
        $hospital = Hospital::find($id);

        $needs = 0;
        foreach($hospital->services as $service){
            $needs = $needs + $service->products->count();
        }

        $hospital->needs = $needs;

        return view('Hospital::frontOffice.public.help', [
            'hospital' => $hospital,
            'announcement' => General::find(1)->announcement,
        ]);
    }

    public function handleSearchHospital(Request $request, $offset = 1)
    {
        $data = $request->all();

        $hospitalsOnMap = Hospital::all();

        foreach($hospitalsOnMap as $hospital){
            $need = [];
            foreach($hospital->services as $service){
                foreach($service->products as $product){
                    $need[$product->product->id] = isset($need[$product->product->id]) ? $need[$product->product->id] : $product->product;
                    if($product->auto_renew == 1){
                        $stock = $product->dayStock(today())->first();
                        if($stock){
                            $need[$product->product->id]->stock = $need[$product->product->id]->stock + ($product->needed - $stock->quantity);
                        } else {
                            $need[$product->product->id]->stock = $need[$product->product->id]->stock + $product->needed;
                        }
                    } else {
                        $stock = $product->stocks->sum('quantity');
                        if($stock){
                            $need[$product->product->id]->stock = $need[$product->product->id]->stock + ($product->needed - $stock);
                        } else {
                            $need[$product->product->id]->stock = $need[$product->product->id]->stock + $product->needed;
                        }
                    }
                }
            }
            $hospital->needs = collect($need);
        }

        $hospitals = Hospital::when($data['city'] != -1, function ($query) use ($data) {
            $query->where('city_id', '=', $data['city']);
        })
            ->when($data['title'] != '', function ($query) use ($data) {
                $query->where('title', 'LIKE', '%' . $data['title'] . '%');
            })
            ->when($data['type'] != -1, function ($query) use ($data) {
                $query->where('type', '=', $data['type']);
            })
            ->when(isset($data['needs']), function ($query) use ($data) {
                $query->whereHas('services', function ($query) use ($data) {
                    $query->whereHas('products', function ($query) use ($data) {
                        $query->where(function ($subQuery) use ($data) {
                            foreach ($data['needs'] as $need) {
                                $subQuery->orWhere('product_id', '=', $need);
                            }
                        });
                    });
                });
            })
            ->take(10)
            ->skip(($offset - 1) * 10)
            ->get();

        if(count($hospitals) == 0){
            Toastr::warning('Aucun hôpital trouvé !');
            return redirect(route('showHome'));
        }

        foreach($hospitals as $hospital){
            $need = [];
            foreach($hospital->services as $service){
                foreach($service->products as $product){
                    $need[$product->product->id] = isset($need[$product->product->id]) ? $need[$product->product->id] : $product->product;
                    if($product->auto_renew == 1){
                        $stock = $product->dayStock(today())->first();
                        if($stock){
                            $need[$product->product->id]->stock = $need[$product->product->id]->stock + ($product->needed - $stock->quantity);
                        } else {
                            $need[$product->product->id]->stock = $need[$product->product->id]->stock + $product->needed;
                        }
                    } else {
                        $stock = $product->stocks->sum('quantity');
                        if($stock){
                            $need[$product->product->id]->stock = $need[$product->product->id]->stock + ($product->needed - $stock);
                        } else {
                            $need[$product->product->id]->stock = $need[$product->product->id]->stock + $product->needed;
                        }
                    }
                }
            }
            $hospital->needs = collect($need);
        }


        return view('General::frontOffice.public.home', [
            'hospitals' => $hospitals,
            'searchCount' => count($hospitals),
            'hospitalsOnMap' => $hospitalsOnMap,
            'products' => Product::all(),
            'organisations' => Organisation::all(),
            'cities' => City::all(),
            'announcement' => General::all()[0]->announcement,
            'volunteersCategories' => Category::all(),
            'data' => $data,
            'offset' => $offset,
        ]);
    }

    public function getStockByDay($id,$date)
    {
        $service = HospitalService::find($id);

        $dayProducts = $service->products;

        $dayProducts->map(function ($product) use ($date) {
            $product['title'] = $product->product->title;
            $product['needed'] = $product->needed;
            if ($product->dayStock($date)->first()) {
                $product['quantity'] = $product->dayStock($date)->first()->quantity;
                $product['stock_id'] = $product->dayStock($date)->first()->id;
            } else {
                $product['quantity'] = 0;
                $product['stock_id'] = null;
            }

        });
        return response()->json(['status' => 200, 'data'=> $service->products]);
    }

    public function handleMedicalAgentEditProductStock(Request $request)
    {
        $data = $request->all();

        $date = \Carbon\Carbon::createFromFormat('Y-m-d', $data['date']);

        for ($i=0; $i < count($data['id']); $i++) {

            $product_stock =  HospitalServiceStock::where('product_id', $data['product_id'][$i])->whereDate('created_at', $date->format('Y-m-d'))->first();

            $productNeedCheck = HospitalServiceProduct::find($data['product_id']);
            if($productNeedCheck->needed < $data['quantity']){
                Toastr::error('Le stock de '. $productNeedCheck->product->title .' dépasse le besoin');
                return back();
            }

            if ($product_stock) {
                $product_stock->quantity = $data['quantity'][$i];
                $product_stock->save();
            } else {
                HospitalServiceStock::create([
                    'product_id' => $data['product_id'][$i],
                    'quantity' => $data['quantity'][$i],
                    'created_at' => $date,
                    ]);
                }
        };

        Toastr::success('Le stock a été mis à jour');
        return back();
    }

    public function handleUserRequestServiceManagement($id){
        if(!Auth::user()){
            Toastr::warning('Vous devez vous connecter pour demander le rattachement !');
            return back();
        }

        $check = HospitalService::where('service_id', $id)->whereHas('user')->first();

        if($check){
            Toastr::error('Service déjà attaché à un utilisateur !');
            return back();
        }

        $check = HospitalServiceRequest::where('service_id', $id)->where('user_id', Auth::user()->id)->first();

        if($check){
            Toastr::error('Vous avez déjà demandé ce rattachement !');
            return back();
        }

        HospitalServiceRequest::create([
            'service_id' => $id,
            'user_id' => Auth::user()->id
        ]);

        Toastr::success('Service demandé avec succès !');
        return back();
    }

    public function handleMedicalAgentDeleteProduct($id, $product_id){
        $product = HospitalServiceProduct::where('product_id', $product_id)->where('service_id', $id)->first();

        if(count($product->stocks) > 0){
            HospitalServiceStock::where('product_id', $product->id)->delete();
        }
        $product->destroy();

        Toastr::success('Besoin supprimé avec succès !');
        return back();
    }
}
