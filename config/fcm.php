<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAAEm8qqKY:APA91bGvapFYr7cBx3iBVH1MqjAzHv_TewWGKl3A5lWW4-arzPzGg9TvvnlZZOCtbqPdwo8GLsp77LQoViLcbvwoJspIqaxS7V6LtN7LHY6mcDnhEmuZ-Ylb4-hbReT18Ff7AVXluNgW'),
        'sender_id' => env('FCM_SENDER_ID', '79174477990'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
    /* Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyBRJQqA2pOFX-shd3sSVlMQb9vXdAZQsjo",
        authDomain: "covid-19-tunisie.firebaseapp.com",
        databaseURL: "https://covid-19-tunisie.firebaseio.com",
        projectId: "covid-19-tunisie",
        storageBucket: "covid-19-tunisie.appspot.com",
        messagingSenderId: "79174477990",
        appId: "1:79174477990:web:66b85b45e109f29acf6c67",
        measurementId: "G-4ZMJZ75CWW"
    };
    */

];
