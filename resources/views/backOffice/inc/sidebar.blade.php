<div class="side-content-wrap">
    <div class="sidebar-left open" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item @if($current == 'dashboard') active @endif">
                <a class="nav-item-hold" href="{{ route('showManagerHome') }}">
                    <i class="nav-icon i-Bar-Chart"></i>
                    <span class="nav-text">Accueil</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item @if($current == 'general') active @endif">
                <a class="nav-item-hold" href="{{ route('showManagerGeneralConfiguration') }}">
                    <i class="nav-icon i-Line-Chart"></i>
                    <span class="nav-text">Général</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item @if($current == 'hospitals') active @endif"  data-item="hospitals">
                <a class="nav-item-hold" href="">
                    <i class="nav-icon i-Hospital"></i>
                    <span class="nav-text">Hôpitaux</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item @if($current == 'users') active @endif">
                <a class="nav-item-hold" href="{{ route('showManagerUsersList') }}">
                    <i class="nav-icon i-Find-User"></i>
                    <span class="nav-text">Utilisateurs</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item @if($current == 'volunteers') active @endif" data-item="volunteers">
                <a class="nav-item-hold">
                    <i class="nav-icon i-Checked-User"></i>
                    <span class="nav-text">Volontaires</span>
                </a>
                <div class="triangle"></div>
            </li>
        </ul>
    </div>

    <div class="sidebar-left-secondary" data-perfect-scrollbar data-suppress-scroll-x="true">
        <!-- Submenu Dashboards -->
        <ul class="childNav" data-parent="hospitals">
            <li class="nav-item">
                <a class="" href="{{ route('showManagerHospitalsList') }}">
                    <i class="nav-icon i-Hospital1"></i>
                    <span class="item-name">Synthèse</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="" href="{{ route('showManagerHospitalsServicesList') }}">
                    <i class="nav-icon i-Hospital"></i>
                    <span class="item-name">Services</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="" href="{{ route('showManagerHospitalsProductsList') }}">
                    <i class="nav-icon i-Building"></i>
                    <span class="item-name">Besoins</span>
                </a>
            </li>
        </ul>
        <ul class="childNav" data-parent="volunteers">
            <li class="nav-item">
                <a class="" href="{{ route('showManagerVolunteersList') }}">
                    <i class="nav-icon i-Love-User"></i>
                    <span class="item-name">Synthèse</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="" href="{{ route('showManagerVolunteersCategoriesList') }}">
                    <i class="nav-icon i-Brain"></i>
                    <span class="item-name">Catégories</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="" href="{{ route('showManagerVolunteersOrganisationsList') }}">
                    <i class="nav-icon i-Building"></i>
                    <span class="item-name">Organisations</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="sidebar-overlay"></div>
</div>
