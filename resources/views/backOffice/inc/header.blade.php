<div class="main-header">
    <div class="logo">
        <img src="{{ asset('img/covid-logo-rev.png') }}" alt="">
    </div>

    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>

    <div class="d-flex align-items-center">
    </div>

    <div style="margin: auto"></div>

    <div class="header-part-right">
        <!-- Full screen toggle -->
        <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
        <!-- Grid menu Dropdown -->

        <!-- User avatar dropdown -->
        <div class="dropdown">
            <div  class="user col align-self-end">
                <img @if(Auth::user()->photo) src="{{ asset(Auth::user()->photo) }}" @else src="{{ asset('img/unknown.png') }}" @endif id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <div class="dropdown-header">
                        <i class="i-Lock-User mr-1"></i> {{ Auth::user()->name }}
                    </div>
                    <a class="dropdown-item" href="{{ route('showHome') }}">FrontOffice</a>
                    <a class="dropdown-item" href="{{ route('handleLogout') }}">Déconnexion</a>
                </div>
            </div>
        </div>
    </div>

</div>
