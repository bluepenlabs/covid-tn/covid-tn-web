<!DOCTYPE HTML>
<html lang="fr">

@yield('head')
<body>
{!! Toastr::render() !!}

<div class="loader-wrap">
    <div class="loader-inner">
        <div class="loader-inner-cirle"></div>
    </div>
</div>

<div id="main">
    <!-- TOP NAVBAR -->
    @yield('header')

    <div id="wrapper">
        @yield('content')
    </div>

    @yield('footer')
</div>
@include('frontOffice.inc.scripts')

</body>

</html>
