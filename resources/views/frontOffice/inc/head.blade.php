<head>
    <!--=============== basic  ===============-->
    <meta charset="UTF-8">
    <title>{{ $title }}</title>
    <meta name="author" content="BluePen Labs">

    <meta property="og:title" content="{{ $title }}"/>
    <meta property="og:site_name" content="Covid Tunisie"/>
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://www.covid.tn/"/>
    <meta property="og:description" content="{{ $description }}"/>
    <meta property="og:image" content="{{ asset('img/pub.jpg') }}" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="index, follow"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <!--=============== js  ===============-->
    <script type="text/javascript" src="{{ asset('plugins/jquery/jquery.min.js')}}?v={{ today()->timestamp }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.js') }}?v={{ today()->timestamp }}"></script>
    <!--=============== css  ===============-->
    <link rel="stylesheet" type="text/css" href="{{ asset ('plugins/toastr/toastr.css') }}?v={{ today()->timestamp }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/frontOffice/reset.css')}}?v={{ today()->timestamp }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/frontOffice/plugins.css')}}?v={{ today()->timestamp }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/frontOffice/style.css')}}?v={{ today()->timestamp }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/frontOffice/color.css')}}?v={{ today()->timestamp }}">
    <!--=============== favicons ===============-->
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico')}}?v={{ today()->timestamp }}">

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-33598597-9"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-33598597-9');
    </script>
</head>
