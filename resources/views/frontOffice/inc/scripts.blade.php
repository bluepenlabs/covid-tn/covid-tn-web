<!--=============== scripts  ===============-->
<script src="{{ asset('js/frontOffice/plugins.js')}}?v={{ today()->timestamp }}"></script>
<script src="{{ asset('js/frontOffice/scripts.js')}}?v={{ today()->timestamp }}"></script>

<script type="text/javascript">
    var Tawk_API=Tawk_API||{};
    @auth
        Tawk_API.visitor = {
        name  : '{{ Auth::user()->name }}',
        email : '{{ Auth::user()->email }}',
        hash : '{{  hash_hmac('sha256', Auth::user()->email, 'bad7e992243dad13a13114af3e40b85af2c5ed2b') }}'
    };
            @endauth
    var Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5e7f9ef535bcbb0c9aab69b9/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>

