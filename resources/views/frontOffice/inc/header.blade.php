<header class="main-header">
    <!-- logo-->
    <a href="{{ route('showHome') }}" class="logo-holder"><img src="{{ asset('img/covid-logo.png')}}" alt=""></a>


    @auth()
        <div class="header-user-menu">
            <div class="header-user-name">
                <span><img @if(Auth::user()->photo)src="{{ asset(Auth::user()->photo) }}" @else src="{{ asset('img/unknown.png') }}" @endif alt=""></span>
                Salut , {{ Auth::user()->name }}
            </div>
            <ul>
                <li><a href="{{ route('showUserProfile') }}"> Mon Profil</a></li>
                <li><a href="{{ route('handleLogout') }}">Déconnexion</a></li>
            </ul>
        </div>
        <div class="lang-wrap">
            <div class="show-lang"><span><i class="fal fa-chart-line"></i><a href="{{ route('showStatistics') }}" style="color: white"><strong>Statistques</strong></a> </span></div>
        </div>
    @else
        <a href="" class="modal-open add-list color-bg">Je suis BÉNÉVOLE <span><i class="fal fa-layer-plus"></i></span></a>

    <div class="show-reg-form modal-open avatar-img" data-srcav="images/avatar/3.jpg"><i class="fal fa-user"></i>Se Connecter</div>

    @endauth

    <div class="nav-button-wrap color-bg">
        <div class="nav-button">
            <span></span><span></span><span></span>
        </div>
    </div>

    <div class="nav-holder main-menu">
        <nav>
            <ul class="no-list-style">
                <li>
                    <a @if($actual == 'home') class="act-link" @endif href="{{ route('showHome') }}">Accueil</a>
                </li>
                @auth()
                @if(checkAdministratorRole(Auth::user()))
                <li>
                    <a href="{{ route('showManagerHome') }}" @if($cat == 'admin') class="act-link" @endif>Administration</a>
                </li>
                @endif
                @if(checkMedicalAgentRole(Auth::user()))
                <li>
                    <a href="#" @if($cat == 'medical') class="act-link" @endif>Mes Services <i class="fa fa-caret-down"></i></a>
                    <ul>
                        @foreach(Auth::user()->services as $service)
                        <li><a @if($actual == 'self-hospital') class="act-link" @endif href="{{ route('showMedicalAgentHospitalService', ['id' => $service->id ]) }}">{{ $service->service->title }}</a></li>
                        @endforeach
                    </ul>
                </li>
                @endif

                <li>
                    <a href="{{ route('showVolunteers') }}" @if($cat == 'volunteers') class="act-link" @endif>Bénévoles <i class="fa fa-caret-down"></i></a>
                    <!--second level -->
                    <ul>
                        <li><a @if($actual == 'self-volunteer') class="act-link" @endif href="{{ route('showVolunteerUpdateProfile') }}">J'aide</a></li>
                    </ul>
                    <!--second level end-->
                </li>

                    @else
                    <li>
                        <a href="{{ route('showVolunteers') }}" @if($cat == 'volunteers') class="act-link" @endif>Bénévoles</a>
                    </li>
                    <li>
                        <a @if($actual == 'statistics') class="act-link" @endif href="{{ route('showStatistics') }}">Statistiques</a>
                    </li>
                @endauth
                <li>
                    <a @if($actual == 'api') class="act-link" @endif href="{{ route('showAPI') }}">API</a>
                    <a @if($actual == 'contact') class="act-link" @endif href="{{ route('showContact') }}">Contact</a>
                </li>
            </ul>
        </nav>
    </div>

</header>
