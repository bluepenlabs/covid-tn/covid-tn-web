
<footer class="main-footer fl-wrap">

    <div class="sub-footer  fl-wrap">
        <div class="container">
            <div class="lang-wrap" style="background-color: transparent; color: #ffffff;">
                &#169; <strong><a href="https://www.bluepenlabs.com" target="_blank" style="color: #ffffff;">BluePen Labs</a></strong> {{ now()->format('Y') }} - Aucun Droit Réservé. <strong>Partageons notre savoir.</strong>
            </div>
        </div>
    </div>
    <!--sub-footer end -->
</footer>

<div class="main-register-wrap modal">
    <div class="reg-overlay"></div>
    <div class="main-register-holder tabs-act">
        <div class="main-register fl-wrap  modal_main">
            <div class="main-register_title">Bienvenue sur <span><strong>Covid</strong>.Tn</span></div>
            <div class="close-reg"><i class="fal fa-times"></i></div>
            <ul class="tabs-menu fl-wrap no-list-style">
                <li class="current"><a href="#tab-1"><i class="fal fa-sign-in-alt"></i> Connexion</a></li>
                <li><a href="#tab-2"><i class="fal fa-user-plus"></i> Inscription</a></li>
            </ul>
            <!--tabs -->
            <div class="tabs-container">
                <div class="tab">
                    <!--tab -->
                    <div id="tab-1" class="tab-content first-tab">
                        <div class="custom-form">
                            <form method="post" action="{{ route('handleUserLogin') }}" name="registerform">
                                {{ csrf_field() }}
                                <label>Adresse Email <span>*</span> </label>
                                <input name="email" type="email" required>
                                <label >Mot de Passe <span>*</span> </label>
                                <input name="password" type="password" required>
                                <button type="submit"  class="btn float-btn color2-bg"> Connexion <i class="fas fa-caret-right"></i></button>
                                <div class="clearfix"></div>
                                <div class="filter-tags">
                                    <input id="check-a3" type="checkbox" value="1" name="remember">
                                    <label for="check-a3">Rester connecté</label>
                                </div>
                            </form>
                            <div class="lost_password">
                                <a href="#">Mot de passe perdu ?</a>
                            </div>
                        </div>
                    </div>
                    <!--tab end -->
                    <!--tab -->
                    <div class="tab">
                        <div id="tab-2" class="tab-content">
                            <div class="custom-form">
                                <form method="post" action="{{ route('handleUserRegister') }}"  name="registerform" class="main-register-form" id="main-register-form2">
                                    {{ csrf_field() }}
                                    <label >Nom Complet <span>*</span> </label>
                                    <input name="name" type="text" required>
                                    <label>Email Address <span>*</span></label>
                                    <input name="email" type="email" required>
                                    <label >Password <span>*</span></label>
                                    <input name="password" type="password" >

                                    <div class="clearfix"></div>
                                    <button type="submit" class="btn float-btn color2-bg"> Inscription  <i class="fas fa-caret-right"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--tab end -->
                </div>
                <!--tabs end -->
                <div class="log-separator fl-wrap"><span>où</span></div>
                <div class="soc-log fl-wrap">
                    <p>Pour une connexion ou inscription plus rapide, utilisez un réseau social.</p>
                    <a href="{{ route('handleSocialRedirect', ['provider' => 'facebook']) }}" class="facebook-log"> Facebook</a>
                </div>
                <div class="wave-bg">
                    <div class='wave -one'></div>
                    <div class='wave -two'></div>
                </div>
            </div>
        </div>
    </div>
</div>

<a class="to-top"><i class="fas fa-caret-up"></i></a>
