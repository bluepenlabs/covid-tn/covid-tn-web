<?php

use App\Modules\General\Models\City;
use App\Modules\General\Models\General;
use Illuminate\Database\Seeder;

class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        City::create([ 'title' => 'Ariana', 'id' => 1 ]);
        City::create([ 'title' => 'Béjà', 'id' => 2 ]);
        City::create([ 'title' => 'Ben Arous', 'id' => 3 ]);
        City::create([ 'title' => 'Bizerte', 'id' => 4 ]);
        City::create([ 'title' => 'Gabès', 'id' => 5 ]);
        City::create([ 'title' => 'Gafsa', 'id' => 6 ]);
        City::create([ 'title' => 'Jendouba', 'id' => 7 ]);
        City::create([ 'title' => 'Kairouan', 'id' => 8 ]);
        City::create([ 'title' => 'Kasserine', 'id' => 9 ]);
        City::create([ 'title' => 'Kébili', 'id' => 10 ]);
        City::create([ 'title' => 'Kef', 'id' => 11 ]);
        City::create([ 'title' => 'Mahdia', 'id' => 12 ]);
        City::create([ 'title' => 'Manouba', 'id' => 13 ]);
        City::create([ 'title' => 'Médenine', 'id' => 14 ]);
        City::create([ 'title' => 'Monastir', 'id' => 15 ]);
        City::create([ 'title' => 'Nabeul', 'id' => 16 ]);
        City::create([ 'title' => 'Sfax', 'id' => 17 ]);
        City::create([ 'title' => 'Sidi Bouzid', 'id' => 18 ]);
        City::create([ 'title' => 'Silliana', 'id' => 19 ]);
        City::create([ 'title' => 'Sousse', 'id' => 20 ]);
        City::create([ 'title' => 'Tataouine', 'id' => 21 ]);
        City::create([ 'title' => 'Tozeur', 'id' => 22 ]);
        City::create([ 'title' => 'Tunis', 'id' => 23 ]);
        City::create([ 'title' => 'Zaghouan', 'id' => 24 ]);

        General::create([
            'announcement' => 'Pour plus d\'informations ou pour déclarer un cas d\'infection, appelez le numéro vert  <strong>80 10 19 19</strong>'
        ]);
    }
}
