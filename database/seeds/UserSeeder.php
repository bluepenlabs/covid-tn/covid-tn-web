<?php

use App\Modules\User\Models\User;
use App\Modules\User\Models\Role;
use App\Modules\Product\Models\CategoryType;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name' => 'Chaibi Alaa',
            'email' => 'ceo@bluepenlabs.com',
            'password' => bcrypt('e1zcs5y213'),
            'status' => 1,
        ]);

        Role::create([
            'title' => 'Administrateur',
        ]);

        Role::create([
            'title' => 'Volontaire',
        ]);

        Role::create([
            'title' => 'Agent Hôpital',
        ]);


        \Illuminate\Support\Facades\DB::table('user_roles')->insert([
            'user_id' => 1,
            'role_id' => 1
        ]);
    }
}
