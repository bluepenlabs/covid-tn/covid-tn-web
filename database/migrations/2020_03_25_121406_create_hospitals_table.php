<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->integer('type')->default(1); // 1 public / 2 private / 3 militaire / 4 instit
            $table->float('lat');
            $table->float('lng');
            $table->integer('phone')->nullable();
            $table->text('address')->nullable();
            $table->unsignedBigInteger('city_id')->nullable();
            $table->foreign('city_id')->references('id')->on('cities');
            $table->bigInteger('rib')->nullable();
            $table->string('iban')->nullable();
            $table->string('bank')->nullable();
            $table->string('bic_swift')->nullable();
            $table->string('picture')->nullable();
            $table->timestamps();
        });


        Schema::create('hospitals_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('hospitals_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->integer('category');
            $table->string('picture')->nullable();
            $table->text('description')->nullable();
            $table->float('min')->nullable();
            $table->float('max')->nullable();
            $table->timestamps();
        });

        Schema::create('hospital_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('service_id')->nullable();
            $table->foreign('service_id')->references('id')->on('hospitals_services');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('hospital_id')->nullable();
            $table->foreign('hospital_id')->references('id')->on('hospitals');
            $table->integer('beds')->default(0);
            $table->timestamps();
        });

        Schema::create('hospital_service_request', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('service_id')->nullable();
            $table->foreign('service_id')->references('id')->on('hospitals_services');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });

        Schema::create('hospital_service_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('service_id')->nullable();
            $table->foreign('service_id')->references('id')->on('hospitals_services');
            $table->unsignedBigInteger('product_id')->nullable();
            $table->foreign('product_id')->references('id')->on('hospitals_products');
            $table->integer('needed');
            $table->integer('auto_renew')->default(0);
            $table->timestamps();
        });

        Schema::create('hospital_service_stock', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('hospital_service_products');
            $table->integer('quantity')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospital_service_stock');
        Schema::dropIfExists('hospital_service_products');
        Schema::dropIfExists('hospital_service_request');
        Schema::dropIfExists('hospital_services');
        Schema::dropIfExists('hospitals_products');
        Schema::dropIfExists('hospitals_services');
        Schema::dropIfExists('hospitals');
    }
}
