<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('announcement');
            $table->timestamps();
        });

        Schema::create('statistics_global', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('deaths');
            $table->integer('recovered');
            $table->integer('cases');
            $table->integer('tests');
            $table->timestamps();
        });

        Schema::create('statistics_gender', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('gender');
            $table->integer('deaths');
            $table->integer('recovered');
            $table->integer('cases');
            $table->timestamps();
        });

        Schema::create('cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('g_id')->nullable();
            $table->timestamps();
        });

        Schema::create('statistics_city', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('city_id')->nullable();
            $table->foreign('city_id')->references('id')->on('cities');
            $table->integer('deaths');
            $table->integer('cases');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistics_city');
        Schema::dropIfExists('cities');
        Schema::dropIfExists('statistics_gender');
        Schema::dropIfExists('statistics_global');
        Schema::dropIfExists('generals');
    }
}
